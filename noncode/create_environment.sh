# Script to setup the environment for the application

if [ $(ls main.py |wc -l) == 0 ]; then
    echo "This script should be run in the main directory where main.py lives"
    exit 1
fi

# make the folder
mkdir env

# create the environment
virtualenv -p python3 env

# enter the environment
source env/bin/activate

# install dependencies
pip install flask ldap3 psycopg2 xhtml2pdf weasyprint

# exit environment
deactivate

echo "All requirements should be installed for a postgresql application.  If you're using mysql or mssql, you may need to install additional libraries."
