"""
Configuration for the application
"""


#Create a new secret key for each app
SECRET_KEY = "Change me to something random"
DEBUG = True

# change to true to enable profiling data in the log
PROFILE = False

##########################
# Database configuration #
##########################
DB = {
    "type": "sqlite",  # valid values are sqlite, mysql, and postgresql
    "name": ":memory:",  # the database name
    "user": "",  # user to login
    "password": "",  # user's password
}

##################
# Authentication #
##################

AUTH = {
    "type": "active_directory",  # valid types are "dummy", "active_directory", and "edirectory"
    "args": {  # args is dictionary of arguments to pass to the authenticator object.
        "server": "MY_DC",
        "base_dn": "dc=MY_DOMAIN, dc=MY_TLD",
        "bind_dn": "BIND_USER@MY_DOMAIN",
        "bind_pw": "BIND_USERS_PASSWORD",
        "require_group": "MY_REQUIRED_LDAP_GROUP",
        "ssl": True
        # LDAP ARGS
        # "server": Name of a server to talk to
        # "base_dn": AD only; top DN to use for searching
        # "bind_dn": DN of a username to bind the directory for secure searching
        # "bind_pw": password for the bind_dn user
        # "require_group": require this group CN for login
        # "write_group": If using auth level 3, membership in this group is required for write permissions.
        #                 If you have auth level 3 and this is not defined, nobody can write.
        # "ssl": ssl on or off

        # DUMMY ARGS
        # "users": A list or tuple of dictionaries with user records in them:
        #   {"username": The login name, "password": the password, "password_sha256": "The password, but encoded in sha256",
        #    "user_fullname": Full name of user, "groups": [List, of, groups]}
        # "write_group": A group to which membership is required in order to write data.
    },
    # AUTH LEVEL
    "level": 1,
    # auth_level is an integer that defines the authorization/authentication schema of the site.
    # The levels are:
    # 0: Public read/write.  No login, everyone has full read/write
    # 1: Public read.  Login is optional.  Read permissions don't require login, write does.
    # 2: Private read/write.  Login is required to access the site.  All authenticated users can write.
    # 3: Private read.  Login is required to read the site.  Only users with "write access" permissions can write.
    #GROUPS
    # This is a dict of auth groups and session keys.
    # Membership in the group named by the key will set the session value named in value to true.
    "groups": {}
}

# IF defined, log all POSTs that change a dataset.
# Default for schema is 'audit_log'
# Set to None to disable logging

AUDIT_LOG_TABLE = None


###########
# General #
###########

APPLICATION_NAME = "Williamson County Generic application"
ORGANIZATION_NAME = "Williamson County Government"

# The default callback to use
# In other words, which callback to use for "/"
DEFAULT_CALLBACK = "site_index"

# Which search types should be shown, and in what order.
# Current values are "keyword", "name", and "field"

SEARCHES = ("keyword", "name", "field")

# Whether to use dynamic or explicit field selectors for
# field searches and custom reporting UI.
# Values are "explicit" or "dynamic"
FIELD_SELECTORS = 'explicit'

# If set to True, keyword search will be broken out by schema
SEARCH_BY_SCHEMA = False

# Whether or not to enable REGEX support for field search and custom reports
ENABLE_REGEX_CONDITIONS = False

# Which navigation links to include, and in what order.
# Also define if special permissions are required.
# a "special permission" is basically a session key that evaluates truthy
# Possible values are:
# "home" (/), "site_index", "datasets", "search", "reports", "about"
NAVIGATION = {
    "home": {"permission": None, "order": 0},
    "datasets": {"permission": None, "order": 1},
    "search": {"permission": None, "order": 2},
    "reports": {"permission": None, "order": 3}
}

# Stylesheets
# The "base" stylesheet is where most of the basic styles are
# This stylesheet is defined upstream
BASE_STYLESHEET = "style.css"

# The "local" stylesheets are not part of upstream VCS
# It optionally defines overrides for this application instance
LOCAL_STYLESHEETS = []

# Jquery UI CSS path is a relative path to the css folder where the jquery-ui.css
# file and images folder can be found.  If empty, it will be found in the css folder
# itself.

JQUERY_UI_CSS_PATH = "."

# These are added right after wcgff.js, in order:

LOCAL_JS_FILES = []

# This should be updated whenever JS files are changed to eliminate caching issues.
WCG_JS_VERSION = '20220303'

############
# DataSets #
############
"""
Datasets define the model for the tables.  They are structured like this:

datasets = {
    "dataset_name": {
       "label": "the friendly name of the dataset",
       "main_table": "the primary table of the dataset",
       "main_view" : "the view for the main index page; must be formatted for datatables",
       "search_view": "the view used to return search results; must be formatted for datatables",
       "name_search_view": "the view used to search by name; (specialty)",
       "field_search_view": "the view used for generating the field search fields",
       "id_columns": ("primary key field", "other primary key field"),
       "limit_visibility": ['If specified', 'is a list of sessionkeys', 'any of which must be true', 'for the dataset to be accessible'],
       "limit_write": ['If specified', 'is a list of seesionkeys', 'any of which must be true', 'for the dataset to be writable'],
       'order_by': [(main_view_column_number, 'asc'), (main_view_column_number, 'desc')],
       "lookups": (
            {"label": "friendly name of the lookup table", "table_name": "table name of lookup table",
              "for_table": "table the lookup table matches back to",
              "fk_columns": {"column in main table": "matching column in lookup table"},
              "display_column": "name of a column to display when e.g. the lookup is shown in a drop-down"},
        ),
        "details": (
            {"label": "friendly name of the detail table", "table_name": "the name of the detail table",
             "fk_columns": {"column in main table": "matching column in detail table"},
              "order_by" [("fieldname", "dir"), ("anotherfieldname", "dir")}
        )
}
"""


DATASETS = {
}  # end datasets

# The name of the table holding the prepared reports
# Should be left alone unless you need to change this to avoid
# A name conflict
REPORTS_TABLE = "reports"

# The type of report interfaces available.
# Valid types are "prepared" and "custom_dataset".
# Remove one or the other to disable this feature.
REPORT_TYPES = ["prepared", "custom_dataset"]

# The default template for report output
# This will be used for custom reports or prepared reports that specify default
DEFAULT_REPORT_TEMPLATE = 'default'

# The folder (absolute or relative to the software root) where uploads go
# should be writeable by the user running the script

UPLOAD_FOLDER = "uploads"
