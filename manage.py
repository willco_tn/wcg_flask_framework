#!/usr/bin/env python3
"""
This tool is a general management tool for the application deployment

"""
from argparse import ArgumentParser
from getpass import getpass
import json
import re
from pathlib import Path
import readline

from fabric import Config, Connection
from paramiko.ssh_exception import AuthenticationException
# GitPython -- NOT pygit or pygit2
from git import Repo

############################
# Configuration Parameters #
############################

TIMEOUT = 5
BANNER_TIMEOUT = 30
CONFIG_FILE = Path(__file__).parent / 'manage.json'

CREDS = {
    'username': '',
    'password': ''
}

###########
# GLOBALS #
###########

OPERATIONS = {}
FAILED_HOSTS = []
DOMAINS = {}
LOCAL_GIT = Repo(Path(__file__).parent)
DEFAULT_KEYPATH = Path.home() / '.ssh' / 'id_rsa'

#####################
# Utility functions #
#####################

def prompt(message, default=None, choices=None):
    """Ask for some information until you get a valid response"""

    if default:
        string = f'{message}: [{default}] '
    else:
        string = f'{message}: '

    while True:
        result = input(string)
        if default and not string:
            result = default
        if choices and result not in choices:
            print('Invalid choice.  Valid choices are: ')
            print(', '.join(choices))
        if result == '_NONE_':
            result = None
        else:
            break

    return result


def get_config():
    """Retrieve the current configuration"""

    try:
        with open(CONFIG_FILE) as fh:
            config = json.load(fh)
    except FileNotFoundError:
        return {'domains': dict()}
    return config

def get_domain_info(domain):
    """Return information on a domain name"""

    d_info = get_config()['domains'].get(domain)
    if not d_info:
        raise ValueError(f'No such domain: {domain}')
    return d_info

def save_config(config):
    """Save the current configuration"""

    with open(CONFIG_FILE, 'w') as fh:
        json.dump(config, fh)

def operation(creds_required=True, existing_repo=True):
    """Decorator to register OPERATIONS"""
    def inner(function):
        OPERATIONS[function.__name__] = {
            "f": function,
            "creds_required": creds_required,
            "existing_repo": existing_repo
        }
        return function
    return inner


def get_creds(domain):
    """Query user for connection credentials"""

    d_info = get_domain_info(domain)
    def_username = d_info['username']

    username = prompt(
        'Connect as user',
        def_username
    )
    if not username:
        username = def_username
    CREDS['username'] = username

    password = getpass(
        f'Please enter the password for {username}: '
    )
    CREDS['password'] = password


def _get_connection(domain, use_sudo):
    """Retrieve and test a connection object."""
    config_args = {}
    connect_kwargs = {
        'password': CREDS['password'],
        'timeout': TIMEOUT,
        'banner_timeout': BANNER_TIMEOUT
    }

    if use_sudo:
        config_args['overrides'] = {
            'sudo': {'password': CREDS['password']}
        }
    d_info = get_domain_info(domain)

    if d_info.get('ssh_keyfile'):
        connect_kwargs['key_filename'] = d_info['ssh_keyfile']

    cx = Connection(
        d_info['host'],
        user=CREDS['username'],
        config=Config(**config_args),
        connect_kwargs=connect_kwargs
    )
    # Test the connection
    try:
        cx.open()
    except AuthenticationException as e:
        print(
            f'!!!! Could not connect to host {d_info["host"]}:'
            ' Authentication Failed'
        )
        print(e)
        get_creds(domain)
        if not CREDS['password']:
            cx = None
        else:
            cx = _get_connection(domain, use_sudo)
    except Exception as e:
        print(f'!!!! Could not connect to host {d_info["host"]}: {e}')
        cx = None

    if cx is None and d_info['host'] not in FAILED_HOSTS:
        FAILED_HOSTS.append(d_info['host'])
    elif cx is not None and d_info['host'] in FAILED_HOSTS:
        FAILED_HOSTS.remove(d_info['host'])
    return cx


def _run_commands(domain, commands, use_sudo=False):
    """Run a list of commands on a list of hosts"""
    host = get_domain_info(domain)['host']
    cx = _get_connection(host, use_sudo)
    if not cx:
        print(f'!!!! Skip host {host}\n')
        return
    print(f'#### Response from host {host}: \n')
    for command in commands:
        if use_sudo:
            cx.sudo(command)
        else:
            cx.run(command)
    cx.close()
    print(f'\n#### End of connection to {host}\n\n')

##############
# OPERATIONS #
##############

@operation(creds_required=False, existing_repo=False)
def detect_config(domain):
    """Create a config file from existing git info"""
    remotes = [x for x in LOCAL_GIT.remotes if x.name == domain]
    if not remotes:
        print(f"No git repo named {domain} found")
        return
    remote = remotes[0]
    url = list(remote.urls)[0]
    user, host, git_path = (
        re.split('[@:]', url)
        if '@' in url
        else [''] + url.split(':')
    )
    app_name = prompt(
            'App name',
            default=Path(__file__).parent.name
        )
    deploy_path = prompt(
        'Deployment path',
        f'/srv/www/{app_name}'
    )
    config = get_config()
    config['domains'][remote.name] = {
        'name': remote.name,
        'git_remote_name': remote.name,
        'host': host,
        'username': user,
        'git_path': git_path,
        'app_name': app_name,
        'deploy_path': deploy_path
    }
    save_config(config)

@operation(creds_required=False, existing_repo=False)
def add_domain(domain):
    """Add a new domain to the stored configuration"""
    name = domain
    git_remote_name = prompt("Git remote name", name)
    host = prompt('Host')
    username = prompt('User')
    app_name = prompt(
        'App name on this host',
        Path(__file__).parent.name
    )
    git_path = prompt('Remote git repo path', f'/srv/git/{app_name}')
    deploy_path = prompt('Deployment path', f'/srv/www/{app_name}')
    ssh_keyfile = prompt('SSH Key File', DEFAULT_KEYPATH)

    config = get_config()
    d_info = {
        'host': host,
        'name': name,
        'git_remote_name': git_remote_name,
        'username': username,
        'git_path': git_path,
        'deploy_path': deploy_path,
        'app_name': app_name,
        'ssh_keyfile': ssh_keyfile
    }
    config['domains'][name] = d_info
    save_config(config)

@operation(creds_required=False, existing_repo=False)
def show_domain(domain):
    """Display domain information"""

    d_info = get_domain_info(domain)

    if d_info:
        for key, value in d_info.items():
            print(f"{key}: {value}")
    else:
        print(f'No such domain: {domain}')

@operation(creds_required=True, existing_repo=True)
def reload(domain):
    # TODO: don't prompt for sudo password in plaintext
    # TODO: actually show log output?

    cx = _get_connection(domain, True)
    tail_log = prompt('Tail log?', choices=('y', 'n'), default='n')
    cx.sudo('systemctl reload apache2')
    if tail_log == 'y':
        cx.run('tail -f /var/log/apache2/error.log')


"""
# NOT ready for primetime
@operation
def deploy(domain):
    "Deploy the application"
    cx = _get_connection(domain, False)
    name = domain['name']
    host = domain['host']
    username = domain['username']
    git_path = domain['git_path']
    deploy_path = domain['deploy_path']
    app_name = domain['app_name']

    confirm = input(
        'You are about to initialize a new deployment on {host}.  '
        'Continue? [y/N] '
    ).lower()[0:1] == 'y'
    if not confirm:
        return

    # Begin working

    # create remote paths
    for directory in (git_path, deploy_path):
        cx.sudo(f'mkdir -p {directory}')
        cx.sudo(f'chown {username}:users {directory}')
    # create bare git repo
    cx.run('cd {git_path} && git init --bare')
    # create receive hook
    cx.run(
        f"echo 'GIT_WORK_TREE={deploy_path} "
        f"git checkout -f > {git_path}/hooks/post-receive"
    )
    cx.run(
        f'chmod +x {git_path}/hooks/post-receive'
    )

    # setup local remote
    cx.local(
        f'git remote add {git_remote_name} ssh://{host}/{git_path}'
    )
    cx.local(
        f'git push {git_remote_name} +master:refs/heads/master'
    )

    # configure apache
    cx.sudo(
        f"cp -nc {deploy_path}/noncode/apache_conf.conf"
        f" /etc/apache2/sites-available/{app_name}"
    )
    cx.sudo(f'a2ensite {app_name}')
    cx.sudo('systemctl restart apache2')
"""

@operation()
def run_commands(domain):
    commands = []
    while True:
        command = input('Command: ')
        if not command:
            break
        commands.append(command)
    use_sudo = input('Use sudo [y/N]: ').lower()[0:1] == 'y'

    _run_commands(domain, commands, use_sudo)

@operation()
def push(domain):
    cx = _get_connection(domain, True)
    d_info = get_domain_info(domain)
    cx.local(f"git push {d_info['git_remote_name']}")
    cx.run(f"{d_info['deploy_path']}/env/bin/pip install -r {d_info['deploy_path']}/requirements.txt")
    cx.sudo('systemctl restart apache2')


####################
# Main Entry point #
####################


def main():
    # populate the "all" option

    config = get_config()

    ap = ArgumentParser(description="WCGFF project management tool")
    ap.add_argument(
        "domain",
        help="The repo name to operate on."
    )
    ap.add_argument(
        "operation",
        choices=list(OPERATIONS.keys()),
        help="The operation you want to perform"
    )

    args = ap.parse_args()

    op = OPERATIONS[args.operation]

    # Get credentials and run the operation
    if op['existing_repo'] and args.domain not in config['domains']:
        print("Invalid domain specified")
        return
    if op['creds_required']:
        get_creds(args.domain)
    op['f'](args.domain)

    # list failed hosts
    if FAILED_HOSTS:
        print('\n!!!! Connection to the following hosts failed: ')
        for host in FAILED_HOSTS:
            print(f"  * {host}")


if __name__ == '__main__':
    main()
2
