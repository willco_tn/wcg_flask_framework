"""The WCG Flask Framework

By Alan Moore, ca 2013-2014 and probably beyond.

This is the main file that defines interactions between the application
and clients.  It's the Grand Central Station of the application.

You can extend this file by creating includes/local_routes.py.
"""
###########
# Imports #
###########

# Std Library imports
from functools import wraps
from pathlib import Path
from datetime import datetime

# Flask / Werkzeug / Jinja2 imports

import flask
from flask import Flask, render_template, session, redirect, url_for, \
    request, g, json, abort, jsonify, send_from_directory
from jinja2 import TemplateNotFound
from markupsafe import escape
# Seems to be broken now
#from werkzeug.contrib.profiler import ProfilerMiddleware

# Project imports

from includes import util, posting
from includes.myjson import JSONProvider

try:
    from includes.local_database import LocalDatabase as Database
except ImportError:
    from includes.db.database import Database
try:
    from includes.local_routes import LOCAL
except ImportError:
    LOCAL = {}
from includes.auth.auth import Authenticator


####################
# App object setup #
####################

# Flask will load config settings from "config.py", unless
# "instance/config.py" exists.  If so, the latter will override the former.

app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')
app.config.from_pyfile('config.py', silent=True)

# removed for now because ProfilerMiddleware is broken
# Turn on profiling support if enabled in config.
#if app.config.get("PROFILE"):
#    app.wsgi_app = ProfilerMiddleware(
#        app.wsgi_app,
#        restrictions=[app.config.get("PROFILE_LIMIT", 10)]
#   )

# A custom json encoder helps us handle additional datatypes
app.json = JSONProvider(app)

# This is required for some logic in the navigation template
app.jinja_env.add_extension('jinja2.ext.do')
app.jinja_env.filters["db2html_type"] = util.sql_datatype_to_html_field_type
app.jinja_env.filters["colname2label"] = util.column_name_to_label
app.jinja_env.filters["pluralize"] = util.pluralize
app.jinja_env.filters['strftime'] = util.strftime

# Log the flask version and location of flask,
# mainly to make sure we're using the correct venv
print("Running on flask v{} at {}.".format(flask.__version__, flask.__file__))

#####################
# Custom Decorators #
#####################


# Decorator to secure pages
def session_required(f, key=None):
    """Require a session for a callback, and optionally a session key.

    All users should have a session, even if it's just "Guest".

    Key is a session key that is required to be truthy in order for the
    page to be accessible.  If it's None, the page will just be available
    for anyone with a session.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' not in session:
            print("Could not access {} without a session".format(request.path))
            return redirect(url_for('login', attempted_url=request.url))
        if key and not session.get(key):
            print("Could not access {} without {}.".format(request.path, key))
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


def write_required(f):
    """Require write permissions for a callback.

    Wrapper that requires a user to have write access and a session.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get("write_access"):
            print("Session lacks write permissions; access to {} denied."
                  .format(request.path))
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


@app.before_request
def before_request():
    """Basic setup for a request

    This logic is run before every request to the application.
    """
    ########################
    # Security and Session #
    ########################

    # If this is a public site, create a session with user "guest"
    g.authconfig = app.config.get("AUTH")
    if 'username' not in session and g.authconfig.get("level") < 2:
        session['username'] = '__guest'
        session['user_fullname'] = 'Guest'
        session['csrf_token'] = util.generate_csrf_token()
        if g.authconfig.get("level") == 0:
            session['write_access'] = True

    ############
    # Database #
    ############

    # This section makes sure there's a valid database instance
    # before processing requests
    g.dbconfig = app.config.get("DB")
    g.datasets = app.config.get("DATASETS")

    # filter datasets according to visibility limits
    # If the dataset has a limit_visibility value
    g.datasets = {
        key: dsdata
        for key, dsdata in g.datasets.items()
        if (dsdata.get('limit_visibility') is None) or
        any([session.get(session_key)
             for session_key in dsdata.get('limit_visibility')])
    }

    g.db = (
        g.db
        if hasattr(g, 'db')
        else Database(
                g.dbconfig.get("type"),
                g.dbconfig.get("name"),
                g.dbconfig.get("host"),
                g.dbconfig.get("user"),
                g.dbconfig.get("password"),
                g.datasets,
                app.config.get("REPORTS_TABLE"),
                app.config.get('AUDIT_LOG_TABLE'),
                app.config.get('ENABLE_REGEX_CONDITIONS')
        )
    )
    g.upload_folder = app.config['UPLOAD_FOLDER']

    ##############
    # Navigation #
    ##############

    # sometime we don't want to call datasets "datasets"
    g.datasets_label = app.config.get("DATASETS_LABEL", "Dataset")

    # dataset_nav is used for navigating to datasets
    g.dataset_nav = sorted(
        [
            (
                dataset.get("label"),
                url_for("dataset", dataset_name=dataset_name)
            )
            for dataset_name, dataset in g.datasets.items()
        ],
        key=lambda dataset: dataset[0]
    )

    # The "master set" of navigation objects.
    # The configured nav will reference this list for details about navigation
    browse_datasets_url = (
        url_for(app.config.get("LINK_DATASET_TO"))
        if app.config.get("LINK_DATASET_TO")
        else None
    )
    navigation = {
        "home": ("Home", url_for("index")),
        "datasets": (
            "Browse {}".format(util.pluralize(g.datasets_label)),
            browse_datasets_url,
            g.dataset_nav
        ),
        "search": ("Search", url_for("search")),
        "reports": ("Reports", url_for("reports")),
        "site_index": ("Site Index", url_for("site_index")),
        "about": ("About", url_for("about"))
    }
    for page in LOCAL.get("pages", []):
        key = page.get("key") or page.get("path").replace("/", '')
        label = page.get("label") or key.title()
        url = page.get("url") if page.get("url") else url_for(page.get("callback", index).__name__)
        navigation[key] = (label, url)

    # filter and order the navigation settings per the config
    default_nav_data = {
        "home": {"permission": None, "order": 0},
        "datasets": {"permission": None, "order": 1}
    }
    nav_data = app.config.get("NAVIGATION", default_nav_data)
    main_nav = [
        k for k, value in nav_data.items()
        if not value.get("permission") or session.get(value.get("permission"))
    ]
    navigation = {
        nav_data.get(k).get("order", k): v for k, v in navigation.items()
        if k in main_nav
    }

    ###############
    # G Variables #
    ###############

    # This defines arguments that will be set to most pages
    # (not including login)
    # TODO:
    #   * clean this up since session, config should be automatically available
    g.std_args = {
        "application_name": app.config.get("APPLICATION_NAME"),
        "username": session.get("username"),
        "user_fullname": session.get("user_fullname"),
        "user_email": session.get("user_email"),
        "organization": app.config.get("ORGANIZATION_NAME"),
        "csrf_token": session.get('csrf_token'),
        "auth_level": g.authconfig.get("level"),
        "write_access": session.get("write_access"),
        "navigation": navigation,
        "datasets_metadata": g.datasets,
        "base_stylesheet": app.config.get("BASE_STYLESHEET", "style.css"),
        "local_stylesheets": app.config.get("LOCAL_STYLESHEETS", []),
        "jquery_ui_css_path": app.config.get("JQUERY_UI_CSS_PATH", "."),
        "datasets_label": g.datasets_label,
        "_session": dict(session),
        "now": datetime.now()
    }
    for session_key in g.authconfig.get("groups", {}).values():
        g.std_args[session_key] = session.get(session_key)

    # Some situations call for the application directory
    g.app_dir = Path(__file__).absolute().parent

#########
# PAGES #
#########


@app.route('/login', methods=['GET', 'POST'])
def login():
    """Process login credentials (POST) or display the login page (GET)

    If this is a POST, process the username and password, and if valid, set up
    the user's session accordingly.

    If its' a GET, show the login form.
    """
    username = ''
    message = None
    attempted_url = request.args.get("attempted_url", '')
    attempted_hash = request.form.get("attempted_hash", '')
    print("Attempted: {}".format(attempted_url))
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        auth = Authenticator(
            g.authconfig.get("type"),
            **g.authconfig.get("args")
        )
        if auth.authenticate(username, password):
            # get a fresh session
            session.clear()
            # now start populating it with info
            session['username'] = username
            session['user_fullname'] = auth.user_fullname()
            session['auth_source'] = auth.source()
            session['user_email'] = auth.user_email()
            session['csrf_token'] = util.generate_csrf_token()
            if g.authconfig.get("level") < 3:
                session['write_access'] = True
            else:
                session['write_access'] = auth.user_can_write()
            for group, session_key in g.authconfig.get("groups", {}).items():
                if auth.user_in_group(group):
                    session[session_key] = True
            # process any instance-specific login hooks
            for hook in LOCAL.get("login_hooks", []):
                hook(
                    username=username,
                    attempted_url=attempted_url,
                    attempted_hash=attempted_hash,
                    authenticator=auth
                )
            # Send the user on his way!
            return redirect(
                (attempted_url + attempted_hash)
                or url_for('index')
            )
        else:
            print("Login for {} failed: {}".format(username, auth.error()))
            message = "Login Failed"
    return render_template(
        'login.jinja2', page='login',
        username=escape(username),
        attempted_url=attempted_url,
        logout_url=url_for('logout'), message=message,
        application_name=app.config.get("APPLICATION_NAME"),
        base_stylesheet=app.config.get("BASE_STYLESHEET", "style.css"),
        local_stylesheets=app.config.get("LOCAL_STYLESHEETS", []),
        jquery_ui_css_path=app.config.get("JQUERY_UI_CSS_PATH", "."),
        _session=dict(session)
    )


@app.route("/logout")
@session_required
def logout():
    """End a user's session"""
    session.pop("username", None)
    session.clear()
    # redirect to index, not login, in case this is a public site.
    return redirect(url_for("index"))


@app.route("/")
def index():
    """Callback for the root of the application"""
    # redirect to the configured landing page
    landing_page = app.config.get("DEFAULT_CALLBACK", "site_index")
    return redirect(url_for(landing_page))


@app.route("/site_index")
@session_required
def site_index():
    """Display a list of all the datasets"""
    template = app.config.get("SITE_INDEX_TEMPLATE", 'site_index.jinja2')
    for hook in LOCAL.get('page_hooks', {}).get('site_index', []):
        hook()
    return render_template(template, page='index', **g.std_args)


@app.route("/about")
@session_required
def about():
    """Display some text about the website, or help, or whatever"""
    for hook in LOCAL.get('page_hooks', {}).get('about', []):
        hook()
    # not every app will have this page
    try:
        return render_template("about.jinja2", page='about', **g.std_args)
    except TemplateNotFound:
        return render_template("base.jinja2", page='about', **g.std_args)


@app.route("/settings")
@session_required
def settings():
    """Display session information or user-settings.

    This page would allow the user to view or set some user-specific settings
    """
    for hook in LOCAL.get('page_hooks', {}).get('settings', []):
        hook()
    raw_content = ''
    return render_template("settings.jinja2", page='settings',
                           raw_content=raw_content, **g.std_args)


@app.route("/sysreq")
def system_requirements():
    """Display system requirements.

    Users are sent here when using an incompatible browser.
    """
    return render_template("system_requirements.jinja2", page='sysreq',
                           **g.std_args)


@app.route("/dataset/<dataset_name>")
@session_required
def dataset(dataset_name):
    """Display the main menu for a dataset.
    """
    if dataset_name not in g.datasets.keys():
        abort(404)
    main_view = g.datasets.get(dataset_name).get("main_view")
    order_by = json.dumps(
        g.datasets.get(dataset_name).get('order_by', [(0, 'asc')])
    )
    data = {
        "dataset_name": dataset_name,
        "dataset": g.datasets.get(dataset_name),
        "headers": g.db.get_headers(main_view),
        'order_by': order_by
    }
    g.std_args['write_access'] = util.check_dataset_write_permissions(dataset_name)
    return render_template("dataset.jinja2", page='dataset', data=data,
                           **g.std_args)


@app.route("/reports")
@session_required
def reports():
    """Display the reports page."""
    reports = None
    valid_report_types = ("prepared", "custom_dataset")
    report_types = [rt for rt in app.config.get("REPORT_TYPES", ["prepared"])
                    if rt in valid_report_types]
    if "prepared" in report_types:
        reports = g.db.get_report_list()
    return render_template('reports.jinja2', page='reports', reports=reports,
                           report_types=report_types, **g.std_args)


@app.route("/run_report", methods=['GET', 'POST'])
@session_required
def run_report():
    """Run a report and return the results.

    This mess of a function runs the actual report and returns a response
    based on the requested output_type.
    """
    # Eliminate some abort conditions
    if not request.form or not any([
            request.form.get("report_query"),
            request.form.get("custom_query")
    ]):
        abort(400, "No report specified.")
    if session['csrf_token'] != request.form.get('csrf_token'):
        abort(403)

    # Now the actual logic
    # Process the input
    post = util.post_process(request.form)
    options = {
        "output_type": post.get("output_type"),
        "transmission_type": post.get("transmission_type", "download"),
        "send_to": post.get("email_to"),
        "email_note": post.get("email_note"),
        "landscape": (
            'landscape'
            if (post.get("landscape") == "1")
            else 'portrait'
        ),
        "paper_format": post.get("paper_format")

    }
    if request.form.get("custom_query"):
        order_fields = []
        for k, v in sorted(post.get("order_fields", {}).items()):
            order_fields.append((v, post.get("order_dir", {}).get(k)))
        results = g.db.get_custom_query_results(
            post.get("dataset"),
            post.get("fields"),
            post.get("search_fields"),
            order_fields
        )
        options["report_name"] = "Custom Report"
        options["parameters"] = []
    else:
        report_id = post.get("report_query")
        options["parameters"] = post.get("parameters")
        options["report_name"] = g.db.get_report_name(report_id).replace(' ', '_')
        results = g.db.get_report_results(
            report_id,
            options["parameters"],
            write_access=session.get('write_access')
        )
    # Allow instances to override the default report template
    if results['template_name'] == 'default':
        results['template_name'] = app.config.get(
            'DEFAULT_REPORT_TEMPLATE', 'default')

    return util.process_report_output(results, options)


@app.route("/search")
@session_required
def search():
    """Display the search page"""
    valid_searches = ("keyword", "name", "field")
    searches = [s for s in app.config.get("SEARCHES", [])
                if s in valid_searches]
    schema_search = app.config.get("SEARCH_BY_SCHEMA")
    schemas = {
        # Be aware that if labels are not in the format "schema: table",
        # The label of each schema will be that of the last table in the list
        # Due to the way dict comprehensions work.
        key.split(".")[0]: value.get("label").split(":")[0]
        for key, value in g.datasets.items()
    }
    return render_template(
        "search.jinja2",
        page='search',
        searches=searches,
        schema_search=schema_search,
        schemas=schemas,
        **g.std_args
    )


@app.route("/do_search/<search_type>", methods=['POST'])
@session_required
def do_search(search_type='keyword'):
    """Perform a requested search, and return the results"""
    if (session['csrf_token'] != request.form.get('csrf_token')):
        abort(403)

    search_types = {
        "keyword": g.db.get_keyword_search_results,
        "name": g.db.get_name_search_results,
        "field": do_field_search
    }
    if search_type in search_types.keys():
        search_results = search_types[search_type](request.form)
        print(search_results)
        return render_template(
            "search_results.jinja2",
            results=search_results,
            formdata=request.form,
            **g.std_args
        )
    else:
        return "<p class=error>Invalid search type</p>"


def do_field_search(formdata):
    """Run the field search results query after processing formdata

    TODO: make this function unnecessary.  It's here because the field search
    Needs the data in a slightly different format than the other searches.
    """
    return g.db.get_field_search_results(util.post_process(formdata))


# Add in locally defined pages
localfunctions = []
for page in LOCAL.get("pages", []):
    if not page.get("callback"):
        # This is a page with just a URL, no need to add a URL_rule
        continue
    app.add_url_rule(
        page.get("path"),
        page.get("callback").__name__,
        session_required(page.get("callback"), page.get("permission"))
    )

##################
# Uploaded Files #
##################

@app.route("/uploads/<path:filename>")
@session_required
def upload(filename):
    """Allow download of uploaded file"""
    upload_folder = app.config['UPLOAD_FOLDER']
    if not Path(upload_folder).is_absolute():
        upload_folder = str(g.app_dir / upload_folder)
    return send_from_directory(
        upload_folder,
        filename,
        as_attachment=True
    )

#########
# Forms #
#########
@app.route("/forms/<form_id>")
@app.route("/forms/<form_id>/<key>")
@session_required
def forms(form_id, key=None):
    """Return the requested form

    This callback is strictly for returning empty HTML forms from
    the templates/forms directory.
    """
    forms_dir = "forms"  # subdirectory of templates dir
    forms_dir = g.app_dir / "templates" / "forms"
    if forms_dir.exists():
        valid_forms = [
            formname.stem
            for formname in forms_dir.iterdir()
        ]
    else:
        print('No form directory; please create <app root>/templates/forms')
        abort(404)
        return

    data = {}  # this will hold data to send to the template

    # Assume the dataset name is the same as the form name, unless
    # a dataset is explicitly specified in the request.
    dataset = (
        form_id
        if form_id in g.datasets.keys()
        else request.args.get("dataset", None)
    )
    form_id = form_id if form_id in valid_forms else None

    if not form_id and not dataset:
        # if the form_id is neither a valid dataset or form name, abort
        print("Invalid form id and dataset specified")
        abort(404)
        return

    elif dataset and not form_id:
        # if it's a dataset, but not a valid form name, make a default form
        form_id = "default"

    if dataset:
        data["_schema"] = g.db.get_schema_for_dataset(dataset)
        data["_fields_with_lookups"] = {
            field: {
                "table": lookup.get("table_name"),
                "display_column": lookup.get("display_column"),
                "id_column": lookup.get("fk_columns")[field],
                "for_table": lookup.get('for_table')
            }
            for lookup in g.datasets[dataset].get("lookups", [])
            for field in lookup["fk_columns"].keys()
        }
        data["_lookups"] = g.db.get_lookup_values_for_dataset(dataset)
        file_fields = (
            [
                f
                for f in data["_schema"]["main"]
                if f.get('data_type') == 'file'
            ] +
            [
                f
                for t in data["_schema"].get("details", {}).values()
                for f in t
                if f.get('data_type') == 'file'
            ]
        )
        data['has_file_fields'] = any(file_fields)
    data["dataset_name"] = dataset
    data["dataset"] = g.datasets.get(dataset)
    data['args'] = request.args
    g.std_args['write_access'] = util.check_dataset_write_permissions(dataset)

    # run any form hooks in the local instance
    if LOCAL.get("form_hooks"):
        for hook in LOCAL.get("form_hooks", {}).get(form_id, []):
            hook(key=key, data=data)
    # print ("DATA for form: " + data.__str__())
    html = render_template(
        "{}/{}.jinja2".format(forms_dir.stem, form_id),
        key=key,
        data=data,
        **g.std_args
    )
    return html

########
# JSON #
########


@app.route("/json/<callback>")
@app.route("/json/<callback>/<key>")
@session_required
def json_get(callback, key=None):
    """Fetch data from the database and return as JSON

    This method is for getting JSON data from the server.
    callback is a key that maps to a db callback.
    key is optional.
    """
    callback_functions = {
        "dataset_list": g.db.get_dataset_mainlist,
        "record_details": g.db.get_record_details,
        "autocomplete": g.db.get_autocomplete_values,
        "dataset_lookups": g.db.get_lookup_values_for_dataset
    }
    if LOCAL:
        local_ops = {
            k: (
                v.get("callback") or getattr(g.db, v.get("db_function"))
            )
            for k, v in LOCAL.get("json_ops", {}).items()
            if not v.get("permission") or session.get(v.get("permission"))
        }
        callback_functions.update(local_ops)

    f = callback_functions.get(callback)
    if not f:
        print('Invalid callback {}'.format(callback))
        abort(400)
    try:
        result = f(key, **request.args) if (f and key) else f(**request.args)
        return json.dumps(result)
    except (Database.NoSuchDataset, Database.NoSuchRecord) as e:
        print(e)
        abort(404, str(e))

    except Database.DataError as e:
        print(e)
        abort(400, str(e))


################
# POST METHODS #
################
"""
Post should only be used for methods that write to the database.
Write permissions are required.
"""

@app.route("/post/<callback>", methods=['POST'])
@app.route("/post/<callback>/<key>", methods=['POST'])
@session_required
@write_required
def post_data(callback, key=None):
    """Process a POST for writing to the database"""
    # check the session token
    if (session['csrf_token'] != request.form.get('csrf_token')):
        print("Bad csrf_token: {} expected, got {}.".format(
            session['csrf_token'], request.form.get('csrf_token'))
        )
        abort(403)

    post_functions = {
        "save_record": posting.write_dataset_record
    }
    # Add in local post operations to the functions dict
    if LOCAL:
        local_ops = {
            k: (
                v.get("callback") or getattr(g.db, v.get("db_function"))
            )
            for k, v in LOCAL.get("post_ops", {}).items()
            if not v.get("permission") or session.get(v.get("permission"))
        }
        post_functions.update(local_ops)

    f = post_functions.get(callback)
    if not f:
        print("Invalid post method {}".format(f))
        abort(400)
    formdata = util.post_process(request.form)
    try:
        response = (
            f(key, _session=session, **formdata)
            if (f and key)
            else f(_session=session, **formdata)
        )
    except (
            Database.CannotDeleteRecord,
            Database.NoSuchDataset,
            Database.NoSuchRecord,
            Database.NoSuchTable,
            Database.DataError
    ) as e:
        print(str(e))
        response = jsonify({
            "error": "{}: {}".format(
                util.uncamel(type(e).__name__),
                str(e)
            )
        })
        response.status_code = 400
        return response


    return json.dumps(response)


###############
# BOILERPLATE #
###############

application = app

if __name__ == "__main__":
    app.run(host=app.config.get("HOST"))
