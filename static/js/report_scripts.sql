-- Possible Greenbelt forest query

with yes_ids as (
        select 'FM' || right((date_part('year', NOW()) + a.x)::varchar, 2) as m_id
        from generate_series(-9, 0) as a(x)
    ),
    no_ids as (
        select 'FM' || right((date_part('year', NOW()) + a.x)::varchar, 2) as m_id
        from generate_series(-10, 1) as a(x)
        ),
    yes_lrsns as (select lrsn from grm.memos where memo_id in (select m_id from yes_ids)),
    no_lrsns as (select lrsn from grm.memos where memo_id in (select m_id from no_ids) and memo_text like '%pending%')
select distinct pb.lrsn, pb.OwnerName1, pb.OwnerName2
from
    grm.PARCEL_BASE pb
where pb.property_class = 102512   -- only return forest parcels
    and pb.lrsn in (select lrsn from yes_lrsns)
    and pb.lrsn not in (select lrsn from no_lrsns)
    and pb.status = 'A'
;
