///////////////////
// Reporting App //
///////////////////

/* global $ util Mustache WCGRecordList */


function OutputForm($of){
    $of.type_sel = $of.find(':input[name=output_type]');
    $of.pdf_options = $of.find('.pdf_options');
    $of.file_options = $of.find('.file_options');
    $of.email_options = $of.find('.email_options');
    $of.transmission_sel = $of.find('[name=transmission_type]');
    $of.animation_options = {
	effect: 'blind',
	duration: 250
    };
    $of.email_note = $of.find('[name=email_note]');
    $of.normal_submit = false;
    $of.transmit = '';

    //toggle the right output bits
    $of.type_sel.on('change', function(){
	var value = $of.type_sel.filter(':checked').val();
	$of.output_type = value;
	if (value.match(/pdf|csv|xlsx/)){
	    $of.file_options.show($of.animation_options);
	    $of.normal_submit = $of.transmit !== 'email';
	}else{
	    $of.file_options.hide($of.animation_options);
	    $of.normal_submit = false;
	}
	if (value.match(/pdf/)){
	    $of.pdf_options.show($of.animation_options);
	}else{
	    $of.pdf_options.hide($of.animation_options);
	}
    });
    $of.type_sel.trigger('change');

    $of.transmission_sel.on('change', function(){
	var value = $of.transmission_sel.filter(':checked').val();
	$of.transmit = value;
	if (value.match(/email/)){
	    $of.email_options.show($of.animation_options);
	    $of.normal_submit = false;
	}else{
	    $of.email_options.hide($of.animation_options);
	    $of.normal_submit = $of.output_type.match(/pdf|csv|xlsx/);
	}
    });
    $of.transmission_sel.trigger('change');

    $of.set_note_text = function(text){
	if ($of.email_note.val() === ''){
	    $of.email_note.val(text);
	}
    };

    return $of;
}

function ReportForm($rf, options){
    if ($rf.length === 0){
	return null;
    }
    options = options || {};
    $rf.form = $rf.find('FORM');
    //output section
    $rf.output = OutputForm($rf.form.find('.output'));
    $rf.form.wcgform($.extend({
	submit_event_name: 'report_form_submitted',
	normal_submit: function(){
	    return $rf.output.normal_submit;
	}
    }, options.wcgform));

    return $rf;
};

function PreparedReport($node, options){
    var $pr = ReportForm($node, options);
    if (!$pr) return null;
    //find items
    $pr.report_select = $pr.find('SELECT[name=report_query]');
    $pr.parameter_inner = $pr.find('#parameters_inner');
    $pr.parameter_outer = $pr.find("#parameters");
    $pr.report_description = $pr.find('#report_description');
    $pr.report_help = $pr.find("#report_help");

    //param transforms
    // data about each report is kept in document.prepared_reports
    // we use that data to construct parameter fields
    // This object translates parameter types to HTML templates
    $pr.param_transforms = {
	'tf': {
	    template: "<label for='{{param}}_inp'>{{label}}</label>" +
		"<SELECT  name=parameters[{{param}}] id='{{param}}_inp' {{#required}}required{{/required}}>" +
		"<OPTION value='true'>True</OPTION><OPTION value='false'>False</OPTION></SELECT>"
	},
	'atf': {
	    template: "<label for='{{param}}_inp'>{{label}}</label>" +
		"<SELECT  name=parameters[{{param}}] id='{{param}}_inp' {{#required}}required{{/required}}>" +
		"<OPTION value='any' selected>(any/ignore)</OPTION>" +
		"<OPTION value='true'>True</OPTION><OPTION value='false'>False</OPTION></SELECT>"
	},
	'num': {
	    template: "<label for='{{param}}_inp'>{{label}}: </label> <input type=number name=parameters[{{param}}] value='' step=.01 id='{{param}}_inp' {{#required}}required{{/required}}>"
	},
	'integer': {
	    template: "<label for='{{param}}_inp'>{{label}}: </label> <input type=number name=parameters[{{param}}] value='' step=1 id='{{param}}_inp' {{#required}}required{{/required}}>"
	},
	'date': {
	    template: "<label for='{{param}}_inp'>{{label}}: </label> <input type=date name=parameters[{{param}}] id='{{param}}_inp' {{#required}}required{{/required}}>"
	},
	'lookup': {
	    template: "<label for='{{param}}_inp'>{{label}}: </label> <select name=parameters[{{param}}] id='{{param}}_inp' {{#required}}required{{/required}}>{{#choices}}<option value={{value}}>{{label}}</option>{{/choices}}</select>"
	},
	'text': {
	    template: "<label for='{{param}}_inp'>{{label}}: </label> <input name=parameters[{{param}}] id='{{param}}_inp' {{#required}}required{{/required}}>"
	}
    };

    //Actions on the report change
    $pr.report_select.on('change', function(){
	var $selected = $(this).find('OPTION:selected');
	if (!$selected.val()) return;
	//setup a field containing the name of the report
	$pr.find("INPUT[name=report_name]").val($selected.html());

	// set the default email
	$pr.output.set_note_text(Mustache.render(
	    '{{user}} has sent you the attached report, "{{report}}" from the {{db}} database.',
	    {user: window.user_fullname, report: $selected.text(), db: document.title}
	));

	// setup the parameter fields
	// This is still a mess
	$pr.parameter_inner.html("");
	$pr.parameter_outer.filter(":visible").hide("blind", {}, 250);
	$pr.report_description.filter(":visible").hide();
	var report_help = "";
	var description = "<h2>" + $selected.html() + ":</h2><p>" + util.prep($selected.attr("title")) + "</p>";
	$pr.report_description.html(description).show("fade", {}, 250);

	//var params = $selected.data("params");
	var params = document.prepared_reports[$selected.val()]['params'];
	if (params && !$.isEmptyObject(params)){
	    var param_types = {text: 0, date: 0, tf: 0, atf: 0, num: 0, integer: 0, lookup:0};
	    // sort params by the "order" value
	    pkeys = Object.keys(params).sort(function(a, b){ return params[a].order > params[b].order ? 1:-1 });

	    pkeys.forEach(function(p){
		var param = params[p];
		var form_html = '';
		for (var ptype in $pr.param_transforms){
		    if (param.type === ptype){
			var transform = $pr.param_transforms[ptype];
			param_types[ptype] += 1;
			// generate the html
			form_html = Mustache.render(
			    transform.template,
			    {
				param: p,
				label: param.label,
				choices: param.choices,
				required: param.required
			    }
			);

			//stop looking for matches
			break;
		    }
		}
		$pr.parameter_inner.append($("<li />", {html: form_html}));
	    }); // end foreach
	    //generate report help text
	    if (param_types.text > 0){
		report_help += "<li>Use % as a wildcard in text fields</li>";
	    }
	    // initialize the new widgets
	    $pr.form.wcg.init_widgets();

	    //dates are pretty much required
	    //Deprecated:  don't hardcode anything required.
	    //$("INPUT[type=date]").attr("required", "required");
	}else{ //not params
	    $pr.parameter_inner.append("<p class=note>No parameters for this query</p>");
	}
	$pr.report_help.html(report_help);
	$pr.parameter_outer.show("blind", {}, 250);
    });
    $pr.report_select.trigger('change');

    return $pr;
}

function CustomReport($node, options){
    options = options || {};
    var $cr = ReportForm($node, options);
    if (!$cr) return null;

    //set the default email text
    $cr.output.set_note_text(Mustache.render(
	'{{user}} has sent you the attached custom report from the {{db}} database.',
	{user: window.user_fullname, db: document.title}
    ));

    $cr.dataset_sel = $cr.find('SELECT[name=dataset]');
    $cr.conditions = $cr.find('#conditions_inner');
    $cr.output_fields = $cr.find('#output_fields_inner');
    $cr.order_by = $cr.find('#order_by_inner');
    $cr.order_dir_select = $('<select />', {name: 'order_dir[0]'})
	.append($('<option />', {value: 'asc', text: 'Ascending'}))
	.append($('<option />', {value: 'desc', text: 'Descending'}))
    ;
    $cr.search_fields = $cr.find("#field_search_fields");

    $cr.dataset_sel.on('change', function(){
	var dataset = $(this).val();
	if (!dataset) return;
	$cr.output_fields.html('');
	$cr.order_by.html('');

	//setup output fields and order by fields
	$.get(
	    util.mkurl(['forms', 'dataset_field_select'], {dataset: dataset}),
	    function(html){
		$cr.output_fields.append($('<li/>', {html: html}));
		var $order_select = $(html).clone();
		$order_select.attr("name", "order_fields[0]").removeAttr('dataset-requireset_id').removeAttr('required');
		$cr.order_by.append(
		    $('<li/>', {html: $order_select.prop('outerHTML') + $cr.order_dir_select.prop('outerHTML')})
		);
		$cr.form.wcg.init_widgets($cr.output_fields);
		$cr.form.wcg.init_widgets($cr.order_by);
	    });
	//setup conditions form
	$.get(
	    // field_search_form is set by js in base.jinja2
	    util.mkurl(['forms', field_search_form], {dataset: dataset, raw_tables:1}),
	    function(html){
		$cr.conditions.html(html);
		$cr.conditions.wcgfield_parameter_form($cr.form);
		$cr.form.wcg.init_widgets($cr.conditions);
		//trigger change on the operator selects to appropriately enable/disable fields.
	    }
	);

    });
    $cr.dataset_sel.trigger('change');

    //add an output field selector when the last one is set to a value
    $cr.on("change", "SELECT[name^='fields[']:last", function(){
	var $fs = $(this);
	if ($fs.val().length > 0){
	    //only clone the select, in case downstream uses something like select2
	    var $newselect = $fs.clone();
	    $newselect.val("");
	    var $newli = $("<li />");
	    $newli.append($newselect);
	    util.increment_input_name_ordinals($newli, 'fields');
	    $cr.output_fields.append($newli);
	    if ($cr.output_fields.find('SELECT').length > 1){
		$cr.output_fields.find('SELECT:last-child').removeAttr('required');
	    }
	    $cr.form.wcg.init_widgets();
	}
    });
    //add an order field selector when the last one is set to a value
    $cr.on("change", "SELECT[name^='order_fields[']:last", function(){
	var $os = $(this);
	if ($os.val().length > 0){
	    // rather than cloning the entire li, clone the individual selects.
	    // This way if downstream modifies with a widget, it won't duplicate generated fields
	    var $newfieldselect = $os.clone();
	    var $newdirselect = $os.closest('li').find('select[name^=order_dir]').clone();
	    var $newli = $('<li />');
	    $newli.append($newfieldselect);
	    $newli.append($newdirselect);
	    $newfieldselect.val('');
	    $newdirselect.val('asc');
	    util.increment_input_name_ordinals($newli, 'order_fields');
	    util.increment_input_name_ordinals($newli, 'order_dir');
	    $cr.order_by.append($newli);
	    $cr.form.wcg.init_widgets();
	}
    });
    // remove extra blank selects if they are empty
    $cr.on("change", "SELECT[name^='fields['],#queryform SELECT[name^='order_fields[']", function(){
	if ($(this).val().length === 0){
	    $(this).closest("li").remove();
	}
    });

    return $cr;
}

function WCGReportingApp(selector, options){
    var $ra = $(selector);
    if ($ra.length === 0){
	return null;
    }
    options = options || {};

    //find items


    $ra.results = $ra.find("#results");

    $ra.init_ui = function(){
	$ra.tabs();
	$ra.reports = {
	    prepared: PreparedReport($ra.find('#reportform'), options.prepared),
	    custom: CustomReport($ra.find('#queryform'), options.custom)
	};
    };
    $ra.init_ui();

    $.subscribe('report_form_submitted', function(e, html){
	$ra.results.html(html);
	$ra.results.find('>TABLE').each(function(i, el){
	    WCGRecordList(el, options.results);
	});


    });

    return $ra;
}
