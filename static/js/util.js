//Generic JS utility functions
//Assumes jquery, jquery-ui, moment, mustachejs
/* global $, FormData, jQuery Mustache moment*/
//functions.js
/* global initialize_widgets, initialize_autocomplete, upload_base */


// Hack to fix Select2 dialogs inside jquery-ui dialogs
if ($.ui && $.ui.dialog && $.ui.dialog.prototype._allowInteraction) {
    var ui_dialog_interaction = $.ui.dialog.prototype._allowInteraction;
    $.ui.dialog.prototype._allowInteraction = function(e) {
	if ($(e.target).closest('.select2-dropdown').length) return true;
	return ui_dialog_interaction.apply(this, arguments);
    };
}

const util = new ( function() {
    "use strict";
    const util = this;

    ///////////
    // mkurl //
    ///////////

    // This tool is used extensively throughout WCGFF to build URLs
    // It takes an array of path components and an object of data arguments
    // It returns an absolute URL string prefixed with the application's base URL.
    // It also handles URI encoding of data components.
    util.mkurl = function(path_components, data){
	// builds a url from arguments with the basepath prepended
	var url = document.basepath.replace(/\/$/, '');
	for (var i in path_components){
	    var c = path_components[i]; // must be var, can be overwritten
	    if (c instanceof Function) {
		c = c();
	    };
	    if (c){
		url += '/';
		url += c.replace(/\//, '');
	    }
	}
	if (data){
	    url += "?";
	    for (var key in data){
		var value = data[key]; // must be var, can be overwritten
		if (value instanceof Function){
		    value = value();
		}
		url += encodeURIComponent(key) + '=' + encodeURIComponent(value) + '&';
	    }
	}
	return url;
    };

    ///////////////////
    // populate_data //
    ///////////////////

    // This is a critical component of the WCGFF front-end.  It's used by forms to populate data
    // retrieved by AJAX.  I've tried several approaches to optimizing it and this seems the fastest, though
    // it's still slower than it should be.
    util.populate_data = function($target, data, ifnull, name_template, select_opts){
	//populates form inputs and spans from a data object by matching names & classnames to property names
	//triggers a "populated" event on each element when it populates the data
	// Arguments:
	// $target is a jquery object that CONTAINS the nodes to be populated.
	// data is an object containing the data to be populated
	// ifnull is a value to use when data contains a null
	// name_template is a template applied to the data key to convert it to the field name.
	//               it is a string that contains $NAME$ where the key should go.
	//               this is mostly used for detail entries.
	// select_opts is an object that contains arrays of values for each SELECT in the form.
	//               It is necessary so named SPANS can be populated with the selected option's text rather than its value.

	name_template = name_template || '$NAME$'; // default name template is just the name
	$.each(data, function(item, value){
	    item = name_template.replace("$NAME$", item);

	    // populated named SPANs
	    // WCGFF uses SPANs with a [name=] attribute for read-only display.
	    var span = $target.find('SPAN[name="' + item + '"]');
	    if (span.length > 0){
		// for spans marked "date", humanize the date value before populating it.
		if(span.hasClass("date")){
		    value = util.human_date(value, span.hasClass('no_time'));
		// for selects, get the display value of the option rather than the key value
		}else if (span.hasClass('from_select')){
		    var opts = select_opts[span.attr('name')];
		    value = (value in opts) ? opts[value]: '';
		// for number fields, a class of "commas" will add commas
		}else if (span.hasClass('from_number') && span.hasClass('commas')) {
		    value = (value !== null && value !== undefined ) ? value.toLocaleString() : '';
		// File fields need special handling
		}else if (span.hasClass('file_info') && value){
		    value.upload_base = upload_base; // upload_base is defined in the config and populated by the base Jinja2 template
		    value.size = util.human_file_size(value.size);

		    // if the mime type is an image of some kind, put a thumbnail in an img tag
		    // otherwise, display a download link.
		    if (value.mime_type.match(/image\//)){
			value = Mustache.render(
			    "<a href='{{upload_base}}{{url}}' target='_blank'>" +
				"<img src='{{upload_base}}{{url}}' title='Click to see full image' height=100 />" +
				"<br>{{name}}</a>, ({{size}})",
			    value
			);
		    }else{
			value = Mustache.render("<a href='{{upload_base}}{{url}}'>{{name}}</a>, ({{size}})", value);
		    }
		}
		span.html(util.prep(value, ifnull));
		span.trigger("populated");
	    }
	    //populate inputs
	    const inputs = $target.find(':input[name="' + item + '"]');
	    if (inputs.length > 0){
		inputs.each(function(i, el){
		    const input = $(el);
		    // a class of no_populate causes populate to skip this field
		    // this can be useful in rare cases
		    if (input.hasClass('no_populate')) { return; };
		    if(input.attr("type") === 'checkbox'){
			// converting boolean data to an HTML checkbox is always fun.
			// it seems to work when you set the checkbox's value to '1' or 'true'
			if (
			    value === true
				|| (parseInt(value, 10) === parseInt(input.attr("value"), 10))
				|| (value !== null && value.length > 0 && value == input.attr("value"))
			){
			    input.prop("checked", true);
			    input.attr('checked', true); //toggling the attr as well fixes Printable output, since we clone the HTML without the DOM.
			}else{
			    input.prop("checked", false);
			    input.removeAttr('checked');
			}
		    }else if (input.prop("tagName").toLowerCase() == "select"){
			// SELECT>OPTION values are always strings, so we need to convert
			// bools and numbers to strings to match the SELECT
			if ((typeof value).match(/boolean|number/)){
			    value = value.toString();
			}
			input.val(value);
		    }else if (input.prop("tagName").toLowerCase() == "textarea"){
			// textareas just have to be different, don't they?
			input.html(value);
			// under some circumstances in Chrome, we also need to set the value
			input.val(value);
		    }else if (input.attr('type') === 'file'){
			//if there's a value, make the delete checkbox visible
			// and the file input invisible
			const dlabel_name = 'label[name="' + item +'_delete_label"]';
			const delete_label = $target.find(dlabel_name);
			if (value){
			    input.hide();
			    input.attr("disabled", "disabled");
			    delete_label.show();
			}else{
			    input.show();
			    input.removeAttr("disabled").val('');
			    delete_label.hide();
			}
			return;
		    }else{
			input.val(value);
		    }
		    input.trigger('populated');
		});
	    }
	});
	return $target;
    };

    /////////////////////
    // human_file_size //
    /////////////////////
    util.human_file_size = function(size){
	// Display a file size in a friendly way
	if (size < 1024){
	    return size + ' b';
	}else if (size < 1024 * 1024){
	    return (size / 1024).toFixed(2) + ' Kb';
	}else if (size < 1024 * 1024 * 1024){
	    return (size / (1024 * 1024)).toFixed(2) + ' Mb';
	}else{
	    return (size / (1024 * 1024 * 1024)).toFixed(2) + 'Gb';
	}
    };

    ///////////////////////////////////
    // increment_input_name_ordinals //
    ///////////////////////////////////
    util.increment_input_name_ordinals = function($container, matchtext){
	// Used in detail tables, where names are like '_details[table][N][somefield]'
	// where N is an integer
	// Finds and increments the value N
	const $input = $container.find(':input[name*=' + matchtext + ']');
	// also check for delete labels
	const $dlabel = $container.find('label[name*=' + matchtext + ']');
	const name = $input.attr('name');
	const ordinal = $input.attr('name').match(/\[(\d+)\]/)[1];
	const next = parseInt(ordinal, 10) + 1;
	const newname = name.replace(/\[(\d+)\]/, '[' + next + ']');
	$input.attr('name', newname);
	$dlabel.attr('name', newname + '_delete_label');
    };

    //////////////////////////
    // hide_hide_when_empty //
    //////////////////////////
    util.hide_hide_when_empty = function($container){
	//hide elements of class "hide_when_empty" if they are empty
	$container.find(".hide_when_empty").each(function(i, el){
	    var do_hide = true;
	    const $container = $(el);
	    const $inputs = $container.find("SPAN.ro_input");
	    $inputs.each(function(j, el){
		do_hide = do_hide && ($(el).is(":empty"));
	    });
	    if (do_hide){
		$container.hide();
	    }else{
		$container.show();
	    }
	});
    };

    //////////////////////
    // form_to_readonly //
    //////////////////////
    util.form_to_readonly = function($form){
	// converts a form to readonly state.
	// Used when a user has no write privileges or to generate printable versions

	$form.find(":input,textarea").each(function(i, input){
	    const type = $(input).attr("type");
	    if (type && type.toLowerCase() === 'search'){
		//continue
		return true;
	    }else{
		util.input_to_readonly($(input));
	    }
	    return true;
	});
	$form.find(".edit_only").hide();
	$form.find('SPAN.select2').remove();
	return $form;
    };

    ///////////////////////
    // input_to_readonly //
    ///////////////////////
    util.input_to_readonly = function($input){
	// Convert a form input to a readonly display
	// while preserving some metadata for formatting purposes

	const name = $input.attr("name");
	const tag = $input[0].tagName.toLowerCase();
	const type = (function(){
	    if ($input.hasClass('was_number_input')){
	    // wcgform changes number inputs to type text with a class of "was_number_input"
	    // preserve the fact that this was a number input by setting type back to "number"
	    // this won't affect the input widget, but will affect the "from__" class of the ro_input
		return 'number';
	    }else if ($input.attr("type")){
		return $input.attr("type").toLowerCase();
	    }else if (tag === 'input'){
		return 'text';
	    }else{
		return tag;
	    }
	})();
	var value = null;
	if (tag === 'select'){ // remove select2 trappings from SELECT tags
	    if ($input.hasClass('select2-hidden-accessible')){
		try{ // if the form was cloned, there might not actually be a select2 object
		    $input.select2('destroy');
		}catch (e){
		    $input.show();
		}
	    }
	    //get text, not HTML, to avoid symbols becoming entities
	    value = $input.find(":selected").text();
	}else{
	    value = $input.val();
	}
	// format dates and times
	if (type === 'date'){
	    value = util.date_format(value);
	}else if(type === 'datetime-local'){
	    value = util.human_date(value);
	}else if(type === 'time'){
	    value = util.time_format(value);
	}

	if ($.inArray(type, ['checkbox', 'radio', 'hidden']) === -1){
	    // for most inputs, replace with a named span of class 'ro_input'
	    $input.after(Mustache.render(
		'<span name="{{name}}" class="ro_input from_{{type}}">{{value}}</span>',
		{name: name, type: type, value: value}
	    ));
	    $input.remove();
	}else if($.inArray(type, ['checkbox', 'radio']) !== -1) {
	    // for radios and checkboxen, show a unicode box character
	    var box = '☐';
	    var check_class = 'unchecked';
	    if ($input.is(':checked')){
		box = '☒';
		check_class = 'checked';
	    }
	    $input.after(Mustache.render(
		'<span name="{{name}}" class="ro_input from_{{type}} {{check_class}}">{{ box }}</span>',
		{name: name, type: type, box: box, check_class: check_class}
	    ));
	    $input.remove();
	}else{
	    // for hidden fields, just disable them.
	    $input.attr("disabled", "disabled");
	}
    };

    ////////////////
    // confirmbox //
    ////////////////

    // This pops up a simple yes/no dialog which can either publish an event
    // or run a callback depending on the answer.

    util.confirmbox = function(options){
	// options should at least have:
	//  - title
	//  - message
	//  - either a yes_event or yes_callback
	//  - possibly a no_event or no_callback
	const title = options.title || "Confirm";
	$("#confirm_dialog").remove();
	$("<div />", {id: "confirm_dialog", title: title}).appendTo("BODY");
	const $dbox = $("#confirm_dialog");
	$dbox.html(options.message);
	$dbox.dialog({
	    modal: true,
	    buttons: [
		{ text : "Yes",
		  click : function(){
		      if (options.yes_event){
			  $.publish(options.yes_event);
		      }
		      if (options.yes_callback){
			  options.yes_callback();
		      }
		      $dbox.dialog("close");
		  }},
		{ text : "Cancel",
		  click : function(){
		      if (options.no_event){
			  $.publish(options.no_event);
		      }
		      if (options.no_callback){
			  options.no_callback();
		      }
		      $dbox.dialog("close");
		  }}
	    ]
	});
	return $dbox;
    };

    ///////////
    // popup //
    ///////////

    // A generic popup dialog function.  Can be "click to close" or optionally timeout
    util.popup = function(message, title, width, div, timeout){
	width = width || 'auto';
	div = div || 'dialog';
	$("#" + div).remove();
	$("<div />", {id: div, title: title}).appendTo("BODY");
	const $divobj = $("#"+div);
	$divobj.html(message);
	$divobj.on("click", "BUTTON.cancel", function(){
	    $divobj.dialog("close");
	    return false;
	});
	if (timeout){
	    window.setTimeout(function(){
		if ($divobj.is(":visible")){
		    $divobj.dialog("close");
		}
	    }, timeout);
	}
	return $divobj.dialog({
	    modal: true,
	    width:width,
	    show: {effect: "fold", duration: 100},
	    hide: {effect: "fold", duration: 100}
	});
    };

    ////////////////////////
    // show_request_error //
    ////////////////////////
    util.show_request_error = function(jqxhr, status, error){
	// This should be used as the "error" handler in all jQuery Ajax calls
	// It tries to show a meaningful error.
	// WCGFF mostly responds with 400, 403, and 500 errors.
	const http_code = jqxhr.status;
	const messages = {
	    400: "There was a problem with the data in your request.",
	    403: "You do not have permissions to make this request.",
	    500: "You found a bug!  Please contact technical support!",
	    default: "There was an unknown error with your request"
	};
	const displaydata = {
	    raw_message: messages[http_code] || messages.default,
	    details: jqxhr.responseJSON ? jqxhr.responseJSON.error : jqxhr.responseText,
	    timestamp: new Date().toString()
	};
	const message = Mustache.render(
	    "<p>{{raw_message}}</p><p>The actual message from the server was:</p><blockquote>{{{details}}}</blockquote><p>{{timestamp}}</p>",
	    displaydata
	);

	util.popup(message, "Error with request", 'auto', '_error');

    };

    ///////////////
    // link_urls //
    ///////////////
    // search for URLs and email addresses and automatically hyperlink them.
    util.link_urls = function(text){
	text = text.replace(/(http:\/\/\S*)/g, "<a target='_blank' href='$1'>$1</a>");
	text = text.replace(/(\w+@\w+\.\w+)/g, "<a href='mailto:$1'>$1</a>");
	return text;
    };

    /////////////////
    // date_format //
    /////////////////
    // Attempt to interpret a date string, then reformat it using
    // the locale date format.
    util.date_format = function(datestring){
	if (!moment(datestring).isValid()){
	    return datestring;
	}else{
	    return moment(datestring).format('l');
	}

    };

    /////////////////
    // time_format //
    /////////////////
    // Attempt to interpret a time string, then reformat it using
    // 12-hour time display

    util.time_format = function(timestring){
	if (!moment(timestring, 'HH:mm:ss').isValid()){
	    return timestring;
	}else{
	    return moment(timestring, 'HH:mm:ss').format('h:mm A');
	}
    };

    ////////////////
    // human_date //
    ////////////////
    // Attempt to interpret a date string, then format it into
    // a friendly string like "Yesterday at 3:15 PM"
    // suppress_time, if True, prints only the date portion
    util.human_date = function(datestring, suppress_time){
	//requires moment.js
	if (!datestring || datestring === "0000/00/00 00:00:00"){
	    return "";
	}else if (!moment(datestring).isValid()){
	    return datestring;
	}
	const dt = moment(datestring);
	const today = moment().startOf('day');
	const yesterday = moment().startOf('day').subtract(1, "days");
	const oneYearAgo = moment().startOf('day').subtract(1, "year");

	var output = "";
	if (dt.isSame(today, 'day')){
	    output = "Today";
	}else if (dt.isSame(yesterday, 'day')){
	    output = "Yesterday";
	}else{
	    output = dt.format("MMM Do");
	    if (dt <  oneYearAgo){
		output += dt.format(" YYYY");
	    }
	}
	if (!suppress_time){
	    output += " at " + dt.format("H:mm");
	}
	return output;
    };

    //////////////////
    // money_format //
    //////////////////

    // Format an arbitrary number like US currency.
    util.money_format = function(value, add_space, cents){
	//wanted to do a default arg, but IE doesn't do that.
	// cents defaults to true
	cents = (typeof cents === 'undefined') ? true: cents;

	//format a value as money
	if (value[0] !== '$' && value !== ''){
	    value = Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
		useGrouping: true,
		maximumFractionDigits: (cents ? 2 : 0),
		minimumFractionDigits: (cents ? 2 : 0)
	    }).format(value);
	    if (add_space){
		value = value.replace('$', '$ ');
	    }
	}
	return value;
    };

    //////////
    // prep //
    //////////
    // HTML-ize a piece of text.  Mostly just adds breaks and hyperlinks URLs

    util.prep = function(text, ifnull){
	//Preps text for HTML display
	if (typeof(text) === "string"){
	    text = util.link_urls(text);
	    text = text.replace(/\n/g, '<br />\n');
	}else if(text === null){
	    text = ifnull || '';
	}
	return text;
    };

    /////////////////
    // toTitleCase //
    /////////////////
    util.toTitleCase = function(str){
	return str.replace(
		/\w\S*/g,
	    function(txt){
		return txt.charAt(0).toUpperCase() +
		    txt.substr(1).toLowerCase();}
	);
    };

    ////////////////////////
    // popup_form_handler //
    ////////////////////////
    util.popup_form_handler = function($context, button_id, form, options){
	//not used by wcg_flask_framework, but commonly used by instances
	// Arguments:
	// - $context: the jQuery object that contains the triggering button.  Usually a recordview
	// - button_id:  the #id of the button that triggers the form
	// - form:  the name of the form that is popped up.
	//          Should be the name of the file in templates/forms (without the .jinja2)
	//options:
	// - keyname: name of a property of $context whose value is appended to the form url path
	// - wcgform: object of options to be passed to wcgform
	// - form_args: object of arguments to add to form URL
	const pfh = this;

	pfh.form_id = "#" + form;
	pfh.button_id = button_id;
	pfh.form_name = form;
	pfh.init = function(e){
	    e.preventDefault();
	    var key = $context[options.keyname];
	    var url = util.mkurl(['forms', form, key], options.form_args);
	    $.get(url, function(html){
		pfh.$pup = util.popup (html, $(html).attr("title"));
		pfh.$form = pfh.$pup.find(pfh.form_id);
		$.publish(form+"_shown");

		// if the form has a data-json_populator value attribute,
		// call that json function and populate the form with that
		// data.
		pfh.json_populator = pfh.$form.data("json_populator");
		if (pfh.json_populator){
		    $.getJSON(
			util.mkurl(['json', pfh.json_populator], {key: key}),
			function(data){
			    util.populate_data(pfh.$form, data);
			});
		}
		pfh.$form.wcgform(options.wcgform || {});
		$.subscribe(pfh.$form.submit_event_name, function(){
		    // close the dialog on form submit
		    if (pfh.$pup.dialog('instance')){
			pfh.$pup.dialog('close');
		    }
		});
	    });
	};
	$context.off("click", button_id);
	$context.on("click", button_id, pfh.init);

	return pfh;
    };

    ////////////////////
    // printable_page //
    ////////////////////
    // take a jQuery object and open it in a new tab as a printable version of the node
    util.printable_page = function($node){
	//has to be assigned to document because of an IE issue
	document.printable_content = $node.clone(true)[0];
	if ($node.hooks && $node.hooks.printable_prep){
	    $node.hooks.printable_prep.map(function(f, i){
		f(document.printable_content);
	    });
	}
	const pp = window.open(util.mkurl(["forms", "printable"]), "Printable_page");
	if (!pp){
	    alert("Could not open a new window.  Please make sure your popup blocker is turned off for this website.");
	}
	pp.printable_content = document.printable_content;

	return pp;
    };


    ///////////////////////////////
    // match_radiobutton_heights //
    ///////////////////////////////
    // little fix to jquery radio buttons to make sure they are uniform in height
    util.match_radiobutton_heights = function($radiobuttonlist){
	//assumes radio buttons are in a list and have had jquery-ui .button() applied
	if ($radiobuttonlist.length === 0) return;
	const $buttonlabels = $radiobuttonlist.closest("LI").find("LABEL");
	const button_heights = $buttonlabels.map(function(){
	    return $(this).height();
	}).get();
	var buttons_maxheight = Math.max.apply(null, button_heights);
	$buttonlabels.each(function(i, el){$(el).css({"height":buttons_maxheight});});
    };

    //////////////////
    // toggle_input //
    //////////////////
    // enable or disable an input
    util.toggle_input = function($input, enabled){
	$input.attr("disabled", !enabled);
	if ($input.is('.ui-button')){
	    $input.button('option', 'disabled', !enabled);
	}
    };
})();


//////////////////////
// jQuery additions //
//////////////////////


(function($){
    $.fn.extend({
	/////////////////////////////////
	// replace_input_name_ordinals //
	/////////////////////////////////
	replace_input_name_ordinals: function(new_number){
	    const $record = this;
	    const ord_regex = new RegExp("\\[(-?\\d+)\\]");
	    $record.find(":input,SPAN,LABEL").each(function(i, input){
		var $input = $(input);
		if ($input.attr("name")){
		    var newname = $input.attr("name").replace(ord_regex, "["+ new_number + "]");
		    $input.attr("name", newname);
		    $input.attr('id', newname);
		}
	    });
	    return $record;
	}
    });
})(jQuery);
