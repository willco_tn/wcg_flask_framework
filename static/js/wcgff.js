/////////////////////////////////////////
// WCG Flask Framework Main Javascript //
/////////////////////////////////////////

/* global $ WCGRecordView WCGRecordList WCGRecordSearch WCGReportingApp util */
// instances can override this for more power.
document.options = {
    recordlist_height: '70vh',
    recordview: {
        wcgform: { }
    },
    recordsearch: {
        keywordsearch: {
            wcgform: { }
        },
        fieldsearch: {
            wcgform: { }
        }
    },
    reportingapp: {
        prepared: {
            wcgform: { }
        },
        custom: {
            wcgform: { }
        }
    }
};


function WCGFF(){
    var wcgff = {};

    //hash navigation
    wcgff.hash_nav = function(e){
        e.preventDefault();
        var hash = window.location.hash.replace("#", '').split('-');
        wcgff.dataset = hash[0];
        wcgff.record_id = hash[1];
        $.publish('record_requested', hash);
        return false;
    };

    $(window).on('hashchange', wcgff.hash_nav);

    return wcgff;
}

function LoginBox(selector){
    var $lb = $(selector);
    if ($lb.length === 0){
        return null;
    }
    var hash = window.location.hash;
    $lb.append($("<input />", {
        name: 'attempted_hash',
        type: 'hidden',
        value: hash
    }));
    var action = $lb.attr("action");
    $lb.attr("action", action + hash);

    return $lb;
}



$(document).ready(function(){
    // Turn off caching for Ajax calls.  We want fresh data!
    $.ajaxSetup({ cache: false });

    document.wcgff = WCGFF();
    document.wcgff.loginbox = LoginBox('FORM[name=login]');
    document.wcgff.recordview = WCGRecordView('#recordview', document.options.recordview);
    document.wcgff.recordlist = WCGRecordList('#main_view', {ajax: true, height: document.options.recordlist_height});
    document.wcgff.search = WCGRecordSearch('#recordsearch', document.options.recordsearch);
    document.wcgff.reports = WCGReportingApp('#reportapp', document.options.reportingapp);

    //in case there's a hash, check it.
    $(window).trigger('hashchange');

    //global subscriptions
    $.subscribe('printable_page_requested', function(e, $node){
        util.printable_page($node);
    });

});
