///////////////////////////////////////////////
// WCG Flask Framework RecordView definition //
///////////////////////////////////////////////

/* global $  util Mustache */

function WCGRecordView (selector, options){

    var $rv = $(selector);
    if ($rv.length == 0){
        return null;
    }

    $rv.dataset = '';
    $rv.record_id = '';
    $rv.hooks = {};

    $rv.dialog_options = {
        title: '',
        width: '90%',
        beforeClose: function(){window.location='#';},
        open: function(){$.publish('recordview_shown', [$rv]);},
        position: {my: 'top', at: 'bottom', of: $('#navigation'), collision: 'none'}
    };

    $rv.get_form = function(){
        //retreive the basic html form from the server
        $.ajax({
            url: util.mkurl(['forms', $rv.dataset, $rv.record_id]),
            cache: false,
            success: function(html){
                $rv.html(html);
                $rv.form = $rv.find('.record_form');
                //reset the form, so firefox won't do any autofill shenanigans (hopefully...)
                $rv.form[0].reset();
                $rv.dialog_options.width = $rv.form.attr('width');
                $rv.form.wcgform($.extend({
                    new: $rv.record_id === 'new',
                    dataset: $rv.dataset,
                    record_form: true,
                    json_response: true,
                    refresh_on_submit: false,
                    require_dirty: true
                }, options.wcgform));
                $.unsubscribe($rv.form.submit_event_name + '.get_form');
                $.subscribe($rv.form.submit_event_name + '.get_form', $rv.handle_form_submit);
                $rv.printable_btn = $rv.find('#printable_form');
                $rv.printable_btn.on('click keypress', function(){
                    $.publish('printable_page_requested', [$rv]);
                });
                $.publish('recordview_form_loaded', [$rv]);
            }
        });
    };

    $rv.get_data = function(){
        //get the JSON data for this record from the server
        if (!$rv.dataset || !$rv.record_id){
            return;
        }
        var params = {dataset: $rv.dataset, key: $rv.record_id};
        $rv.form.record_id = $rv.record_id;
        $.ajax({
            url: util.mkurl(['json', 'record_details'], params),
            cache: false,
            dataType: 'json',
            success: function(json){
                $rv.data = json;
                $.publish('recordview_data_loaded', [$rv]);
            },
            error: function(jqXHR, textStatus, errorThrown){
                if (jqXHR.status === 404){
                    window.location.hash = '';
                }else{
                    util.show_request_error(jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

    $rv.populate_data = function(){
        // set the title
        $rv.dialog_options.title = $rv.form.attr('title') + $rv.record_id;

        //merge the data into the form
        util.populate_data($rv, $rv.data, null, null, $rv.form.select_opts);
        if ($rv.data._details){
            $.each($rv.data._details, function(table, data){
                var $de = $rv.form.detail_entries[table];
                if ($de){
                    $de.populate(data);
                }
            });
        }
        $.publish('recordview_data_populated', [$rv]);
    };

    $rv.handle_form_submit = function(e, response){
        util.popup('The record was saved', 'Success', null, 'temp-popup', 2000);
        if ($rv.record_id == 'new'){
            var record_id = response[0].id;
            if (record_id.toString() !== $rv.record_id){
                var newhash = Mustache.render(
                    '#{{dataset}}-{{record_id}}',
                    {dataset: $rv.dataset, record_id: record_id}
                );
                $rv.dataset = null; // to trigger a form refresh
                window.location.hash = newhash;
            }
        }else{
            $.publish('data_refresh');
        }
    };

    //subscriptions
    $.subscribe('record_requested', function(e, dataset, record_id){
        const loadform = dataset !== $rv.dataset || record_id == 'new';
        const hideme = !record_id;
        $rv.dataset = dataset;
        $rv.record_id = record_id ? record_id.toString() : null;
        if (hideme && $rv.dialog_obj){
            $rv.dialog('close');
            $.publish('data_refresh');
        }else if (loadform){
            $rv.get_form();
        }else{
            $.publish('recordview_form_loaded', [$rv]);
        }
    });

    $.subscribe('recordview_form_loaded', function(e){
        if ($rv.record_id !== 'new'){
            $rv.get_data();
        }else{
            $rv.data = {};
            $.publish('recordview_data_loaded', [$rv]);
        }

    });

    $.subscribe('recordview_data_loaded', function(e){
        $rv.populate_data();
    });

    $.subscribe('recordview_data_populated', function(e){
        $rv.dialog_obj = $rv.dialog($rv.dialog_options);
    });


    $.subscribe('data_refresh', function(e, response){
           if ($rv.dialog_obj && $rv.is(':visible')){
               $rv.get_data();
           }
    });

    return $rv;
}
