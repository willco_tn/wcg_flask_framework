//////////////////////////////
// WCG Flask Framework      //
//////////////////////////////

/* global jQuery $ util moment */

(function($){
    $.fn.extend({
        wcgform: function(options){
            //Options:
            // - datepicker_options: object to be passed to jquery datepicker
            // - datetimepicker_options: object to be passed to datetimepicker
            // - pre_submit: list of hooks to be called before form submission
            // - init_widget: list of hooks to be called repeatedly after widgets are initialized
            // - init_form: list of hooks to be called once after the form is initialized
            // - dataset: the dataset for the form
            // - normal_submit: a value or function that, if evaluated to true, indicates non-ajax submit
            // - submit_event_name: Name of event to publish when form successfully submitted
            // - fail_event_name: Name of event to publish when form submission fails
            // - refresh_on_submit: publish a data_refresh on successful submit.  Default true.
            // - recordview_form: set True if this is a form displaying a record with data.
            // - json_response: set True if the submit action returns JSON
            // - require_dirty: set True to disable submit until an input has changed
            // - select2: set True to use select2 for selects.
            var $form = this;
            options = options || {};
            //public inteface
            $form.wcg = {};
            $form.dataset = options.dataset;
            $form.record_id = options.record_id;

            $form.hooks = {
                pre_submit: options.pre_submit || [],
                init_widget: options.init_widget || [],
                init_form: options.init_form || [],
                printable_prep: options.printable_prep || []
            };

            $form.id = $form.attr('id') || 'unnamed_form';

            $form.submit_event_name = options.submit_event_name || 'form_submit_succeeded.' + $form.id;
            $form.fail_event_name = options.fail_event_name || 'form_submit_failed.' + $form.id;
            $.unsubscribe($form.submit_event_name);
            $.unsubscribe($form.fail_event_name);
            $.unsubscribe('form_changed.wcgff');

            if (options.refresh_on_submit !== false){
                $.subscribe($form.submit_event_name, function(e, response){

                    $.publish('data_refresh', [response]);

                });
            }

            if (options.new){
                $form.find('.existing_only').hide();
                $form.prepend($('<input/>', {type: 'hidden', name: 'new_record', value: '1'}));
            }else{
                $form.find('.new_only').hide();
            }

            // Save SELECT values so the form can be populated correctly in RO mode
            $form.select_opts = {};
            $form.find('SELECT').each(function(i, el){
                const $el = $(el);
                const name = $el.attr('name');
                $form.select_opts[name] = {};
                $el.find('OPTION').each(function(i, opt){
                    $form.select_opts[name][$(opt).val()] = $(opt).text().trim();
                });
            });

            //readonly
            $form.to_readonly = function(){
                util.form_to_readonly($form);
                util.hide_hide_when_empty($form);
            };

            if ($form.attr('readonly') === 'readonly'){
                // make sure to unsubscribe since we are referencing $form
                $.unsubscribe('recordview_data_populated.' + $form.id + '.readonly');
                $.subscribe('recordview_data_populated.' + $form.id + '.readonly', $form.to_readonly);
            }

            //form can be globally disabled or enabled
            $form.wcg.toggle_inputs = function(enabled, reason, $scope){
                $scope = $scope || $form;

                if (!$scope.is(":input")){
                    $scope = $scope.find(":input:not(#deletecontrol INPUT, INPUT[type=submit], INPUT[type=hidden])");
                }
                if (enabled && reason){
                    $scope = $scope.filter("[disabled_by=" + reason +"]");
                }else if (!enabled){
                    //don't touch things already disabled
                    $scope = $scope.filter(":not(:input[disabled])");
                }
                $scope.each(function(i, el){
                    util.toggle_input($(el), enabled);
                    if (enabled){
                        $(el).removeAttr("disabled_by");
                    }else{
                        $(el).attr("disabled_by", reason);
                    }
                });
            };

            //check for a dirty form
            $form.dirty = !options.require_dirty;
            $form.check_dirty = function(){
                if (!options.require_dirty){
                    return;
                }
                var $submit_btn = $form.find(':input[type=submit]');
                $form.wcg.toggle_inputs($form.dirty, 'form_not_dirty', $submit_btn);
                $form.toggleClass('_dirty', $form.dirty);
            };
            $form.check_dirty();

            //handle dirty form detection
            $form.on_edit = function(e){
                $form.dirty = true;
                $form.check_dirty();
            };
            $form.off('keypress change', ':input[name]', $form.on_edit);
            $form.on('keypress change', ':input[name]', $form.on_edit);
            $.subscribe($form.submit_event_name, function(){
                $form.dirty = !options.require_dirty;
                $form.check_dirty();
            });

            //configure new form
            $form.wcg.init_widgets = function($context){
                $context = $context || $form;
                //form submit button
                $context.submit_button = $context.find('input[type=submit]');
                $context.submit_button.original_text = $context.submit_button.val();

                // widget initialization: should be safely repeatable.
                $context.datepicker_options = $.extend(
                    {},
                    options.datepicker,
                    document.wcgff_datepicker_options,
                    {
                        minDate: new Date(1500, 1, 1),
                        maxDate: new Date(2200, 1, 1)
                    }
                );

                $context.datetimepicker_options = $.extend(
                    {},
                    options.datetimepicker,
                    document.wcgff_datetimepicker_options,
                    {
                        timeFormat: "hh:mm tt",
                        controlType: "select",
                        oneLine: true,
                        minDate: new Date(1500, 1, 1),
                        maxDate: new Date(2200, 1, 1)
                    }
                );

                //autocomplete fields
                $context.find(":input.autocomplete").each(function(i, el){
                    var $el = $(el);
                    $el.autocomplete({
                        // use a source if specified, else the automated autocompleter
                        source: (
                            $el.attr("autocomplete_source") ?
                                util.mkurl(['json', $el.attr('autocomplete_source')]) :
                                util.mkurl(['json', 'autocomplete'], {dataset: $context.dataset, fieldname: $el.attr("name")})
                        ),
                        appendTo: $context
                    });
                });

                // Add a calendar widget to date fields, but only when
                // it's not supported by the browser.
                $context.find(":input[type=date]").filter(
                    function(index){
                        return this.type !== 'date';
                    }).each(function(i, el){
                        var $di = $(el);
                        $di.datepicker($context.datepicker_options);
                        $di.attr('min', $di.datepicker('option', 'minDate'))
                            .attr('max', $di.datepicker('option', 'maxDate'));
                        $di.off('blur');
                        $di.on('blur', function(e){
                            el.setCustomValidity(
                                !el.value || moment(el.value).isValid() ? '' : 'Enter a valid date'
                            );
                        });
                    });


                // Likewise with datetime fields, add a timepicker.
                $context.find(":input[type=datetime],:input[type=datetime-local]").filter(
                    function(index){
                        return this.type !== 'datetime' || this.type !== 'datetime-local';
                    }).each(function(i, el){
                        var $di = $(el);
                        $di.datetimepicker($context.datetimepicker_options);
                        $di.attr('min', $di.datetimepicker('option', 'minDate'))
                            .attr('max', $di.datetimepicker('option', 'maxDate'));
                        $di.off('blur');
                        $di.on('blur', function(e){
                            el.setCustomValidity(
                                !el.value || moment(el.value).isValid() ? '' : 'Enter a valid date/time.'
                            );
                        });
                    });

                // Enforce number widgets, even in browsers that don't do this right.
                $context.find(":input[type=number], :input.was_number_input").each(function(i, el){
                    var $el = $(el);
                    var step = parseFloat($el.attr("step"));
                    if (isNaN(step)){
                        step = .01;
                    }
                    var alias = (step % 1 === 0) ? 'integer' : 'decimal';
                    var min = parseFloat($el.attr("min"));
                    var max = parseFloat($el.attr("max"));

                    if (isNaN(min)){
                        min = undefined;
                    }
                    if (isNaN(max)){
                        max = undefined;
                    }
                    //inputMask does not work on type=number, but we need to remember that
                    // this was a number input so we can reinstate the inputmask if the
                    // field gets copied in the DOM.
                    if ($el.prop('type') == 'number'){
                        $el.prop('type', 'text');
                        $el.addClass('was_number_input');
                    }
                    $el.inputmask({alias: alias, step: step, min: min, max: max, positionCaretOnClick:'select', positionCaretOnTab: false, nullable: true});
                });


                // If any other widgets have data-inputmask properties, create the inputmask
                $context.find(":input[data-inputmask]").inputmask();


                //button-ify buttons, radios, etc.
                $context.find('button, input[type=submit]').filter(':not(.nojqui)').button();
                $context.find('input[type=radio]').filter(':not(.nojqui)').button();

                //jquery-ui selectmenu is... bizarre.  Leaving this here to remember not to use it.
                //$context.find('select').filter(':not(.nojqui)').selectmenu();
                //select2 for selects instead.
                if (options.select2){
                    $context.find('select').not('.noselect2').each(function(i, el){
                        $(el).select2(
                        {
                            width: "16em",
                            "minimumResultsForSearch": 5
                        });
                        // fixes a bug where select2 doesn't always show the selected value
                        // When we populate an input, emit a 'change.select2' event.
                        // We namespace the change event so it doesn't mess up the dirty status
                        $(el).on('populated', function(){$(el).trigger('change.select2');});
                    });
                }

                //requiresets
                //This feature allows us to require at least one (but not all) of a set of fields
                $context.requiresets = {};
                $context.find(":input[data-requireset_id]").each(function(i, el){
                    var $input = $(el);
                    $input.on("keyup change", function(){
                        var satisfied = false;
                        var set_id = $(this).data('requireset_id');
                        $context.requiresets[set_id].map(function($el){
                            if ($el.val() !== null && $el.val().length > 0){
                                satisfied = true;
                            }
                        });
                        if (satisfied){
                            $context.find(":input[data-requireset_id=" + set_id + "]").removeAttr('required');
                        }else{
                            $context.find(":input[data-requireset_id=" + set_id + "]").attr('required', 'required');
                        }
                    });

                    var rsid = $input.data("requireset_id");
                    $context.requiresets[rsid] = $context.requiresets[rsid] || [];
                    $context.requiresets[rsid].push($input);


                });

                //deletecontrols
                $context.find('#deletecontrol INPUT[type=checkbox]').wcgdeletecontrol($context);


                if ($context === $form){
                    //init hooks
                    for (var i in $context.hooks.init_widget){
                        $context.hooks.init_widget[i]($context);
                    }
                }

                //bound spans
                // setup bound spans.  These are spans that are bound to an input so its value
                // can be displayed elsewhere on the form.
                // Options:
                //   data-bindto:  The name of the input to be bound to
                //   data-default:  Default value if the input is empty
                $context.find('[data-bindto]').each(function(i, el){
                    const $span = $(el);
                    const input_name = $span.data('bindto');
                    const $input = $(":input[name=" + input_name + "]");
                    const default_value = $span.data('default');

                    if ($input.length == 0){
                        console.warning('Could not find input named "' + input_name + '" to bind to.');
                        return;
                    }

                    $context.on("change populated", ":input[name=" + input_name + "]", function(){
                        var value = $(this).val();
                        if (value){
                            $span.html(value);
                        }else{
                            $span.html(default_value);
                        }
                        $span.trigger('populated');

                    });
                });

            }; //end widget init
            $form.wcg.init_widgets();

            $.subscribe('form_changed.wcgff', function(e, $context){
                $form.wcg.init_widgets($context);
            });


            //detail handling
            $form.detail_entries = {};
            $form.find(".detail_entry").each(function(i, el){
                var name = $(el).prop('id');
                var display_only = $(el).data('display-only');
                $form.detail_entries[name] = $(el).wcgdetailentry($form, {display_only: display_only, recordview_form: options.recordview_form});
            });

            //handle a submission
            $form.wcg.submit = function(e){
                if (options.require_dirty && !$form.dirty){
                    e.preventDefault();
                    return false;
                }
                for (var i in $form.hooks.pre_submit || []){
                    $form.hooks.pre_submit[i]($form);
                }
                // check if options.normal_submit is a function
                // if it is, call it.  Otherwise, treat as a bool
                var normal_submit = (options.normal_submit && 'call' in options.normal_submit) ? options.normal_submit() : options.normal_submit;
                if (normal_submit){
                    return true;
                }else{
                    e.preventDefault();
                    $form.submit_button.button('option', 'disabled', true);
                    $form.submit_button.val('Please wait...');
                    var ajax_opts = {
                        url: $form.attr("action"),
                        method: $form.attr("method") || 'GET',
                        success: function(response, status, jqxhr){
                            $form.submit_button.button('option', 'disabled', false);
                            $form.submit_button.val($form.submit_button.original_text);
                            if(options.json_response){
                                response = $.parseJSON(response);
                            }
                            $.publish($form.submit_event_name, [response, status, jqxhr]);
                        },
                        error: function(jqXHR, textstatus, errorThrown){
                            $form.submit_button.button('option', 'disabled', false);
                            $form.submit_button.val($form.submit_button.original_text);
                            $.publish($form.fail_event_name, [errorThrown, textstatus, jqXHR]);
                        }
                    };

                    // fix checkboxes so that they send a false value when unchecked
                    $form.find('INPUT[type=checkbox]')
                        .not(':checked')
                        .not('[name="_delete"]')
                        .not('.detail_delete')
                        .not('.file_delete')
                        .each(function(i, el){
                            const $el = $(el);
                            const name = $(el).attr('name');
                            $el.append($('<input>', {name: name, type: 'hidden', value: 0, class: 'temp_hidden_inp'}));
                    });
                    if ($form.find("INPUT[type=file]").length > 0){
                        //we handle this differently, because it has file uploading
                        ajax_opts.data = new FormData($form[0]);

                        // We need to fix a problem using FormData with inputs that have a jquery-inputmask
                        $form.find(':input[data-inputmask]').each(function(i, el){
                            const $input = $(el);
                            ajax_opts.data.set($input.attr('name'), $input.val());
                        });

                        ajax_opts.cache = false;
                        ajax_opts.contentType = false;
                        ajax_opts.processData = false;
                    }else{
                        ajax_opts.data = $form.serialize();
                    }
                    $.ajax(ajax_opts);

                    // cleanup temporary inputs
                    $form.find('input.temp_hidden_inp').remove();
                    return false;
                }
            };
            $form.on('submit', $form.wcg.submit);
            $.subscribe('form_submit_failed.' + $form.id, function(e, error, status, jqxhr){
                util.show_request_error(jqxhr, status, error);
            });

            for (var i in $form.hooks.init_form || []){
                $form.hooks.init_form[i]($form);
            }

            return $form;
        },
        //detail entries
        wcgdetailentry: function($form, options){
            //options:
            // - display_only: only show values instead of widgets.  Values are set by some outside means (popup form,e.g.)
            var $de = $(this);
            options = options || {};
            $de.wcg = {};
            var template_node = $de.find('.template:last-child');
            try{
                //we need to clean up select2 before cloning the template.
                template_node.find('SELECT').select2('destroy');
                template_node.find('SELECT').removeAttr('data-select2-id');
                template_node.find('OPTION').removeAttr('data-select2-id');
            }catch(e){
                //just do nothing
            }
            $de.wcg.template = template_node.clone();
            $de.wcg.name = $de.prop('id');
            $de.wcg.display_only = options.display_only;
            $de.find('.template').remove();

            $de.set_record_dirty = function($record, dirty){
                $record.find('input[type=hidden][name *="[_dirty]"]').val(dirty? 1 : '');
            };

            $de.populate = function(data, options){
                //Options:
                //  - populate_as_new allows you to populate detail entries, but
                //    leave them as new items.  This is useful if you are populating
                //    a w record from another table.
                options = options || {};
                var $template = $de.wcg.template.clone();
                $template.removeClass("edit_only");
                $template.removeClass("never_show");
                if (!options.populate_as_new){
                    $template.find("INPUT[name*=\"[new_record]\"]").remove();
                    $template.find("INPUT[name*=\"[_delete]\"]")
                        .attr("type", "checkbox")
                        .val("1")
                        .attr("id", "DELETEBOX_ID")
                        .addClass("detail_delete")
                        .after("<label class='inline detail_delete' for='DELETEBOX_ID'>Delete</label>")
                    ;
                }else{
                    $template.find('INPUT[name*="[_dirty]"]').val("1");
                }

                $de.html('');
                data.map(function(rowvalues, record_no){
                    var $new_row = $template.clone();
                    $new_row.replace_input_name_ordinals(record_no);
                    $new_row.html(function(i, old_html){
                        var new_id = $(old_html).find("input[name$='[_delete]']").attr("name");
                        return old_html.replace(/DELETEBOX_ID/g, new_id);
                    });
                    var name_template = "_details[" + $de.wcg.name + "]["+record_no+"][$NAME$]";

                    //if the row has any data-* keys, try to populate from the rowvalues
                    for (var key in $new_row.data()){
                        if ((typeof rowvalues[key]) !== 'undefined'){
                            $new_row.data(key, rowvalues[key]);
                            $new_row.attr('data-' + key, rowvalues[key]);
                        }
                    }
                    //append the row
                    $de.append($new_row);
                    util.populate_data($new_row, rowvalues, null, name_template);
                    $new_row.show();
                });
                if (!$de.wcg.display_only){
                    $de.append_input_row();
                }
            };

            $de.append_input_row = function(){
                var $empty_row = $de.wcg.template.clone();
                var $last_row = $de.find('.template:last-child');
                var old_ordinal;
                if ($last_row.length === 0){
                    old_ordinal = -1;
                }else{
                    old_ordinal = parseInt($last_row.find(':input:first').attr("name").match(/\[(-?\d+)\]/)[1], 10);
                }
                if ((typeof old_ordinal) == 'null'){
                    old_ordinal = -1;
                    //console.log('old ordinal null', $last_row.find(':input:first').attr("name"));
                }
                $empty_row.replace_input_name_ordinals(old_ordinal + 1);
                //set the record non-dirty just to be sure
                $de.set_record_dirty($empty_row, false);
                //Add the original template to the end
                $de.append($empty_row);
                $.publish('form_changed', [$de]);
                $.publish('detail_row_appended', [$de, $de.find('.template:last-child'), $last_row]);
            };

            //if this is not a recordview form, populate will never be called.  Append a blank row.
            if (!options.recordview_form){
                $de.append_input_row();
            }

            $de.on("keyup change", ".template:last-child :input.trigger_new", function(){
                var $input = $(this);
                $input.record = $input.closest('.template');
                if ($input.val().length > 0){
                    //mark the row dirty
                    $de.set_record_dirty($input.record, true);
                    //generate a fresh record for the next entry
                    $de.append_input_row();
                }else{
                    $de.set_record_dirty($input.record, false);
                }
            });

            $de.on("change", ":input", function(){
                $de.set_record_dirty($(this).closest('.template'), true);
            });

            $de.on("change", ":input[name *=\"[_delete]\"]", function(){
                //mark form fields disabled when the record is selected for deletion
                var checked = $(this).is(":checked");
                var $record = $(this).closest(".template");
                if (checked){
                    $record.find(":input:not([name *=\"[_delete]\"],[type=hidden])").each(function(i, el){
                        $(el).attr("disabled", "disabled");
                    });
                }else{
                    $record.find(":input:not([name *=\"[_delete]\"])").each(function(i, el){
                        $(el).removeAttr("disabled");
                    });
                }
            });

            return $de;
        },

        wcgdeletecontrol: function($form){
            //controls that are used for deletion have special behavior
            var $dc = $(this);
            var $dw = $('#deletewarning');
            $dc.on('change', function(){
                var checked = $dc.is(":checked");
                if(checked){
                    $dc.addClass("active");
                    $dw.slideDown();
                    $form.wcg.toggle_inputs(false, 'deletecontrol');
                }else{
                    $dc.removeClass('active');
                    $dw.slideUp();
                    $form.wcg.toggle_inputs(true, 'deletecontrol');
                }
            });

            return this;
        },

        wcgfield_parameter_form: function($form){
            var $fpf = $(this);

            $fpf.search_field_select_templ = $fpf.find('SELECT.search-field-select').closest('li').clone();

            $fpf.on('change', 'SELECT.search-field-select', function(){
                var $select = $(this);
                var $li = $select.closest('li');
                var fname = $select.val();
                // replace this LI
                if ($select.hasClass('select2-hidden-accessible')){
                    //destroy select2 objects since we're removing the select
                    $select.select2('destroy');
                }
                var template = $fpf.find('script[type=template][name="' + fname + '"]').get(0).innerHTML;
                $li.html(template);
                $li.attr('name', fname).addClass('search-field');
                $fpf.append_search_field_select();
                $fpf.init_field_widgets();

            });

            $fpf.init_field_widgets = function(){
                $fpf.op_selects = $fpf.find('.operatorselect');
                $fpf.op_selects.on('change', function(){
                    const $context = $(this).closest('LI');
                    const $value_input = $context.find(":input[name *=\"[value]\"]").not(this);
                    const $value2_input = $context.find(":input[name *=\"[value2]\"]").not(this);
                    const operator = $(this).val();

                    // these operators do not take an argument
                    const no_arg_ops = ['is_null', 'is_blank', 'is_not_null', 'is_not_blank'];
                    // these operators require two arguments
                    const two_arg_ops = ['between', 'not_between', 'size_between', 'size_not_between'];
                    if(operator !== ""){ // when an operator is selected
                        //enable the value field, unless it's a no-argument operator
                        if (no_arg_ops.indexOf(operator) == -1){
                            $value_input.removeAttr("disabled");
                        }else{
                            $value_input.attr('disabled', 'disabled');
                        }
                        //possibly enable the second value field
                        if (two_arg_ops.indexOf(operator) >= 0){
                            $value2_input.removeAttr("disabled");
                        }else{
                            $value2_input.attr("disabled", "disabled");
                        }

                        // make selects multi-selects if needed
                        if (operator.endsWith('equals_any')){
                            $value_input.attr("multiple", true);
                        }else{
                            $value_input.removeAttr("multiple");
                        }
                        $form.wcg.init_widgets($context.find('.parameter_values'));
                    }else{
                        $value_input.attr("disabled", "disabled").removeAttr("multiple");
                        $value2_input.attr("disabled", "disabled");
                    }
                });
                // if this is an "explicit" rather than dynamic field list, we need to intialize the widgets
                // Dynamic forms will intialize widgets when a new field is appended.
                if ($fpf.hasClass('explicit')){
                    $form.wcg.init_widgets($fpf);
                }
                $fpf.op_selects.each(function(i, el){$(el).trigger('change');});
            };

            $fpf.init_field_widgets();

            $fpf.append_search_field_select = function(){
                var $new_fsel_li = $fpf.search_field_select_templ.clone();
                // prep new selector
                // disable already-added fields
                $fpf.find('LI.search-field').each(function(i, el){
                    var fname = el.getAttribute('name');
                    $new_fsel_li.find('OPTION[value="' + fname + '"]').attr('disabled', 'disabled');
                });
                $fpf.find('.search-field-select').closest('li').remove();
                if ($new_fsel_li.find('.search-field-select OPTION:not([disabled])').length > 0){
                    $fpf.append($new_fsel_li);
                    $form.wcg.init_widgets($new_fsel_li);
                }
            };


            $fpf.on('click', 'BUTTON.field-remove', function(e){
                e.preventDefault();
                var $li = $(this).closest('li');
                var fname = $li.attr('name');
                $li.remove();
                $fpf.append_search_field_select();
            });

        }//end wcgfield_parameter_form


    });
})(jQuery);
