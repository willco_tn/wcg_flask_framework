///////////////////////////////////////////////
// WCG Flask Framework RecordList definition //
///////////////////////////////////////////////

/* global $ util */

function WCGRecordList (selector, options){

    //options:
    // ajax: t/f whether the source is ajax
    // ajax_args: query args to send with ajax URL
    // height: How tall the list can be

    options = options || {};
    var $rl = $(selector);
    if ($rl.length == 0){
        return null;
    }
    $rl.dataset = $rl.data('dataset');
    $rl.table_headers = [];
    $rl.find('>THEAD TH').each(function(){
        $rl.table_headers.push({data: $(this).text()});
    });
    $rl.new_button = $("#new_record");


    $rl.save_dt_scroll = function(){
        $rl.dt_scroll_top =  $rl.scrollbody.scrollTop();
        $rl.dt_scroll_left = $rl.scrollbody.scrollLeft();
        $rl.current_page = $rl.datatable.page();
    };

    $rl.restore_dt_scroll = function(){
        if ($rl.scrollbody){
            //scrollbody doesn't exist until after datatables has initialized.
            $rl.scrollbody.scrollTop($rl.dt_scroll_top || 0);
            $rl.scrollbody.scrollLeft($rl.dt_scroll_left || 0);
            //$rl.datatable.page($rl.current_page).draw('page');
        }
    };

    $rl.save_dt_page = function(){
        //save the current page.  This must be a preDrawCallback
        if ($rl.datatable && $rl.datatable.page()){
            $rl.current_page = $rl.datatable.page();
        }
    };
    $rl.restore_dt_page = function(){
        if ($rl.datatable && $rl.current_page){
            $rl.datatable.page($rl.current_page);
            // clear this value so it doesn't break pagination
            // otherwise "restore_dt_page" gets called on every page change attempt.
            // and you won't be able to change pages.
            $rl.current_page = null;
        }
    };

    $rl.dt_options = {
        scrollY : options.height || "600px",
        scrollX : true,
        sort : true,
        jQueryUI : true,
        paginate : true,
        pagingType: "full",
        autoWidth : false,
        displayLength: 25,
        scrollCollapse : true,
        serverSide : false,
        sortClasses : false,
        columns : $rl.table_headers,
        ajax: options.ajax ? {} : undefined,
        createdRow: function(row, data, dataIndex){
            //prevents inadvertent scrolling of the table when hash changes
            $(row).data('id', $(row).attr('id'));
            $(row).removeAttr('id');
        },
        drawCallback: $rl.restore_dt_scroll,
        preDrawCallback: $rl.restore_dt_page
    };

    if (options.ajax){
        $rl.dt_options.ajax.url = util.mkurl(['json', 'dataset_list', $rl.dataset], options.ajax_args);
        $rl.dt_options.ajax.dataSrc = function(rawdata){
            var data = rawdata.aaData;
            for (var rownum in data){
                var row = data[rownum];
                //Turn objects to strings
                for(var key in row){
                    var value = row[key];
                    if (typeof(value) == 'object' && value != null && value._type == 'file'){
                        row[key] = value.name;
                    }
                }
            }
            return data;
        };
        $rl.dt_options.serverSide = true;
    }

    $rl.update_ajax = function(ajax_options){
        options.ajax_args = ajax_options.ajax_args || options.ajax_args;
        $rl.dt_options.url = util.mkurl(['json', 'dataset_list', $rl.dataset], options.ajax_args);
        $rl.datatable_init();
    };

    $rl.datatable_destroy = function(){
        //because datatable.destroy doesn't to the job!
        $rl.datatable.destroy();
        $rl.find('.DataTables_sort_icon').remove();
        $rl.find('.DataTables_sort_wrapper').each(function(i, el){
            el.replaceWith(el.childNodes[0].data);
        });
        $rl.datatable = null;
    };

    $rl.datatable_init = function(){
        if ($rl.datatable){
            $rl.datatable.destroy();
            $rl.datatable = null;
        }
        $rl.datatable = $rl.DataTable($rl.dt_options);
        $rl.addClass('stripe');
        $rl.scrollbody = $($rl.datatable.settings()[0].nScrollBody);
    };


    $rl.on('click', '.record', function(e){
        e.preventDefault();
        window.location = "#" + $(this).data('id');
    });

    $rl.new_button.on('click', function(e){
        e.preventDefault();
        window.location = '#' + $rl.dataset + '-new';
    });

    $rl.refresh = function(e){
        if (options.ajax){
            $rl.save_dt_scroll();
            $rl.datatable.ajax.reload();
        }
    };
    $.subscribe('data_refresh', $rl.refresh);
    $rl.datatable_init();

    return $rl;
}
