/* Forked From ! Tiny Pub/Sub - v0.7.0 - 2013-01-29
* https://github.com/cowboy/jquery-tiny-pubsub
* Copyright (c) 2013 "Cowboy" Ben Alman; Licensed MIT */

// Modifications added to prevent subscribing the same function to an event
// Multiple times.
(
    function(n){
        var u = n({});
        u.subcache = {};
        n.subscribe = function(){
            const serialized_fn = String(arguments[1]);
            const signal = String(arguments[0]);
            if (! u.subcache[signal]){
                u.subcache[signal] = [serialized_fn];
                u.on.apply(u,arguments);
            }else if (u.subcache[signal].indexOf(serialized_fn) == -1){
                u.subcache[signal].push(serialized_fn);
                u.on.apply(u,arguments);
            }else{
                console.log(
                    'Not re-registering signal: "' + signal +
                        ' for function: ' + serialized_fn + '"'
                );
            }
        };
        n.unsubscribe = function(){
            u.off.apply(u,arguments);

            // we need to find all signals that are `signal` or `signal.(anything)`
            const signal = arguments[0];
            for (var subscribed_signal in u.subcache){
                const parts = subscribed_signal.split('.');
                var partial_name = '';
                for (var i in parts){
                    partial_name += parts[i];
                    if (signal === partial_name){
                        delete(u.subcache[subscribed_signal]);
                    }
                    partial_name += '.';
                }
            }
        };
        n.publish = function(){
            u.trigger.apply(u,arguments);
        };
    }
)(jQuery);
