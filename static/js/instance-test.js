document.wcgff_datepicker_options = {
        changeYear: true,
        yearRange: "1700:c",
        dateFormat: "yy-m-d",
        constrainInput: false
    };

document.options = {
    recordlist_height: '70vh',
    recordview: {
        wcgform: {
            select2: true
        }
    },
    recordsearch: {
        keywordsearch: {
            wcgform: {
                select2: true
            }
        },
        fieldsearch: {
            wcgform: {
                select2: true
            }
        },
        results: {
            height: '700px'
        }
    },
    reportingapp: {
        prepared: {
            wcgform: {
                select2: true
            }
        },
        custom: {
            wcgform: {
                select2: true
            }
        },
        results:{
            height: '700px'
        }
    }
};
