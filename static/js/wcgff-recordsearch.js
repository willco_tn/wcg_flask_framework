//////////////////////////////////
// Record searching application //
//////////////////////////////////


/* global $ util WCGRecordList field_search_form */

function SearchForm(selector, options){
    var $sf = $(selector);
    options = options || {};
    if ($sf.length === 0){
        return null;
    }
    $sf.wcgform($.extend({
        submit_event_name: 'search_form_submitted'
    }, options.wcgform));


    $sf.dataset_select = $sf.find("SELECT[name=dataset]");

    $sf.schema_btns = $sf.find('INPUT[type=radio][name=schema]');
    $sf.using_schema = $sf.schema_btns.length > 0;
    if ($sf.using_schema){
        util.match_radiobutton_heights($sf.schema_btns);
        $sf.dataset_options = $sf.dataset_select.clone();
        $sf.dataset_select.find('OPTION').remove();
        $sf.schema_btns.on('change', function(){
            $sf.dataset_select.find('OPTION').remove();
            var schema = $(this).val();
            var options = $sf.dataset_options.find('OPTION[value^=' + schema + ']').clone();
            var any_option = $sf.dataset_options.find('OPTION[value=_any_]').clone();
            if (options.length  > 1){
                $sf.dataset_select.append(any_option);
            }
            $sf.dataset_select.append(options);
            $sf.dataset_select.find('OPTION:first').attr('selected', 'selected');
        });
    }
    return $sf;
}

function KeywordSearch(selector, options){
    var $ks = SearchForm(selector, options);
    if (!$ks) return null;


    return $ks;
};

function NameSearch(selector, options){
    var $ns = SearchForm(selector, options);
    if (!$ns) return null;


    return $ns;
};

function FieldSearch(selector, options){
    var $fs = SearchForm(selector, options);
    if (!$fs) return null;

    $fs.search_fields = $fs.find("#field_search_fields");

    $fs.dataset_select.on('change', function(){
        $fs.dataset = $fs.dataset_select.find(":selected").val();
        if($fs.dataset){
            $.get(
                // field_search_form is set by js in base.jinja2
                util.mkurl(['forms', field_search_form], {dataset: $fs.dataset}),
                function(html){
                    $fs.search_fields.html(html);
                    $fs.search_fields.wcgfield_parameter_form($fs);
                });
        }
    });
    $fs.dataset_select.trigger('change');

    return $fs;
};

function WCGRecordSearch(selector, options){
    var $sa = $(selector);
    if ($sa.length === 0){
        return null;
    }
    options = options || {};
    //find objects
    $sa.searches = $sa.find("#searches");
    $sa.searches.keywordsearch = KeywordSearch("#searchform", options.keywordsearch);
    $sa.searches.namesearch = NameSearch("#namesearchform", options.namesearch);
    $sa.searches.fieldsearch = FieldSearch("#fieldsearchform", options.fieldsearch);
    $sa.results = $sa.find('#results');
    $sa.printable_btn = $sa.find('#printable_results');
    $sa.type_btns = $sa.find('INPUT[type=radio][name=type]');

    //setup interface
    $sa.searches.tabs();
    $sa.printable_btn.button({disabled: true});


    //methods

    $sa.init_results = function(){
        $sa.results_tables = [];
        $sa.results.find('TABLE').each(function(i, el){
            $sa.results_tables.push(WCGRecordList(el, {height: '300px'}));
        });
        if ($sa.results_tables.length > 0){
            $sa.printable_btn.button("option", "disabled", false);
        }else{
            $sa.printable_btn.button('option', 'disabled', true);
        }
    };

    //event handlers

    $sa.printable_btn.on('click', function(e){
        e.preventDefault();
        $sa.results_tables.forEach(function($rt){
            $rt.datatable_destroy();
        });
        util.printable_page($sa.results);
        $sa.results_tables.forEach(function($rt){
            $rt.datatable_init();
        });
    });

    //subscriptions
    $.subscribe('search_form_submitted', function(e, data){
        $sa.results.html(data);
        $sa.init_results();
    });



    return $sa;
};
