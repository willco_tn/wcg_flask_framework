//check if jquery loaded.  If it didn't, the browser is most likely incompatible.
function compat_check(sysreq_url){
    if (!("$" in window)){
    var errordiv = document.createElement("div");
    errordiv.setAttribute("class", "compat_error");
    errordiv.innerHTML = "Warning:  Your web browser is not compatible with this website.  Please see the <a href='" + sysreq_url +"'>System Requirements</a> page for options.";
    //set the style here, because old versions of IE don't always pick up the styles from the stylesheet.
    errordiv.style.position = "absolute";
    errordiv.style.top = "2em";
    errordiv.style.backgroundColor = "#F88";
    errordiv.style.opacity = ".8";
    errordiv.style.textAlign = "center";
    errordiv.style.width = "100%";
    errordiv.style.padding = "1em";
    errordiv.style.textSize = '1.4em';
    document.body.insertBefore(errordiv, document.body.firstChild);
    }
}
