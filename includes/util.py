"""

These are just generic utility functions that are helpful for web applications,
usuall for presentation prep or data mangling.

"""
import re
import datetime
import os
import base64
import csv
from io import BytesIO as pdf_io
from io import BytesIO as xls_io
from io import StringIO as csv_io

import weasyprint
import collections
import xlsxwriter as xl

from flask import g, render_template, session, Response
from .email_utils import send_email


def post_process(postdata):
    output = {}
    for key, value in postdata.items(multi=True):
        # Any character that can be part of a subkey should be part
        # of this regex.  The square brackets SHOULD NOT be OK, since
        # They are the delimiters.
        token_regex = r"([.:\-\w\s'&]+)"
        subkeys = re.findall(token_regex, key)
        context = output
        while len(subkeys) > 1:
            current_key = subkeys.pop(0)
            if not context.get(current_key):
                context[current_key] = {}
            context = context[current_key]
        current_key = subkeys.pop(0)
        if context.get(current_key):
            # if the key we're writing the value to already has a value,
            # make sure it's a list then append the value
            if type(context[current_key]) is not list:
                context[current_key] = [context.get(current_key)]
            context[current_key].append(value)
        else:
            context[current_key] = value
    return output


def parse_detail_name(name):
    """Parse the input name of a detail field into table, ordinal, and field"""

    if not name.startswith('_details') and '][' not in name:
        raise Exception('field name is not a details item name')

    first_bracket = name.find('[') + 1
    last_bracket = name.rfind(']')

    table, ordinal, field = name[first_bracket:last_bracket].split('][')
    return table, ordinal, field


def nl2br(text):
    """Convert newlines to HTML breaks."""
    return text.replace("\n", "<br>")


def timestamp(time=False, fmt=None):
    """Generate a current date/time-stamp.

    if `time` is true, include the time.
    if `fmt` is non-none, apply it as a strftime template
    """
    now = datetime.datetime.now()
    if fmt:
        return now.strftime(fmt)
    elif time:
        return now.strftime("%Y-%m-%d %H:%M:%S")
    else:
        return now.strftime("%Y-%m-%d")


def strftime(datetime_value, format_string):
    """A template-safe version of strftime

    Unlike Python strftime, the %o tag can be used for ordinal English suffix.
    """

    if not datetime_value or not hasattr(datetime_value, 'strftime'):
        return datetime_value or ''

    ordinals = {
        1: 'st',
        21: 'st',
        31: 'st',
        2: 'nd',
        22: 'nd',
        3: 'rd',
        23: 'rd'
    }

    # replace '%o'
    if '%o' in format_string:
        day = datetime_value.day
        suffix = ordinals.get(day, 'th')
        format_string = format_string.replace('%o', suffix)
    return datetime_value.strftime(format_string)


def generate_csrf_token():
    """Generate a random CSRF token as a unicode string"""
    return str(base64.b64encode(os.urandom(16)).decode('utf-8'))


def xstr(obj):
    """Ensure that an item is a string."""

    if obj is None:
        return ""
    if isinstance(obj, (datetime.date, datetime.datetime)):
        return obj.isoformat()
    if isinstance(obj, bytes):
        return bytes.decode('utf-8')

    return str(obj)


def create_csv(headers, data):
    """Build a CSV file as a string from headers and data rows"""
    fh = csv_io()
    csvfile = csv.writer(fh)
    csvfile.writerow(headers)
    for row in data:
        output_row = [xstr(row[header])
                      for header in headers]
        csvfile.writerow(output_row)
    return fh.getvalue()


def create_xlsx(headers, data, report_name, parameters):
    """Build an xlsx file as a bytes object from headers and data rows"""

    fh = xls_io()
    wb = xl.Workbook(fh, {'in_memory': True, 'default_date_format': 'yyyy-mm-dd'})
    wb.set_properties({
        'title': report_name,
        'subject': str(parameters),
        'author': g.std_args.get('user_fullname'),
        'created': datetime.date.today(),
        'comments': 'Created using {}'.format(
            g.std_args.get('application_name')
        )
    })

    # create the worksheet
    ws = wb.add_worksheet()

    # freeze the first row
    ws.freeze_panes(1, 0)

    # add data and headers as a data range
    data_lists = [
        [row[header] for header in headers]
        for row in data
    ]

    # header format
    header_format = wb.add_format({'bold': True, 'bg_color': "#CCCCEE"})
    ws.add_table(0, 0, len(data_lists), len(headers) - 1, {
        'banded_rows': True,
        'style': 'Table Style Light 11',
        'data': data_lists,
        'columns': [
            {'header': header, 'header_format': header_format}
            for header in headers
        ]
    })
    # calculate max field widths
    col_widths = list()
    for i, header in enumerate(headers):
        col_widths.append(len(header))
    for row in data:
        for cnum, header in enumerate(headers):
            col_widths[cnum] = max(col_widths[cnum], len(xstr(row[header])))

    # Resize columns to contents
    padding = 3  # accounts for arrow widgets in headers
    for i, width in enumerate(col_widths):
        ws.set_column(i, i, width + padding)

    # close workbook and return bytes object
    wb.close()
    return fh.getvalue()


def create_pdf(pdf_data, cssfile=None):
    """Turn an HTML file and optional CSS file into a PDF.

    Returns a BytesIO object containing the PDF data.
    """
    css = open(cssfile, 'r').read() if cssfile else ''
    pdf = pdf_io()
    html = weasyprint.HTML(string=pdf_data.encode('utf-8'), encoding='utf-8')
    html.write_pdf(pdf, stylesheets=[weasyprint.CSS(string=css)])
    return pdf


def sql_datatype_to_html_field_type(sqltype):
    """Given a SQL datatype, return an appropriate HTML field type."""
    lookup = {
        "text": "textarea",
        "integer": "number",
        "character": "text",
        "character varying": "text",
        "varchar": "text",
        "date": "date",
        "datetime": "datetime-local",
        "timestamp": "datetime-local",
        "timestamp with time zone": "datetime",
        "timestamp without time zone": "datetime-local",
        "numeric": "number",
        "bigint": "number",
        "smallint": "number",
        "time": "time",
        "boolean": "boolselect",
        "nvarchar": "text",
        "money": "number",
        "inet": "text",
        "cidr": "text",
        "macaddr": 'text',
        "file": "file"
    }
    if sqltype.lower() not in lookup.keys():
        print("Cannot match sql type '{}' to html input in util.py."
              .format(sqltype))
    return lookup.get(sqltype.lower())


def column_name_to_label(colname, remove_trailing_id=False):
    """Convert a column name to a label string"""

    colname = colname.replace("_", " ")
    if remove_trailing_id:
        colname = re.sub(r"\sid$", "", colname)
    colname = colname.capitalize()
    return colname


def uncamel(string):
    """Turn camelcase string to spaced"""

    if not string:
        return string
    return re.sub('([A-Z])', ' \\1', string).strip()


def process_report_output(results, options):
    """Route report results to the appropriate HTTP response"""

    output_type = options.get("output_type", "html")
    report_name = options.get("report_name", "Query Results")
    parameters = options.get("parameters", [])
    transmission_type = options.get("transmission_type", "download")
    send_to = options.get("send_to", "")
    # use `or` to short-circuit session access
    sender = options.get('sender') or session.get('user_email')
    # this allows us to use the function outside a request context, such as in a script.
    app_dir = options.get('app_dir') or g.app_dir
    email_note = options.get("email_note", "")
    print(options)

    # CSV should generate a CSV file.
    if output_type == 'csv':
        response_data = create_csv(results["headers"], results["data"])
        response_mimetype = 'application/csv'
        response_headers = {
            "Content-Disposition":
            "attachment; filename={}.csv".format(report_name)}

    elif output_type == 'xlsx':
        response_data = create_xlsx(results["headers"], results["data"], report_name, parameters)
        response_mimetype = 'application/vnd.ms-excel'
        response_headers = {
            "Content-Disposition":
            "attachment; filename={}.xlsx".format(report_name)
        }
    # PDF renders the report template for PDF and creates the PDF file
    elif output_type == 'pdf':
        pdf_template_name = "{}_pdf".format(results["template_name"])
        template_name = pdf_template_name if (
            app_dir / "templates" / "reports" / "{}.jinja2".format(
                pdf_template_name)
        ).exists() else results["template_name"]

        paper_size = (
            options.get('paper_format')
            if options.get('paper_format') in ('letter', 'legal', 'ledger')
            else 'letter'
        )
        orientation = options.get('landscape')
        htmldata = render_template(
            "reports/{}.jinja2".format(template_name),
            report_label=results["label"], results=results["data"],
            parameters=parameters, headers=results["headers"],
            paper_size=paper_size, orientation=orientation,
            date=timestamp(), **g.std_args)
        pdf = create_pdf(
            htmldata,
            app_dir / "static" / "css" / "report_pdf.css"
        )
        response_data = pdf.getvalue()
        response_headers = {"Content-Disposition": "attachment; filename={}.pdf"
                            .format(report_name)}
        response_mimetype = 'application/pdf'

    # Must be dealing with HTML output then, so generate and return that
    else:
        return render_template(
            "reports/{}.jinja2".format(results["template_name"]),
            report_label=results["label"],
            results=results["data"],
            parameters=parameters,
            headers=results["headers"],
            **g.std_args)

    # If we haven't returned yet, we must be dealing with file data, not HTML
    # So we need to find out if it's to be emailed or downloaded, and respond
    # accordingly
    if transmission_type == 'email':
        send_email(
            to=send_to,
            sender=sender,
            subject="[report]" + report_name,
            message=email_note,
            attachments=[{"data": response_data,
                          "filename": "{}.{}".format(report_name, output_type),
                          "mimetype": response_mimetype}]
        )
        return "<p>Email sent to {}.</p>".format(send_to)
    else:
        return Response(response_data, mimetype=response_mimetype,
                        headers=response_headers)


def pluralize(string, suffix_only=False):
    """Attempt to return a pluralized version of a string using English rules

    Fails utterly on weird plurals like "children" or "oxen", etc.
    """
    if not string:
        return string
    last = string[-1]
    if last in ('x', 's', 'z'):
        suffix = "es"
        string = string + suffix
    elif last in ('y', 'i'):
        suffix = "es"
        string = string[:-1] + "i" + suffix
    else:
        suffix = "s"
        string = string + suffix
    return suffix_only and suffix or string


def gfstr(mystery_object):
    """Given something that might be a list, tuple, or string, just get the str.

    Useful for things like LDAP or form requests that sometimes like to give you
    a list with a single string object instead of just a string.
    """
    if not mystery_object:
        return ''
    elif (
        not isinstance(mystery_object, str)
        and isinstance(mystery_object, collections.abc.Iterable)
    ):
        return gfstr(mystery_object[0])
    else:
        return str(mystery_object)


def upload_path(table, record_id, field, filename=''):
    return os.path.join(
        g.upload_folder,
        table,
        str(record_id),
        field,
        filename
    )

def check_dataset_write_permissions(dataset_name):
    """Check if the current session has write permissions for the dataset"""

    dataset = g.datasets.get(dataset_name)
    # The dataset must exist
    if not dataset:
        return False

    # Your session must have "write access"
    if not session.get('write_access'):
        return False

    # You should have access unless "limit_write" is set
    # In that case, we need to check if you're in the "limit_write" group
    limit_write = dataset.get('limit_write')
    if not limit_write:
        return True

    if set(limit_write) & set(session.keys()):
        return True

    # If we haven't returned yet, you don't have write access
    return False


def get_param_type(name):
    """Given a report parameter name, determine its type from the suffix"""

    types = [
        ('_tf', 'tf'),
        ('_atf', 'atf'),
        ('_num', 'num'),
        ('_int', 'integer'),
        ('_date', 'date'),
        ('_lkp', 'lookup'),
        ('', 'text')  # this should be last, to ensure a default
    ]

    for suffix, typename in types:
        if name.endswith(suffix):
            return typename, suffix
    return ('text', '')
