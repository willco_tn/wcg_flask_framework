"""
Custom JSON encoder to handle lots of data types.
"""
from flask.json.provider import JSONProvider as BaseJSONProvider
import json
from datetime import date, time, datetime
from decimal import Decimal


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, time):
            return obj.isoformat()
        elif isinstance(obj, Decimal):
            return float(obj)
        return super(JSONEncoder, self).default(obj)

class JSONProvider(BaseJSONProvider):

    def loads(self, obj, **kwargs):
        return json.loads(obj)

    def dumps(self, obj, **kwargs):
        # the encoder is provided here as a separate class so that
        # it's encoding can be applied recursively on lists/dicts/tuples.
        return json.dumps(obj, cls=JSONEncoder)
