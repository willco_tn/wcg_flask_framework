"""
This library abstracts authentication for the application.

"""
from .ad_auth import AD
from .edirectory_auth import EDirectory
from .dummy import DummyAuth


class Authenticator():
    """
    An authenticator represents an authentication source, which should also include rudimentary info about the user, such as an LDAP.
    """
    sourcetypes = {
        "active_directory": AD,
        "edirectory": EDirectory,
        "dummy": DummyAuth
    }

    def __init__(self, sourcetype, **kwargs):
        self.backend = self.sourcetypes.get(sourcetype)(**kwargs)

    def authenticate(self, *args, **kwargs):
        return self.backend.authenticate(*args, **kwargs)

    def user_fullname(self):
        return self.backend.user_fullname()

    def error(self):
        return self.backend.error

    def source(self):
        return self.backend.authsource

    def user_email(self):
        try:
            email = self.backend.user_email()
        except AttributeError:
            email = None

        return email

    def user_can_write(self):
        return self.backend.user_can_write()

    def user_in_group(self, groupname):
        return self.backend.in_group(groupname)
