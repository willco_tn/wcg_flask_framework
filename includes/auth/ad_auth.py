#!/usr/bin/env python
# Module for authenticating against Active Directory
# by Alan Moore


try:
    from .ldap_auth import LDAP_AUTH
except SystemError:
    from ldap_auth import LDAP_AUTH


class AD(LDAP_AUTH):

    directory_name = "Microsoft Active Directory"
    user_search_template = "(sAMAccountName={})"
    group_membership_key = "memberOf"
