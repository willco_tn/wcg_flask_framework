from hashlib import sha256

class DummyAuth():
    """
    A dummy authenticator for use during testing.  Authenticates with test/test123 by default, or specified.

    It's also sort of a blueprint for a valid authenticator.
    """
    authsource = "Dummy Authenticator"
    DEFAULT_USERS = (
        {
            "username": "readonly",
            "password": "test123",
            "user_fullname": "Test J Readonlyman",
            "groups": ["users"]
        },
        {
            "username": "user",
            "password": "test123",
            "user_fullname": "Test J User",
            "groups": ["users", "readwrite"]
        },
        {
            "username": "admin",
            # passwords can also be sha256 encoded here.
            # this is 'test123'
            "password_sha256": 'ecd71870d1963316a97e3ac3408c9835ad8cf0f3c1bc703527c30265534f75ae',
            "user_fullname": "Test J Admin",
            "groups": ["admins", "users", "readwrite"]
        }
    )

    def __init__(
            self,
            users=DEFAULT_USERS,
            write_group='readwrite',
            email_domain='example.com'
    ):
        self.users = users
        self.write_group = write_group
        self.authenticated_user = None
        self.email_domain = email_domain
        self.error = ""

    def authenticate(self, username, password):
        for user in self.users:
            if all([
                    user.get("username") == username,
                    password,
                    (
                        (user.get("password") == password) or
                        (
                            user.get('password_sha256') ==
                            sha256(password.encode('utf-8')).hexdigest()
                        )
                    )
            ]):
                self.error = ''
                self.authenticated_user = username
                self.authenticated_user_record = user
                return True
        else:  # if the for loop completes without returning
            self.error = "Username or password invalid."
            return False

    def user_fullname(self):
        return self.authenticated_user_record.get("user_fullname")

    def user_can_write(self):
        return self.in_group(self.write_group)

    def in_group(self, groupname):
        return groupname in self.authenticated_user_record.get("groups", [])

    def user_email(self):
        email = "{}@{}".format(self.authenticated_user, self.email_domain)
        return email if self.authenticated_user else None

    def info_on(self, username):
        user = [
            u for u in self.users
            if u["username"].lower() == username.lower()
        ]
        if user:
            return user[0]
        else:
            return None
