#!/usr/bin/env python
# Module for authenticating against eDirectory
# by Alan Moore

try:
    from .ldap_auth import LDAP_AUTH
except SystemError:
    from ldap_auth import LDAP_AUTH


class EDirectory(LDAP_AUTH):
    directory_name = "Novell eDirectory"
    user_search_template = "(uid={})"
    group_membership_key = 'groupMembership'
