#!/usr/bin/env python
# Module for authenticating against Active Directory
# by Alan Moore

import ldap3


class LDAP_AUTH():
    user_search_template = '(uid={})'
    directory_name = "generic LDAP"
    group_membership_key = "memberOf"

    def __init__(
            self,
            server='localhost',
            base_dn="",
            bind_dn="",
            bind_pw="",
            require_group=None,
            write_group=None,
            ssl=False
    ):
        """Contructor for the connection.  Assumes plaintext LDAP"""
        self.error = ""
        self.servername = server
        self.base_dn = base_dn
        self.bind_dn = bind_dn
        self.bind_pw = bind_pw
        self.require_group = require_group
        self.write_group = write_group
        self.authenticated_user = None
        self.authenticated_dn = None
        self.authsource = self.directory_name + " on " + base_dn

        # attempt to connect to the server
        self.server = ldap3.Server(self.servername, use_ssl=ssl)
        self.con = ldap3.Connection(
            self.server,
            user=self.bind_dn,
            password=self.bind_pw
        )
        try:
            self.bound = self.con.bind()
        except ldap3.core.exceptions.LDAPCommunicationError as e:
            self.error = "Failed to connect remote socket: {}".format(e)
            self.bound = False
        if not self.bound:
            self.error = (
                self.error
                or
                "Could not bind to server {}.".format(self.servername)
            )
            if self.bind_dn is not None:
                self.error += "as {}".format(self.bind_dn)
            self.con = False

    def authenticate(self, username, password):
        """Given a simple username and password,
        return true or false if the user is authenticated
        """

        if not self.con:
            self.error = "No connection to LDAP"
            return False

        if not password:
            self.error = "Invalid credentials: no password supplied"
            return False

        search_succeeded = self.con.search(
            search_base=self.base_dn,
            search_filter=self.user_search_template.format(username),
            search_scope=ldap3.SUBTREE
        )

        if not search_succeeded:
            self.error = "Search for {} failed".format(username)
            return False

        user_dn = self.con.response[0].get("dn")

        if not user_dn:
            self.error = "No such user {}".format(username)
            return False

        try:
            self.con = ldap3.Connection(
                self.server, user=user_dn, password=password
            )
            self.con.bind()
            self.authenticated_user = username
            self.authenticated_dn = user_dn
        except ldap3.LDAPBindError:
            self.error = ("Invalid credentials: Login as {} failed."
                          .format(username))
            return False

        if not self.con.bound:
            self.error = ("Invalid credentials: Login as {} failed."
                          .format(username))
            return False

        # If you've gotten to this point, the username/password checks out
        if self.require_group and not (self.in_group(self.require_group)):
            self.error = "Permission denied: not in required group {}".format(
                self.require_group
            )
            return False

        return True  # All tests passed!

    def in_group(self, group, principle=None):
        if not self.con:
            self.error = "No connection to " + self.directory_name
            return False

        principle = principle or self.authenticated_dn
        group_res = self.con.search(
            self.base_dn,
            "(cn={})".format(group),
            search_scope=ldap3.SUBTREE
        )
        if group_res:
            group_dn = self.con.response[0].get("dn")
            principles_groups = self.info_on_dn(principle).get(self.group_membership_key, [])

            if group_dn in principles_groups:
                return True
            for g in principles_groups:
                try:
                    if self.in_group(group, g):
                        return True
                except RecursionError as e:
                    print("Group membership detection failed due to recursion error:")
                    print(e)
                    return False
        return False

    def info_on(self, username):
        """Return LDAP information on the given username"""
        if not self.con:
            self.error = "No connection to LDAP."
            return False
        res = self.con.search(
            self.base_dn,
            self.user_search_template.format(username),
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES
        )

        if not res:
            self.error = "No such user {}".format(username)
            return False
        return self.con.response[0]['attributes']

    def info_on_dn(self, dn):
        """Return LDAP information on the given DN"""
        if not self.con:
            self.error = "No connection to LDAP."
            return False
        res = self.con.search(
            dn,
            "(objectClass=*)",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES
        )
        if not res:
            self.error = "No such DN '{}'".format(dn)
            return {}
        return self.con.response[0]['attributes']

    def user_fullname(self):
        if self.authenticated_user:
            return self.info_on(self.authenticated_user).get("name")
        return ""

    def user_email(self):
        if self.authenticated_user:
            email = self.info_on(self.authenticated_user).get("mail", [''])
            return email
        return None

    def user_can_write(self):
        return self.write_group and self.in_group(self.write_group)
