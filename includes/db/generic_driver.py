"""
This is a parent class for all database drivers.
"""


class GenericDatabaseDriver:
    """
    Parent class for database drivers.
    """

    # A template used to enclose table/schema names, etc.
    object_quote_template = '"{}"'

    # A template for enclosing a field name as a parameter
    parameter_template = "%({})s"

    # A template for casting a field to a type
    cast_template = "CAST({field} as {type})"

    # Operator for string concatenation
    string_concat_operator = "||"

    # For assembling dml queries with output
    dml_with_output_template = "{action} {predicate}; {output}"

    # The output clause template
    output_clause_template = "SELECT last_insert_id()"

    # The output field template
    output_clause_field_template = '"{}"'

    # The operator used for RegExp strings
    regex_operator = 'REGEX'

    # the default schema name, when none is specified
    default_schema = ''

    # template for main list lookup query.  Mostly necessary because
    # SOME databases don't properly support offset/limit
    mainlist_query_template = """SELECT * FROM {main_view} {where}
    ORDER BY {sort_clause} OFFSET {offset} LIMIT {limit}
    """

    # User Exceptions are exceptions that would be triggerd by bad
    # input from the user.  We'll send these back to the front end
    # as a 400 error rather than an internal server error
    user_exceptions = tuple()

    # reference to the library module
    dbapi = None

    class IntegrityError(Exception):
        pass

    def __init__(self, *args, **kwargs):
        """Construct a driver object

        Should receive any connection or configuration information
        relevant to the database
        """
        pass

    def connect(self):
        """Return a connection object based on the information
        passed into the constructor
        """
        pass

    def cursor(self, cx):
        """Return a cursor created from cx.

        This class exists as a hook into the cursor creation process,
        allowing for custom configuration.
        """
        return cx.cursor()

    def fulltext_search_query(
            self, table, match_fields, terms, and_terms=False
    ):
        """Return a fully formatted fulltext search query
        incorporating the terms and everything.

        The caller expects to run the query without parameters.

        WARNING: PROPER DATA SECURITY IS YOUR RESPONSIBILITY
                 WHEN CONSTRUCTING THIS QUERY!!!!!!!!!!!!!!!
        """
        raise Exception("STUB")

    def escape_object(self, db_object):
        """Safely quote an object name

        Given an object name as a string, quote each component.
        For example, turn:
            mydb.myschema.mytable
        Into:
            "mydb"."myschema"."mytable"

        Use RDBMS-specific quoting.
        """
        sub_objs = db_object.split(".")
        sub_objs = [self.object_quote_template.format(x) for x in sub_objs]
        return ".".join(sub_objs)
