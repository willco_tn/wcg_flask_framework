import mysql.connector
from .generic_driver import GenericDatabaseDriver


class MySQLCursorDict(mysql.connector.cursor.MySQLCursor):

    object_quote_template = "`{}`"

    def _row_to_python(self, rowdata, desc=None):
        row = super(MySQLCursorDict, self)._row_to_python(rowdata, desc)
        if row:
            return dict(zip(self.column_names, row))
        return None


class MysqlDriver(GenericDatabaseDriver):
    dbapi = mysql.connector

    def __init__(self, db, host, user, password):
        self.db = db
        self.host = host
        self.user = user
        self.password = password

    def connect(self):
        cx = mysql.connector.connect(user=self.user, password=self.password,
                                     host=self.host, database=self.db)
        return cx

    def cursor(self, cx):
        return cx.cursor(cursor_class=MySQLCursorDict)
