import psycopg2 as pg
import psycopg2.extensions
from psycopg2.extras import RealDictCursor, register_composite, CompositeCaster
from .generic_driver import GenericDatabaseDriver


class FileDictComposite(CompositeCaster):
    """Return dicts rather than named tuples from composite type

    Taken from psycopg2 documentation
    """
    def make(self, values):
        obj = dict(zip(self.attnames, values))
        obj['_type'] = 'file'
        obj['url'] = ''
        return obj

class PostgresqlDriver(GenericDatabaseDriver):
    """Database driver class for PostgreSQL"""
    cast_template = "{field}::{type}"
    dml_with_output_template = "{action} {predicate} {output}"
    output_clause_template = "RETURNING {output_field_list}"
    default_schema = 'public'
    dbapi = pg
    user_exceptions = (pg.DataError, pg.IntegrityError)
    regex_operator = '~*'

    def __init__(self, db, host, user, password):
        self.db = db
        self.host = host
        self.user = user
        self.password = password

    def connect(self):
        """Connect to the database, return the connection"""
        cx = pg.connect(database=self.db, host=self.host,
                        user=self.user, password=self.password)
        try:
            register_composite('file', cx, factory=FileDictComposite)
        except pg.ProgrammingError:
            print('File data type not registered in this DB')

        return cx

    def cursor(self, cx):
        """Return a cursor on the current connection"""
        cur = cx.cursor(cursor_factory=RealDictCursor)
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE, cur)
        return cur

    def fulltext_search_query(
            self, table, match_fields, terms, and_terms=False
    ):
        """Return a query and parameters dict prepared for fulltext search.

        Parameters:
        table - The table to search
        match_fields - which fields to match against
        terms - The search terms to match
        and_terms - If true, all terms must match.  Else, any term.
        """
        conjunction = (and_terms and "&") or "|"
        # remove & and | from terms
        terms = terms.replace('&', ' ').replace('|', ' ')
        term = conjunction.join(terms.split())
        query = """SELECT * FROM {table} WHERE {where}"""
        where = "to_tsvector(CONCAT("
        where += "|| ' ', ".join(  # use the concatenation operator so we don't double our arg count.
            self.cast_template.format(
                field=self.object_quote_template.format(f), type="text"
            )
            # postgresql limits you to 100 args to functions
            # So we can only search the first 100 fields.
            for f in match_fields[:100]
        )
        where += ")) @@ to_tsquery({})".format(
            self.parameter_template.format("term"))
        query = query.format(table=self.escape_object(table), where=where)
        return query, {'term': term}
