import sqlite3
from .generic_driver import GenericDatabaseDriver


class Sqlite3Driver(GenericDatabaseDriver):
    dbapi = sqlite3

    def __init__(self, db=":memory:", *args, **kwargs):
        self.db = db

    def connection(self):
        cx = sqlite3.connect(self.db)
        cx.row_factory = sqlite3.Row
        return cx
