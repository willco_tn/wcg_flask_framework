"""
The database module

This module encapsulates all database interactions for WCGFF.

It can be extended for instances by subclassing this as LocalDatabase
in includes/local_database.py.

This module is NOT an ORM.  It's just a set of methods that interact with the
database via SQL.  Ideally, no SQL should exist outside this file and
 local_database.py

This module is designed to work with multiple database backends, and as such
 code in this module should be RDBMS-agnostic.  RDBMS-specific code should be
 wrapped into the driver modules.

In reality, only PostgreSQL really completely works at production quality.
Unless you have a super good reason to use something else, use PostgreSQL.

"""
try:
    from .mysql_driver import MysqlDriver
except ImportError:
    MysqlDriver = None
try:
    from .postgresql_driver import PostgresqlDriver
except ImportError:
    PostgresqlDriver = None
try:
    from .sqlite3_driver import Sqlite3Driver
except ImportError:
    Sqlite3Driver = None

import re
import shlex
from collections import OrderedDict
from .. import util


class Database(object):
    """
    Represents the application database

    Encapsulates all access to the database through getter/setter functions.
    """
    drivertypes = {
        "mysql": MysqlDriver,
        "postgresql": PostgresqlDriver,
        "sqlite": Sqlite3Driver
    }

    class NoSuchDataset(Exception):
        pass

    class NoSuchRecord(Exception):
        pass

    class NoSuchTable(Exception):
        pass

    class NoSuchField(Exception):
        pass

    class CannotDeleteRecord(Exception):
        pass

    class DataError(Exception):
        pass


    @staticmethod
    def generate_urls_for_file_fields(results, table, primary_keylist):
        """given a list of query results, find the file fields and generate a URL"""
        for row in results:
            for field, value in row.items():
                if isinstance(value, dict) and value.get('_type') == 'file':
                    detail_record_id = '-'.join([
                        str(row[pkf])
                        for pkf
                        in primary_keylist
                    ])
                    value["url"] = '{}/{}/{}/{}'.format(
                        table,
                        detail_record_id,
                        field,
                        value["name"]
                    )

    def __init__(
            self, db_type, db=None, host=None, user=None,
            password=None, datasets=None, reports_table=None,
            audit_table=None, regex_support=False
    ):
        """Construct a database object.

        Parameters:
           - db_type: The driver type to use.  Should match a key in `drivertypes`
           - db: the name of the database
           - host: Hostname where the database is running
           - user: Username to connect with
           - password: Password to connect with
           - datasets:  The `datasets` dictionary from the config file
           - reports_table: Name of the reports table
           - audit_table: Name of the audits table
        """
        self.driver = self.drivertypes.get(db_type)
        if self.driver is None:
            raise ImportError(
                "Missing the correct database library for {}".format(db_type)
            )
        self.db = db
        self.host = host
        self.user = user
        self.password = password
        self.dbo = self.driver(
            db=self.db, host=self.host, user=self.user, password=self.password
        )
        self.cx = None
        self.cursor = None
        self.datasets = datasets or {}
        self.reports_table = reports_table or 'reports'
        self.audit_table = audit_table
        self.regex_support = regex_support

    def connection(self):
        """Always return a valid connection"""
        if self.cx is None:
            self.cx = self.dbo.connect()
        return self.cx

    def __cursor(self):
        """Return a valid cursor for the current connection."""
        return self.cursor or self.dbo.cursor(self.connection())

    def reset(self):
        """Reset the connection and cursors to a fresh state."""
        if self.cx:
            self.cx.close()
        self.cx = None
        self.cursor = None

    def query(self, query, parameters=None, commit=False):
        """Execute a query with the current cursor

        All SQL queries should go through this method to ensure
        proper error handling.

        Parameters:
            - query: SQL string
            - parameters: dictionary of query parameters.
                          If included, must match the parameterized query.
            - commit: if True, a commit will be run after the query.
                      Otherwise, data won't be saved.
        Returns:
            A list of dictionaries containing the query results.
            An empty list is returned when there are no results.
        """

        self.cursor = self.__cursor()
        if not parameters:
            try:
                self.cursor.execute(query)
            except self.driver.user_exceptions as e:
                print(query)
                print(parameters)
                raise Database.DataError(e)
            except Exception as e:
                print(query)
                raise e
        else:
            try:
                self.cursor.execute(query, parameters)

            except self.driver.user_exceptions as e:
                print(query)
                print(parameters)
                raise Database.DataError(e)

            except Exception as e:
                print(query)
                print(parameters)
                raise e

        # Some rdbms need to get the result before
        # the commit to provide a description
        result = (self.cursor.description and self.cursor.fetchall()) or []
        if commit:
            self.connection().commit()
        else:
            # If we're not committing, we need to rollback, so the connection
            # isn't stuck waiting on a transaction to finish
            self.connection().rollback()
        return result

    ############################
    # SQL Formatting utilities #
    ############################

    def split_schema(self, tablename):
        """Split an identifier at the period"""
        schema = self.driver.default_schema
        if '.' in tablename:
            schema = tablename.split(".")[0]
            tablename = tablename.split(".")[1]
        return schema, tablename

    def escape_object(self, tablename):
        """Properly escape a db object name.

        This is just a wrapper for functionality in the driver library.
        """
        return self.dbo.escape_object(tablename)

    def named_parameter(self, parameter):
        """Prepare a parameter name

        Take a name, and return a named parameter string according to
        what the driver needs.
        """
        return self.driver.parameter_template.format(parameter)

    def cast(self, field, dtype):
        """Return a SQL snippet to cast a fieldname to a type"""
        return self.driver.cast_template.format(field=field, type=dtype)

    ###########
    # Getters #
    ###########

    # Define methods here to return data from the database
    # Methods exposed to the web need to check inputs and eat extra kwargs

    def get_headers(self, table, **kwargs):
        """For a given table, return the columns headers.

        Filters out DT_RowId and DT_RowClass; it's meant for defining
        table headers on a dataset main page.
        """
        results = self.get_schema_for_table(table)
        if results:
            return [
                x["column_name"] for x in results
                if x["column_name"] not in ("DT_RowId", "DT_RowClass")
            ]
        return []

    def get_dataset_mainlist(self, dataset, **kwargs):
        """Return the main list of records for a dataset

        This function is meant to receive requests from a dataTables object.
        It can handle the following keyword args, mostly generated by dataTables:

        start : The result offset (integer)
        length : How many records to display (integer)
        sEcho : A number that datatables makes up, sort of like a csrx token
        order[N][column] : For any number of kwargs N, the name of a column
                           to be used in ordering results.
        order[N][dir] : The direction ('asc' or 'desc') to sort on for column N
        search[value] : a string to match records against.  This searches for
                        the existence of ALL terms in ANY field.
        filter_field: A comma-separated list of fields to match against.
        filter_value: A comma-separated list of values to match filter_fields.
                      Default value is NULL for omitted fields.
        filter_comp:  A comma-separated list of comparison functions to use
                      for each filter field. Can be "equals" or "contains".
                      Default value is "equals" for omitted fields.

        It returns a dictionary in a format that makes sense to datatables:
        {
          "aaData": The row data, as a list of dicts,
          "sEcho" : The echo value received from dataTables's request,
          "iTotalRecords" : The total number of records in the table,
          "iTotalDisplayRecords" : The total number of records being displayed
        }
        """
        # validate dataset
        if dataset not in self.datasets.keys():
            raise Database.NoSuchDataset(dataset)
        view_name = self.datasets.get(dataset).get("main_view")
        fields = self.get_headers(view_name)

        # start acquiring offset and limit

        # For some reason, different webservers
        #  return either strings or single-value lists for this data.
        #  for this reason, use util.gfstr() to ensure a single string.
        offset = int(
            util.gfstr(kwargs.get('start')) or 0  # new datatables
        )
        limit = int(
            util.gfstr(kwargs.get('length')) or 0  # new datatables
        )
        if limit < 1:
            limit = 'ALL'

        # echo/sEcho is just a key that DataTables uses to identify
        # the request.  We need to echo the value back in our reply.
        echo = kwargs.get("sEcho")

        # figure out the field names and directions to sort on
        sort_cols = []
        for i in range(len(fields)):  # at most, we can sort on len(fields) fields
            sort_col = util.gfstr(kwargs.get('order[{}][column]'.format(i)))
            if sort_col is None:
                continue
            sort_dir = util.gfstr(kwargs.get('order[{}][dir]'.format(i))).lower()
            sort_dir = sort_dir if sort_dir in ('asc', 'desc') else 'asc'
            sort_col = int(sort_col) if sort_col.isdigit() else 0
            sort_field = fields[sort_col]
            sort_cols.append((sort_field, sort_dir))

        if sort_cols:  # If we have sort columns, build the ORDER BY clause
            sort_clause = ', '.join([
                '{} {}'.format(self.escape_object(field), direction)
                for field, direction
                in sort_cols
            ])
        else:
            sort_clause = '0 ASC'  # Sort the first field by default

        # Datatables wants the total number of records
        # in the table for its footer text
        total_records = self.query(
            "SELECT count(*) as records FROM {} ".format(
                self.escape_object(view_name))
        )[0].get("records")

        # These store where clauses and query data
        # We'll add to these as we go
        query_data = {}
        wheres = []

        # If there's a search string, we're going to create a where clause
        search_terms = util.gfstr(kwargs.get('search[value]'))

        # Split the search terms, then create a where clause by ANDing
        # together matches between each term and a concatenation of all the
        # fields.

        if search_terms:
            # First we'll generate the SQL that casts each field to a string
            # and concatenates them with a space.
            # The COpALESCE is necessary to protect against nulls.
            concatenated_fields = self.driver.string_concat_operator.join(
                [
                    "COALESCE(lower({}), '') {} ' ' ".format(
                        self.cast(self.escape_object(field), 'text'),
                        self.driver.string_concat_operator
                    ) for field in fields
                ]
            )
            # Split the search terms with shlex.split() so that we can keep
            # quoted bits together.  Since we're doing this as the user types,
            # we need to catch a ValueError due to an unclosed quote.
            try:
                search_terms_list = shlex.split(search_terms)
            except ValueError:
                # if there are unclosed quotes, you get a ValueError, so
                # use a conventional whitespace split.
                search_terms_list = search_terms.split()

            # Now, for each search term, create a parameter name
            # and populate the parameter dict and where clause
            # with them.
            for i, search in enumerate(search_terms_list):
                searchname = "search{}".format(i)
                query_data[searchname] = '%{}%'.format(search)
                wheres.append(
                    "({} like lower({}))".format(
                        concatenated_fields,
                        self.named_parameter(searchname)
                    ))

        # Filter fields
        # We can filter the table on a specific field
        # This is different from search, which checks all fields
        filter_fields = kwargs.get('filter_field', '').split(',')
        filter_values = kwargs.get('filter_value', '').split(',')
        filter_comparisons = kwargs.get('filter_comp', '').split(',')

        comparisons = {
            'equals': '{} = {}',
            'contains': "CAST({} AS VARCHAR) LIKE '%%' || {} || '%%'"
        }

        # For each given filter field, determine the matching value and comp
        # default value is NULL, default comp is "equals"
        # Then build a WHERE subclause for each filter comparison
        for i, filter_field in enumerate(filter_fields):
            if filter_field not in fields:  # Ignore invalid fields
                continue
            try:
                filter_comparison = filter_comparisons[i]
            except IndexError:
                filter_comparison = 'equals'
            try:
                filter_value = filter_values[i]
            except IndexError:
                filter_value = None
                wheres.append(
                    '{} IS NULL'.format(self.escape_object(filter_field))
                )
            else:
                wheres.append(
                    comparisons.get(filter_comparison, '{} = {}').format(
                        self.escape_object(filter_field),
                        self.named_parameter(filter_field)
                    )
                )
                query_data[filter_field] = filter_value

        # Generate the final WHERE clause and format query data
        if wheres:
            where = "WHERE {}".format(
                " AND ".join(wheres)
            )
            # Datatables wants a number of records filtered by
            # search and filter conditions, but not by pagination
            display_records_query = (
                "SELECT count(*) as records FROM {} {} ".format(
                    self.escape_object(view_name), where)
            )
            display_records = self.query(
                display_records_query,
                query_data
            )[0].get("records")
        else:
            where = ''
            display_records = total_records

        # Now at last we get the actual data to return
        # This is a driver method because different RDBMS have
        # different pagination syntax
        query = self.driver.mainlist_query_template.format(
            main_view=self.escape_object(view_name),
            where=where,
            sort_clause=sort_clause,
            offset=offset,
            limit=limit
        )
        aaData = self.query(query, query_data) if wheres else self.query(query)

        return {
            "aaData": aaData,
            "sEcho": echo,
            "iTotalRecords": total_records,
            "iTotalDisplayRecords": display_records
        }

    def get_lookup_values_for_dataset(self, dataset=None, **kwargs):
        """Get all the lookup information for a dataset

        It takes the dataset name as a string, and returns a dictionary
        in this format:
        {
          "some_lookup_table_name" : [list of dicts containing lookup table rows],
          "some_other_lookup_table_name" : [list of dicts containing lookup table rows],
          ...
        }
        """
        lookups = {}
        if dataset in self.datasets.keys():
            for lookup in self.datasets.get(dataset).get("lookups", []):
                table = lookup.get("table_name")
                default_order = [
                    (k, 'ASC') for k in
                    self.get_primary_keys_for_table(table).keys()
                ]
                order_by = ', '.join(
                    "{} {}".format(*x)
                    for x in lookup.get("order_by", default_order)
                )

                query = "SELECT * FROM {} ORDER BY {}".format(
                    self.escape_object(table), order_by
                )
                lookups[table] = self.query(query)
        return lookups

    def get_schema_for_table(self, tablename):
        """Return a dict describing the columns in a table

        "Schema" in this case means the layout of the table, not the logical
        container in which it exists.
        Takes the name of a table and returns a list of dicts containing the
        name, datatype, and whether it's a primary key for each column in the
        table in order.
        """
        schema, table = self.split_schema(tablename)
        pk_col = self.get_primary_keys_for_table(tablename)
        query = """SELECT column_name,
                   CASE WHEN data_type = 'USER-DEFINED' THEN udt_name
                     ELSE data_type END AS data_type
                   FROM information_schema.columns
                   WHERE lower(table_schema) LIKE lower(%(schema)s)
                   AND lower(table_name) LIKE lower(%(table)s)
                   ORDER BY ordinal_position ASC
        """
        res = self.query(query, {"schema": schema, "table": table})
        if not res:
            return None
        for n, row in enumerate(res):
            if row["column_name"] in pk_col.keys():
                res[n]["is_pkey"] = True
        return res

    def get_file_fields(self):
        """Return all the file fields in the database

        Returns a dictionary in the format:
        {
            'tablename1': ['file_field_name_1', 'file_field_name_2', …],
            'tablename2': ['file_field_name_1', 'file_field_name_2', …]
        }
        """
        query = """
            SELECT column_name, table_name, table_schema
            FROM information_schema.columns
            WHERE udt_name = 'file'
        """

        res = self.query(query)
        rval = {}
        for row in res or []:
            if row['table_schema'] != 'public':
                table_name = f"{row['table_schema']}.{row['table_name']}"
            else:
                table_name = row['table_name']
            if rval.get(table_name) is None:
                rval[table_name] = [row['column_name']]
            else:
                rval[table_name].append(row['column_name'])
        return rval

    def get_schema_for_dataset(self, dataset):
        """Return a dict describing all the tables in a dataset

        The dict returned looks like this:
        {
          "main": [the schema for the dataset's main table],
          "details": {"name_of_detail_table": [the detail table's schema],...},
          "lookups": {"name_of_lookup_table": [the lookup table's schema],...},
        }
        The schema list in each case is the output of "get_schema_for_table()".
        """
        ds = self.datasets.get(dataset)
        if not ds:
            return None
        rval = {}
        rval["main"] = self.get_schema_for_table(ds.get("main_table"))
        rval["details"] = {}
        rval["lookups"] = {}
        for detail in ds.get("details", []):
            t = detail.get("table_name")
            rval["details"][t] = self.get_schema_for_table(t)
        for lookup in ds.get("lookups", []):
            t = lookup.get("table_name")
            rval["lookups"][t] = self.get_schema_for_table(t)

        # Views

        if ds.get('field_search_view'):
            rval["main_fsv"] = self.get_schema_for_table(ds['field_search_view'])
        else:
            rval['main_fsv'] = rval['main']

        for detail in ds.get('details', []):
            t = detail.get('table_name')
            rval['details'][t + "_fsv"] = (
                self.get_schema_for_table(detail.get('field_search_view'))
                if detail.get('field_search_view')
                else rval['details'][t]
            )
        return rval

    def get_record_details(self, **kwargs):
        """Get all the data of a single record

        Parameters:
            - dataset: The name of the record's dataset
            - key: The record's primary key.  For tables with multiple key
                   columns, the values are separated by a '-' in the order
                   described by the dataset's "id_columns" tuple.

        Returns a dictionary in this format:
        {
            "field1_actual_name": "value of main_table field 1",
            "field2_actual_name": "value of main_table field 2",
            # etc...
            "_details": {
                "detail_tablename": [
                    {'detail_field_1': "value of detail_field_1 for this record", etc.},
                    {'detail_field_1': "value of detail_field_1 for this record"}
                ],
               "detail_tablename2": [# etc]
            }
        }
        """

        # Validate the dataset
        dataset = util.gfstr(kwargs.get("dataset"))
        if dataset not in self.datasets.keys():
            raise self.NoSuchDataset("No such dataset {}".format(dataset))

        # split multiple key values using '-'
        # also ensures key is a list, as web servers aren't consistent here
        key = util.gfstr(kwargs.get('key')).split('-')
        record_id = dict(zip(
            self.datasets.get(dataset).get("id_columns"), key
        ))
        main_table = self.datasets[dataset].get('main_table')

        # Begin building SQL
        query = "SELECT * FROM {} WHERE ".format(self.escape_object(main_table))
        where = " AND ".join([
            "{} = {}".format(pk, self.named_parameter(pk))
            for pk in self.datasets.get(dataset).get("id_columns")
        ])
        query += where

        # Fetch main table results
        results = self.query(query, record_id)

        # We need to look through the results for file field values
        # and construct a download URL for the file
        for field, value in (results[0] if results else {}).items():
            if isinstance(value, dict) and value.get('_type') == 'file':
                value["url"] = '{}/{}/{}/{}'.format(
                    self.datasets.get(dataset).get('main_table'),
                    '-'.join(record_id.values()),
                    field,
                    value["name"]
                )
        # Handle the lack of any results
        if not results:
            raise Database.NoSuchRecord(
                "Record not found: {}".format(
                    ",".join([
                        "{} = {}".format(k, v)
                        for k, v in record_id.items()
                    ]))
            )

        # Results come as a list of rows, but we only want one row here
        # there should be only one anyway, since we matched against the PK
        results = results[0]

        # Now we need to get data for the detail tables
        results["_details"] = {}
        for detail in self.datasets.get(dataset).get("details", []):
            # For each detail table listed in the configuration,
            # We're going to generate a SQL query and put the results
            # in results["_details"][detail_table_name]
            dtable = detail.get("table_name")
            dtable_pk = self.get_primary_keys_for_table(dtable)

            # we can have detail tables for detail tables.
            # if the detail is a detail of a detail, we need to find
            # the correct record_id for that table.
            fortable = detail.get("for_table", main_table)
            if detail.get("order_by"):
                order_by = ", ".join(
                    ' '.join(x) for x in detail.get("order_by")
                )
            else:
                order_by = ", ".join(dtable_pk.keys())

            dquery = """SELECT * FROM {dtable}
                        WHERE {where_list} ORDER BY {order_by}"""
            where_list = " AND ".join(
                '"{}"={}'.format(v, self.named_parameter(k))
                for k, v in detail.get("fk_columns").items()
            )
            dquery = dquery.format(
                dtable=self.escape_object(dtable),
                where_list=where_list,
                order_by=order_by
            )
            if fortable != main_table:
                # In order to deal with details of details, the outer table needs to be
                # specified first and thus run first.  So we'll raise an exception
                # if it's not yet been run.
                if fortable not in results["_details"]:
                    raise Exception((
                        "Detail records for {} cannot be pulled because its "
                         "parent table, {}, is not in the detail output. "
                        "Make sure the parent table is listed first in the config."
                    ).format(dtable, fortable))
                # now, for each detail record, we need to get its detail records
                for drecord in results["_details"][fortable]:
                    if not drecord.get("_details"):
                        drecord["_details"] = {}
                    drecord["_details"][dtable] = self.query(dquery, drecord)
                    self.generate_urls_for_file_fields(
                        drecord["_details"][dtable],
                        dtable,
                        dtable_pk
                    )
            else:
                results["_details"][dtable] = self.query(dquery, record_id)
                self.generate_urls_for_file_fields(
                    results["_details"][dtable],
                    dtable,
                    dtable_pk
                )

        return results


    def get_primary_keys_for_table(self, table):
        """Get Primary Key information for a table

        Queries the DB's information_schema for Primary Key info.
        Parameters:
            - table: name of the table.  Can include a schema.

        Returns a dictionary in the format:
            {'pk_column_name_1': boolean_value, 'pk_column_name_2': boolean_value}
        Where the boolean value indicates if the column has a default value (e.g. autoincrement).
        """
        pkeys = {}

        # This query requires write permissions in some RDBMS (notabley PostgreSQL)
        query = """SELECT kcu.column_name,
                         (CASE WHEN c.column_default is not null THEN 1
                               ELSE 0 END) AS has_default
        FROM information_schema.key_column_usage kcu
        JOIN information_schema.table_constraints tc
          ON tc.constraint_name = kcu.constraint_name
        JOIN information_schema.columns c
          ON c.column_name = kcu.column_name and c.table_name = kcu.table_name
        WHERE tc.constraint_type = 'PRIMARY KEY'
          AND kcu.table_name like {} AND kcu.table_schema like {}""".format(
            self.named_parameter("tablename"),
            self.named_parameter("schemaname")
        )
        schema, tablename = self.split_schema(table)
        results = self.query(
            query,
            {"tablename": tablename, "schemaname": schema}
        )
        # If the table is actually a VIEW, or doesn't have a primary key,
        # we'll check the config
        # this only works for dataset main tables, not detail or lookup tables
        if not results:
            ds = [(dsname, dsmeta)
                  for dsname, dsmeta in self.datasets.items()
                  if dsmeta.get("main_table") == table]
            if ds:
                dsmeta = ds[0][1]
                pkeys = {
                    x: False
                    for x in dsmeta.get("id_columns", [])
                }
        else:
            pkeys = {
                r.get("column_name"): r.get("has_default")
                for r in results
            }
        return pkeys

    def get_report_list(self):
        """Fetch a list of all the reports defined in the db"""
        reports_raw = self.query(
            "SELECT * FROM {} ORDER BY label ASC".format(
                self.escape_object(self.reports_table)
            ))
        reports = {}
        for row in reports_raw:
            report = {
                'id': row['id'],
                'label': row['label'],
                'description': row['description'],
                'params': {}
            }
            # We need to extract a list of parameters from the queries.
            # Those are in the format :variable_name
            param_keys = re.findall('[^::]:(\w+)', row["query"])
            # uniquify
            param_keys = list(set(param_keys))
            param_opts = row.get('param_options') or {}
            for key in param_keys:
                opts = param_opts.get(key, {})
                ptype, suffix = util.get_param_type(key)
                ptype = opts.get('type', ptype)
                # build label
                if not opts.get('label'):
                    opts['label'] = key.removesuffix(suffix).replace('_', ' ').title()
                    if ptype == 'integer':
                        opts['label'] += ' (Integer)'
                    elif ptype == 'num':
                        opts['label'] += ' (Decimal)'
                if opts.get('required'):
                    opts['label'] += '*'

                # get choices for lookups
                if ptype == 'lookup' and 'choice_query' in opts:
                    opts['choices'] = self.query(opts['choice_query'])
                if ptype == 'lookup' and not opts.get('choices'):
                    raise Exception(
                        f'Parameter {key} specified as type lkp but no choices are specified')

                # set sorting value
                opts['order'] = opts.get('order', key)
                opts['type'] = ptype
                report['params'][key] = opts
            reports[row['id']] = report

        return reports

    def get_report_results(self, report_id, parameters, write_access=False):
        """Return results for the given Prepared report and parameters

        Results are returned in a dict that also includes metadata
        about the report:
        {
            "data": The report results as a list of dicts,
            "headers": List of the column names.
                       Does not include the datatables ID/Class fields
            "label":  A label for the report
            "template_name": The name of the report template to use.
        }
        """

        # Try to fetch the report information for report_id
        report_query = """
        SELECT label, query, template_name
        FROM {} WHERE id={}""".format(
            self.escape_object(self.reports_table),
            self.named_parameter("id")
        )
        report_info = self.query(report_query, {"id": report_id})
        if not report_info:
            raise Database.NoSuchRecord(
                "No such report (id: {})".format(report_id))
        report_info = report_info[0]

        # Get the SQL and prep it for our backend DB
        report_sql = report_info["query"]
        # queries are written with ":parameter_name" style parameters
        # need to replace these with db-appropriate parameter style
        report_sql = re.sub('([^::]):(?P<param>\w+)', '\g<1>{}'.format(
            self.named_parameter("\g<param>")), report_sql
        )

        # Run the report query and get the results
        results = self.query(report_sql, parameters, commit=write_access)

        # Build the headers list
        headers = [x[0] for x in self.cursor.description
                   if x[0] not in ('DT_RowId', 'DT_RowClass')]
        template_name = report_info["template_name"]
        return {
            "data": results,
            "headers": headers,
            "label": report_info["label"],
            "template_name": template_name
        }

    def get_custom_query_results(self, dataset, output_fields,
                                 conditions, order_by):
        """Return results for the custom-defined query

        Parameters:
            - dataset:  Name of the dataset to query
            - output_fields: List of fields to include in output
            - conditions:  List of dicts containing query conditions
            - order_by: List of tuples describing the ordering
        """

        # Fetch and check for valid dataset
        dsmeta = self.datasets.get(dataset)

        if not dsmeta:
            raise Database.NoSuchDataset(dataset)

        # create the "fields" list, a list of tuples where each tuple
        # is in the form (field_name, field_label)
        # filter out any invalid fields along the way
        fields = []
        valid_fields = self.get_headers(dsmeta["main_table"])
        lookup_tables = [x["table_name"] for x in dsmeta.get("lookups", [])]
        for _, raw_fieldname in sorted(output_fields.items()):
            if not raw_fieldname:  # empty result
                continue
            elif all([
                    '.' in raw_fieldname,
                    '(' in raw_fieldname,
                    ')' in raw_fieldname
            ]):
                # A dot and parenthesis indicates a composite field,
                # probably a file field.
                # Need to escape the table & field name separately from the subfield
                mainfield, subfield = raw_fieldname.replace('(', '').replace(')', '').split('.')
                base_fname = self.escape_object("{}.{}".format(
                    dsmeta["main_table"], mainfield
                ))
                if mainfield not in valid_fields:
                    continue
                fname = "({}).{}".format(base_fname, subfield)
                flabel = "{} {}".format(mainfield, subfield).replace('_', ' ').title()
            elif "." in raw_fieldname:  # this is a lookup field
                # The label should be the fieldname in the main table
                # minus any "_id" suffix
                table = raw_fieldname.rsplit(".", 1)[0]
                fname = raw_fieldname
                fbasename = raw_fieldname.split('.')[-1]
                if not all([
                        table in lookup_tables,
                        fbasename in self.get_headers(table)
                ]):
                    print('invalid field or table: {}, {}, {}'.format(fname, table, fbasename))
                    continue
                # Locate the main table field that connects to the lookup
                mt_field = list([
                    lkp for lkp in dsmeta["lookups"]
                    if lkp["table_name"] == table
                ][0].get("fk_columns"))[0]
                # Create a field label from the field name
                # by removing "_id", underscores, and making it titlecase
                flabel = mt_field[:-3] if mt_field.endswith("_id") else mt_field
                flabel = flabel.replace("_", " ").title()
            else:  # this is just a regular field
                if raw_fieldname not in valid_fields:
                    continue
                fname = self.escape_object("{}.{}".format(
                    dsmeta["main_table"], raw_fieldname)
                )
                flabel = raw_fieldname.replace("_", " ").title()
            fields.append((fname, flabel))

        # Generate the SQL and the parameters list
        query, qdata = self.build_custom_query(
            dsmeta.get("main_table"),
            dataset,
            dsmeta,
            fields,
            conditions,
            order_by
        )
        results = self.query(query, qdata)

        return {
            "data": results,
            "headers": [l for f, l in fields],
            "template_name": "default",
            "label": "Custom Report"
        }

    def build_custom_query(
            self,
            tablename,
            dataset,
            dataset_meta,
            output_fields,
            conditions,
            order_by=None
    ):
        """Create a SQL query for a custom report.

        Parameters:
          - tablename:  The name of the main table being queried
          - dataset_meta: The metadata for the dataset
          - output_fields: List of tuples in the form (name, label)
          - conditions: List of where conditions, as expected by
                        process_field_search_conditions()
          - order_by: List of (field, direction) to order by.

        Returns a tuple of sql_query, query_parameter_data
        """
        # Generate select clause
        select_fields = [
            '{} AS "{}"'.format(f, l)
            for f, l in output_fields
        ]
        # if this is the main table in the dataset, add the datatables fields
        if tablename == dataset_meta.get("main_table"):
            pk = '{}.{}'.format(tablename, dataset_meta.get("id_columns")[0])
            dt_fields = [
                "'{dataset}-'{concat}{pk} as {dt_rowid}".format(
                    dataset=dataset,
                    concat=self.driver.string_concat_operator,
                    pk=self.escape_object(pk),
                    dt_rowid=self.escape_object("DT_RowId")
                ),
                "'record' as {dt_rowclass}".format(
                    dt_rowclass=self.escape_object("DT_RowClass")
                )
            ]
            select_fields = dt_fields + select_fields

        select_clause = "SELECT {}".format(", ".join(select_fields))

        # Generate FROM clause
        joins = []
        join_template = "LEFT OUTER JOIN {table} ON {join_conditions}"
        for lkp in dataset_meta.get("lookups", []):
            if lkp["for_table"] != tablename:
                continue
            join_conditions = ", ".join(
                "{main_table}.{mt_field}={lkp_table}.{lkp_field}".format(
                    main_table=self.escape_object(tablename),
                    mt_field=self.escape_object(mt_field),
                    lkp_table=self.escape_object(lkp["table_name"]),
                    lkp_field=self.escape_object(lkp_field)
                )
                for mt_field, lkp_field in lkp["fk_columns"].items()
            )
            joins.append(join_template.format(
                table=self.escape_object(lkp["table_name"]),
                join_conditions=join_conditions
            ))
        from_clause = "FROM {table} {joins}".format(
            table=self.escape_object(tablename),
            joins=" ".join(joins)
        )
        # Generate WHERE clause
        filtered_conditions = {}
        for field, data in (conditions or {}).items():
            if not (data and data.get("operator")):
                # empty operators are ignore selections
                # empty data is a wierd corner-case bug
                continue
            elif "." not in field:
                filtered_conditions[
                    "{}.{}".format(tablename, field)
                ] = data
            else:
                filtered_conditions[field] = data

        wheres, qdata = self.process_field_search_conditions(filtered_conditions)
        where_clause = "WHERE {}".format(" AND ".join(wheres))

        # Generate ORDER BY clause
        order_bys = []
        for field, dir in order_by:
            if not field:
                continue
            if "." not in field:
                field = "{}.{}".format(tablename, field)
            dir = "DESC" if (dir == "desc") else "ASC"
            order_bys.append("{} {}".format(
                self.escape_object(field),
                dir
            ))
        order_by_clause = (
            "ORDER BY {}".format(", ".join(order_bys))
            if len(order_bys) > 0
            else ""
        )

        query = "{} {} {} {}".format(
            select_clause, from_clause, where_clause, order_by_clause
        )

        return query, qdata

    def get_report_name(self, report_id):
        """Get the name of a report given its id"""
        query = "SELECT label FROM {} WHERE id={}".format(
            self.escape_object(self.reports_table),
            self.named_parameter("id")
        )
        results = self.query(query, {"id": report_id})
        if len(results) > 0:
            return results[0]["label"]
        else:
            return ""

    def get_keyword_search_results(self, formdata):
        """Return results for a keyword search.

        Parameters:
          - formdata:  Raw form data from request.form

        Returns a dict in the following format:
        {
            dataset_name: {
                "headers": [list, of, headers],
                "results": [
                    { results row as dict of fields }
                ]
            }
        }

        """
        datasets = formdata.getlist("dataset")
        if "_any_" in datasets:
            datasets = self.datasets.keys()
        else:
            datasets = [d for d in datasets if d in self.datasets.keys()]
        if formdata.get("schema"):
            datasets = [d for d in datasets
                        if d.split(".")[0] == formdata.get("schema")]
        results = {}
        for dataset in datasets:
            results[dataset] = {}
            search_view = (self.datasets.get(dataset).get("search_view")
                           or self.datasets.get(dataset).get("main_view"))
            query, params = self.dbo.fulltext_search_query(
                search_view,
                self.get_headers(search_view),
                formdata.get("search_terms"),
                formdata.get("type") == "and"
            )
            results[dataset]["headers"] = self.get_headers(search_view)
            results[dataset]["results"] = self.query(query, params)
        #print(results)
        return results

    def get_name_search_results(self, formdata):
        """Return results for a name search."""
        datasets = formdata.getlist("dataset")
        if "_any_" in datasets:
            datasets = [
                key for key, meta
                in self.datasets.items()
                if "name_search_view" in meta]
        else:
            datasets = [d for d in datasets if d in self.datasets.keys()]
        if formdata.get("schema"):
            datasets = [d for d in datasets
                        if d.split(".")[0] == formdata.get("schema")]
        first_name = formdata.get("first_name", '').strip()
        last_name = formdata.get("last_name", '').strip()
        # Let's don't search if there're no search terms
        if not any([first_name, last_name]):
            return {}
        else:
            first_name = first_name + "%" if first_name else ''
            last_name = last_name + "%" if last_name else ''
        results = {}
        id_query = (
            # split these strings so {name_search_v} doesn't get formatted yet
            "SELECT id FROM {name_search_v} " +  # <- important!
            """WHERE ({fn} IS NULL OR {fn} = ''
                      OR lower(first_name) LIKE lower({fn}) )
            AND ({ln} IS NULL OR {ln} = ''
                 OR lower(last_name) LIKE lower({ln}) )
            """.format(
            fn=self.named_parameter("first_name"),
            ln=self.named_parameter("last_name")
        ))
        data_query = """SELECT * FROM {search_view}
                        WHERE "DT_RowId" IN %(ids)s """
        for dataset in datasets:
            results[dataset] = {}
            search_view = (self.datasets.get(dataset).get("search_view")
                           or self.datasets.get(dataset).get("main_view"))
            name_search_v = self.datasets.get(dataset).get("name_search_view")
            if not name_search_v:
                continue
            results[dataset]["headers"] = self.get_headers(search_view)
            results[dataset]["results"] = []
            ids = self.query(
                id_query.format(
                    name_search_v=self.escape_object(name_search_v)
                ),
                {"first_name": first_name, "last_name": last_name}
            )
            if ids:
                ids = tuple(["{}-{}".format(dataset, x["id"]) for x in ids])
                results[dataset]["results"] = self.query(
                    data_query.format(
                        search_view=self.escape_object(search_view)
                    ), {"ids": ids}
                )
        return results

    def process_field_search_conditions(self, search_fields):
        """Given a dictionary of data from a custom search/query form,
        Return a list of conditions (WHERE clause items for a parameterized query)
        AND processed query data to go along with the conditions.

        Query data will be processed to add wildcards and make the data safe for
        the datatype being matched.
        """
        conditions = ["1=1"]  # Start with a "True" condition
        qdata = {}
        operator_templates = {
            # Each value is a tuple containing a template for the query condition
            # and a template or transform function for the value.
            "equals": (
                """(({param1} IS NULL AND {fieldname} IS NULL)
                OR ({fieldname} = {param1}))""", "{}"
            ),
            "not_equals": (
                """(({param1} IS NULL AND {fieldname} IS NOT NULL)
                OR ({fieldname} != {param1}))""",
                "{}"
            ),
            "iequals": (
                "(lower(CAST ({fieldname} AS TEXT)) = lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "not_iequals": (
                "(lower(CAST ({fieldname} AS TEXT)) != lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "equals_any": (
                """(({param1} IS NULL AND {fieldname} IS NULL)
                OR ({fieldname} IN {param1}))""",
                lambda x: tuple(x) if isinstance(x, list) else (x,)
            ),
            "not_equals_any": (
                """(({param1} IS NULL AND {fieldname} IS NULL)
                OR ({fieldname} NOT IN {param1}))""",
                lambda x: tuple(x) if isinstance(x, list) else (x,)
            ),
            "contains": ("(lower(CAST ({fieldname} AS TEXT)) LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "not_contains": ("(lower(CAST ({fieldname} AS TEXT)) NOT LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "greaterthan": ("({fieldname} > {param1})", "{}"),
            "lessthan": ("({fieldname} < {param1})", "{}"),
            "between": ("({fieldname} BETWEEN {param1} AND {param2})", "{}"),
            "not_between": ("({fieldname} NOT BETWEEN {param1} AND {param2})", "{}"),
            "is_true": ("{fieldname}", ""),
            "is_false": ("NOT {fieldname}", ""),
            "is_null": ("{fieldname} IS NULL", ""),
            "is_blank": ("NULLIF({fieldname}, '') IS NULL", ""),
            "is_not_null": ("{fieldname} IS NOT NULL", ""),
            "is_not_blank": ("NULLIF({fieldname}, '') IS NOT NULL", ""),
            # File operators
            "name_iequals": (
                "(lower(CAST (({fieldname}).name AS TEXT)) = lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "name_not_iequals": (
                "(lower(CAST (({fieldname}).name AS TEXT)) != lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "name_contains": ("(lower(CAST (({fieldname}).name AS TEXT)) LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "name_not_contains": ("(lower(CAST (({fieldname}).name AS TEXT)) NOT LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "type_iequals": (
                "(lower(CAST (({fieldname}).mime_type AS TEXT)) = lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "type_not_iequals": (
                "(lower(CAST (({fieldname}).mime_type AS TEXT)) != lower(CAST ({param1} AS TEXT)))",
                "{}"
            ),
            "type_contains": ("(lower(CAST (({fieldname}).mime_type AS TEXT)) LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "type_not_contains": ("(lower(CAST (({fieldname}).mime_type AS TEXT)) NOT LIKE lower(CAST ({param1} AS TEXT)))", "%{}%"),
            "size_equals": (
                """(({param1} IS NULL AND {fieldname} IS NULL)
                OR (({fieldname}).size = {param1}))""", "{}"
            ),
            "size_greaterthan": ("(({fieldname}).size > {param1})", "{}"),
            "size_lessthan": ("(({fieldname}).size < {param1})", "{}"),
            "size_between": ("(({fieldname}).size BETWEEN {param1} AND {param2})", "{}"),
            "size_not_between": ("(({fieldname}).size NOT BETWEEN {param1} AND {param2})", "{}"),
        }

        if self.regex_support:
            operator_templates['regex_match'] = (
                "{fieldname} " + self.driver.regex_operator + " {param1}",
                "{}"
            )
            operator_templates['regex_not_match'] = (
                "(NOT {fieldname} " + self.driver.regex_operator + " {param1})",
                "{}"
            )

        for fieldname, fielddata in search_fields.items():
            # Get the template for the condition and the value
            ctemplate, vtemplate = operator_templates.get(fielddata.get("operator"), (None, None))
            if not ctemplate and not vtemplate:
                continue
            conditions.append(ctemplate.format(
                fieldname=self.escape_object(fieldname),
                param1=self.named_parameter(fieldname),
                param2=self.named_parameter(fieldname + "2")
            ))
            if vtemplate:
                # The value transform can be a format string or callable
                if isinstance(vtemplate, str):
                    qdata[fieldname] = vtemplate.format(fielddata.get("value"))
                elif hasattr(vtemplate, '__call__'):
                    qdata[fieldname] = vtemplate(fielddata.get("value"))
                if (
                    fielddata.get("operator")
                    in ['between', 'not_between',
                        'size_between', 'size_not_between']
                ):
                    qdata[fieldname + "2"] = vtemplate.format(fielddata.get("value2"))
            else:
                qdata[fieldname] = fielddata.get("value")
            # In the event we're comparing integers, dates, times, or booleans,
            # we need to make sure empty strings
            # are converted to NULLs.
            field_data_type = fielddata.get("fieldtype", "text")
            if field_data_type in (
                'number', 'date', 'datetime', 'boolselect',
                'time', 'datetime-local', 'checkbox'
            ) and fielddata.get("value") == '':
                qdata[fieldname] = None

        return conditions, qdata

    def get_field_search_results(self, formdata):
        """Return results for a field search."""
        dataset = formdata.get("dataset")
        if (
            dataset not in self.datasets.keys()
            or not formdata.get("search_fields")
        ):
            return {}
        conditions, qdata = self.process_field_search_conditions(
            formdata.get("search_fields")
        )
        main_table = self.datasets.get(dataset).get(
            "field_search_view",
            self.datasets.get(dataset).get("main_table")
        )
        pk_list = list(self.get_primary_keys_for_table(main_table).keys()) or ["id"]
        results = {dataset: {"headers": [], "results": []}}
        query = "SELECT {} from {} WHERE {}".format(
            ", ".join(pk_list),
            self.escape_object(main_table),
            " AND ".join(conditions)
        )
        ids = self.query(query, qdata)
        if ids:
            search_view = (self.datasets.get(dataset).get("search_view")
                           or self.datasets.get(dataset).get("main_view"))
            results[dataset]["headers"] = self.get_headers(search_view)
            results[dataset]["results"] = []
            query = """SELECT * FROM {search_view}
                       WHERE "DT_RowId" IN %(ids)s"""
            ids = tuple(["{}-{}".format(dataset, x[pk_list[0]]) for x in ids])
            results[dataset]["results"] = self.query(
                query.format(
                    search_view=self.escape_object(search_view)
                ), {"ids": ids}
            )
        return results

    def get_autocomplete_values(self, dataset=None, fieldname=None, term=None, **kwargs):
        """Return a list of possible completions for a field in a given dataset

        "fieldname" is going to be formatted like it is in the form.
        """

        # In some cases, dataset, fieldname, or term are sent as one-item lists.
        # we need to just get the item
        if isinstance(dataset, list):
            dataset = dataset[0]
        if isinstance(fieldname, list):
            fieldname = fieldname[0]
        if isinstance(term, list):
            term = term[0]

        if dataset not in self.datasets.keys():
            raise Database.NoSuchDataset(dataset)

        dsmeta = self.datasets.get(dataset)
        if not fieldname.startswith("_details"):
            table = dsmeta.get("main_table")
            field = fieldname if fieldname in self.get_headers(table) else None
        else:
            # we have to break up the details from the name.
            # should be in the format "_details[table][rownum][fieldname]"
            matches = re.match(r"_details\[(.*?)\]\[(.*?)\]\[(.*?)\]", fieldname)
            if not matches:
                raise Database.NoSuchField(fieldname)
            table, _, field = matches.groups()  # _ is the ordinal, which we don't use
            field = field if field in self.get_headers(table) else None
        if not field:
            raise Database.NoSuchField(fieldname)
        if term:
            where = "WHERE {field} ILIKE {parameter} || {wildcard}".format(
                field=self.escape_object(field),
                parameter=self.named_parameter(field),
                wildcard=self.named_parameter("wildcard")
            )
            qdata = {field: term, "wildcard": "%"}
        else:
            where = ""
            qdata = None
        query = """SELECT distinct {field} as autocomplete FROM {table}
        {where} ORDER BY {field} ASC""".format(
            field=self.escape_object(field),
            table=self.escape_object(table),
            where=where
        )
        res = self.query(query, qdata)

        return [x["autocomplete"] for x in res] if res else []

    ###########
    # Setters #
    ###########

    # Define methods here to write data to the database

    def log_posting(self, dataset, record_key, operation, user):
        """Log a posting in the audit log table"""

        if not self.audit_table:
            return

        if dataset not in self.datasets:
            raise ValueError('Invalid dataset being logged: {}'.format(dataset))

        if operation not in ('C', 'U', 'D'):
            raise ValueError('Invalid operation being logged')

        query = (
            'INSERT INTO {}(dataset, record_key, operation, user_login)'
            ' VALUES (%(dataset)s, %(record_key)s, %(operation)s, %(user_login)s)'
        ).format(self.escape_object(self.audit_table))

        self.query(
            query,
            {'dataset': dataset, 'record_key': record_key, 'operation': operation, 'user_login': user},
            commit=True
        )

    def write_values_to_table(
            self, table, values_raw,
            new_record=True, delete=False
    ):
        """Write a set of values to the given table

        If "new record" is true, it will be an insert.  Otherwise,
            it'll be an update.
        If "delete" is true, the record will just be deleted without
            updating anything.
        """
        # You can't delete a new record
        if new_record and delete:
            raise Database.CannotDeleteRecord("Record does not exist yet.")
        # get field list
        results = self.get_schema_for_table(table)
        if not results:
            raise Database.NoSuchTable(table)
        fields = {
            row["column_name"]: row["data_type"]
            for row in results
        }
        primary_keys = self.get_primary_keys_for_table(table)
        values = {}
        # filter out fields that don't exist from the value list
        # and fix some datatype mismatch
        for vk in values_raw.keys():
            # if the value key is not in our list of fields, ignore it.
            if vk not in fields:
                continue
            # if there's a default value for a pk and it's a new record,
            # we need to remove the pk values from the datalist
            if new_record and primary_keys.get(vk):
                continue

            # if it's a file field and there's no value, skip
            if fields[vk] == 'file' and not values_raw[vk]:
                continue

            # if it's a file field with "_DELETE" as the value, set to null
            if fields[vk] == 'file' and values_raw[vk] == '_DELETE':
                values_raw[vk] = None

            # if we're still here, we need to add the raw value to the values dict
            values[vk] = values_raw[vk]

            # If it's  a field that can't take an empty string,
            # we need nulls instead
            no_empty_strings = (
                'boolean', 'date', 'timestamp', 'time', 'time without time zone',
                'timestamp without time zone', 'integer', 'smallint',
                'bigint', 'numeric', 'datetime', 'inet', 'cidr', 'macaddr'
            )
            if (  # Leave as "and"s instead of converting to "all([])"
                    fields[vk] in no_empty_strings
                    and isinstance(values[vk], str)
                    and values[vk].strip() == ''
            ):
                values[vk] = None

        valuekeys = values.keys()

        # if there are no valuekeys, and it's not a delete query,
        # there's nothing more to do here.
        if not delete and not [v for v in valuekeys if v not in primary_keys]:
            print(
                'WARNING: write_dataset_record returned None because there'
                ' were no actual values, just PKs.'
                ' This probably indicates a problem in your'
                ' database or configuration.'
            )
            return None

        if delete:
            action = "DELETE FROM {table}"
            predicate = "WHERE {where_list}"
            query = self.driver.dml_with_output_template.format(
                action=action,
                predicate=predicate,
                output=self.driver.output_clause_template.format(
                    output_field_list="NULL")
            )
            values = {
                k: v
                for k, v in values.items()
                if k in primary_keys.keys()
            }
            valuekeys = values.keys()
            where_list = " AND ".join(
                '{}={}'.format(
                    self.escape_object(f),
                    self.named_parameter(f)
                )
                for f in valuekeys
            )
            query = query.format(
                table=self.escape_object(table),
                where_list=where_list
            )
        elif new_record:  # Start writing an insert query
            action = "INSERT INTO {table} ({field_list})"
            predicate = "VALUES ({value_list})"
            pk_list = ", ".join(
                self.driver.output_clause_field_template.format(f)
                for f in primary_keys.keys()
            )
            query = self.driver.dml_with_output_template.format(
                action=action,
                predicate=predicate,
                output=self.driver.output_clause_template.format(
                    output_field_list=pk_list
                )
            )
            field_list = ", ".join(self.escape_object(f) for f in valuekeys)
            value_list = ", ".join(self.named_parameter(f) for f in valuekeys)
            query = query.format(
                table=self.escape_object(table),
                field_list=field_list,
                value_list=value_list,
                pk_list=pk_list
            )
        else:  # update query
            action = "UPDATE {table} SET {field_list}"
            predicate = "WHERE {where_list}"
            pk_list = ", ".join(
                self.driver.output_clause_field_template.format(f)
                for f in primary_keys.keys()
            )
            query = self.driver.dml_with_output_template.format(
                action=action,
                predicate=predicate,
                output=self.driver.output_clause_template.format(
                    output_field_list=pk_list
                )
            )
            valuekeys = [
                v
                for v in valuekeys
                if v not in primary_keys.keys()
            ]
            field_list = ", ".join(
                '{} = {}'.format(
                    self.escape_object(f),
                    self.named_parameter(f)
                )
                for f in valuekeys
            )
            where_list = " AND ".join(
                '{} = {}'.format(
                    self.escape_object(f),
                    self.named_parameter(f)
                )
                for f in primary_keys.keys()
            )
            query = query.format(
                table=self.escape_object(table),
                field_list=field_list,
                where_list=where_list,
                pk_list=pk_list
            )

        try:
            return self.query(query, values, True)
        except self.driver.dbapi.IntegrityError:
            if delete:
                raise Database.CannotDeleteRecord(
                    "Record has references in other tables."
                )
            raise

    def write_dataset_record(self, dataset, **values):
        """Given a dict of all the values of a dataset record,
           write them to the database.

        Values is a dict of "fieldname":"value" pairs.
        The special key "_details" is a dictionary with detail table names and
        records to be written to those detail tables.
        The special key "_delete", if true, will cause the record and all
        detail records to be deleted.
        """
        if dataset not in self.datasets.keys():
            raise Database.NoSuchDataset(dataset)

        #print("VALUES in write_dataset_record: " + values.__str__())
        # write to the main table
        table = self.datasets.get(dataset).get("main_table")
        new_record = bool(values.get("new_record"))
        detail_values = values.get("_details", {})
        delete_record = values.get("_delete")
        # If we're deleting, we'll have to delete details first
        if not delete_record:
            result = self.write_values_to_table(table, values, new_record)
        else:
            result = [{"id": None}]
        #print(result)
        if new_record:  # need to put the primary key values into the details
            for pk_col, pk_val in result[0].items():
                #print(pk_col, pk_val)
                # string expected as this would normally come from an HTML form
                values[pk_col] = str(pk_val)
            #print("values2: ", values)

        # The frontend expects a single ID field called "id"
        # if this isn't our PK, generate the field by combining
        # the actual PK values with a pipe
        if "id" not in result[0]:
            result[0]["id"] = '|'.join([str(x) for x in result[0].values()])

        # write to detail tables
        detail_tables = [
            x["table_name"]
            for x in self.datasets.get(dataset, {}).get("details", [])
        ]
        #print("detail_values: " + str(detail_values))
        result[0]['_details'] = {}
        for detail_table in detail_tables:
            table_meta = [
                x for x in self.datasets.get(dataset).get("details")
                if x.get("table_name") == detail_table
            ][0]
            result[0]['_details'][detail_table] = []
            #print("table: " + str(table_meta))
            detail_tables_values = detail_values.get(detail_table, {})
            #print("detail values for %s" %(table) + str(detail_tables_values))
            for rnum in sorted(detail_tables_values.keys()):
                dtv = detail_tables_values[rnum]
                #print(dtv)
                if delete_record:
                    # If we're deleting the record, we want to delete existing
                    # details.  We can't delete new records, obviously
                    dtv["_delete"] = not dtv.get("new_record") and True
                    # Set "_dirty" to false, because there's no point updating
                    # or creating anything if we're deleting the parent record
                    # Since existing records have "_delete" set, this will just cause
                    # new records to be ignored
                    dtv["_dirty"] = False
                if dtv.get("_dirty") or dtv.get("_delete"):
                    for foreign, local in table_meta.get("fk_columns").items():
                        #print("Set {} to values[{}] == {}".format(local, foreign, values[foreign]))
                        dtv[local] = values[foreign]
                    #print("row of detail tables values: ", str(dtv))
                    detail_res = self.write_values_to_table(
                        detail_table,
                        dtv,
                        bool(dtv.get("new_record")),
                        bool(dtv.get("_delete"))
                    )
                    if not detail_res:
                        raise Exception(
                            'A call to write_values_to_table for {detail_table} with values {dtv} returned no results.'.format(
                                detail_table=detail_table, dtv=dtv) +
                            '  This usually indicates a problem with submitted data.'
                        )
                        print(detail_table, dtv)
                    else:
                        detail_res = detail_res[0]
                    detail_res['_ordinal'] = rnum
                    result[0]["_details"][detail_table].append(detail_res)
        if delete_record:
            result = self.write_values_to_table(table, values, new_record, True)
            #print(result)
        return result
