"""These functions are middle-ware between the database and flask for POST callbacks."""

import os
import shutil
from flask import g, request, session, abort
from . import util
from .db.database import Database


def delete_file(table, record_id, field):
    """Delete a file attached to a particular table record."""

    path = util.upload_path(table, record_id, field)
    try:
        shutil.rmtree(path)
        print("Deleting {}".format(path))
    except FileNotFoundError:
        print("{} does not exist, not deleting".format(path))


def save_file(table, record_id, field, file_obj):
    """Save a file attached to a particular table record.

    file_obj is a werkzeug FileData object
    """
    save_path = util.upload_path(table, record_id, field)
    print(save_path)
    try:
        os.makedirs(save_path)
    except FileExistsError:
        pass  # we just want it to exist; if it already does, no problem

    file_obj.save(os.path.join(save_path, file_obj.filename))

    return save_path


def write_dataset_record(dataset, **values):
    """Write a posted set of values and/or files to the dataset"""

    try:
        ds_meta = g.datasets[dataset]
    except KeyError:
        raise Database.NoSuchDataset(dataset)

    if not util.check_dataset_write_permissions(dataset):
        abort(403)

    def build_pk(values, table):
        """Build an id string from the pk values in the table"""

        primary_key = '-'.join([
            str(values[pkf])
            for pkf
            in g.db.get_primary_keys_for_table(table)
        ])
        return primary_key

    # figure out if any files are being deleted
    deleting_record = values.get('_delete', False)
    file_fields = g.db.get_file_fields()
    main_table = ds_meta["main_table"]

    # Check for file fields in the main table.
    # If there are and any of them are being deleted,
    # or if the whole record is being deleted,
    # Delete the files
    if file_fields.get(main_table):
        for field in file_fields[main_table]:
            if values.get(field) == '_DELETE' or deleting_record:
                #values[field] = None
                record_id = build_pk(values, main_table)
                delete_file(main_table, record_id, field)

    # Now do the same for file fields in the detail tables
    for table, rows in values.get('_details', {}).items():
        for row in rows.values():
            deleting_detail = row.get('_delete', False)
            for field in file_fields.get(table, []):
                if any([
                        row.get(field) == '_DELETE',
                        deleting_detail,
                        deleting_record
                ]):
                    #row[field] = None
                    record_id = build_pk(row, table)
                    delete_file(table, record_id, field)

    # This section will extract the necessary data from the files object
    # and replace the filename in the form data with a tuple of
    # the name, mimetype, and size so that we can populate our compound file object in SQL
    for field_name, file_obj in request.files.items():
        if not file_obj.filename:  # The record was submitted without a file
            continue

        is_detail = field_name.startswith('_details')
        # get the actual file length by seeking the end and calling tell()
        file_obj.seek(0, 2)
        file_info = (
            file_obj.filename,
            file_obj.mimetype,
            file_obj.tell()
        )
        # rewind the file so it can be saved
        file_obj.seek(0)
        if is_detail:
            table, ordinal, field = util.parse_detail_name(field_name)
            values['_details'][table][ordinal][field] = file_info
        else:
            values[field_name] = file_info

    # write the database record
    dataset_write_results = g.db.write_dataset_record(dataset, **values)

    # For each file, create directory and save file
    # files are saved in <uploads_folder>/<table>/<pk>/<fieldname>/
    # So we need to determine each of those bits
    for field_name, file_obj in request.files.items():
        if not file_obj.filename:  # the record was submittted without a file
            continue

        is_detail = field_name.startswith('_details')
        if not is_detail:  # file field in the main table
            table = ds_meta['main_table']
            record_id = build_pk(dataset_write_results[0], table)
            field = field_name
        else:
            # find the row in the write results that matches our file field
            # i.e., they have the same ordinal
            # generate a save path from that table, pk, and field
            table, ordinal, field = util.parse_detail_name(field_name)
            detail_write_results = [
                x
                for x in dataset_write_results[0]['_details'][table]
                if x["_ordinal"] == ordinal
            ]
            if not detail_write_results:  # there was no file uploaded
                continue
            detail_write_result = detail_write_results[0]
            record_id = build_pk(detail_write_result, table)

        save_file(table, record_id, field, file_obj)

    # log the posting operation
    # If this is a delete or update, we can get the key value from the submission
    # Otherwise, we need to get it from the database write results.

    if values.get('_delete'):
        opcode = 'D'
        record_id = build_pk(values, main_table)
    elif values.get('new_record'):
        opcode = 'C'
        record_id = build_pk(dataset_write_results[0], main_table)
    else:
        opcode = 'U'
        record_id = build_pk(values, main_table)
    g.db.log_posting(dataset, record_id, opcode, session['username'])

    return dataset_write_results
