-----------------
-- Basic Setup --
    -----------------

-- Edit this with your application user
-- Then do a search & replace for "my_user"

CREATE ROLE testuser LOGIN
    ENCRYPTED PASSWORD '$1$hc6B8Fso$bnQaRV0JVXsIGDyOBHZcu/'
    NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

CREATE DATABASE framework_test
    WITH OWNER = testuser
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    CONNECTION LIMIT = -1;


    -------------
-- Reports --
    -------------

CREATE TABLE reports
    (
	id serial NOT NULL,
	label character varying(255),
	query text,
	template_name character varying(255),
	description text,
	CONSTRAINT reports_pkey PRIMARY KEY (id)
	)
    WITH (
	OIDS=FALSE
	);
    ALTER TABLE reports
    OWNER TO testuser;
    GRANT ALL ON TABLE reports TO testuser;

    ------------------
-- Simple Table --
    ------------------

-- A flat table with no joins, but most common data types

CREATE TABLE simple_table
    (
	id serial PRIMARY KEY,
	t_varchar varchar(255),
	t_text text,
	t_integer integer,
	t_numeric numeric(12, 2),
	t_date date,
	t_timestamp timestamp,
	t_bool bool
	)
    ;
    ALTER TABLE simple_table OWNER TO testuser;


-- Corresponding datatables view

    CREATE VIEW simple_table_dtv AS (
    SELECT (
	    'simple_table-' || id)::TEXT as "DT_RowId",
	'record' AS "DT_RowClass",
	t_varchar,
	t_text,
	t_integer,
	t_numeric,
	t_date,
	t_timestamp,
	t_bool
    FROM simple_table
	)
    ;
    ALTER VIEW simple_table_dtv OWNER TO testuser;

-- add some data to the table
INSERT INTO simple_table VALUES
(DEFAULT, 'varchar', 'text text', 1024, 41.2,  '2012-12-31', '2012-07-04 14:25:00', True),
(DEFAULT, 'Test', 'something yada yada', 2, 1.0,  '1964-10-13', '2012-12-12 12:00:03', False)
;
-------------
-- Dataset --
-------------

-- A basic dataset, with some joins to lookup/detail tables
CREATE TABLE lookup_1
    (
	id serial PRIMARY KEY,
	name TEXT,
	ordinal_number integer
	);
    ALTER TABLE lookup_1 OWNER TO testuser;

INSERT INTO lookup_1 (name, ordinal_number) VALUES ('second', 2), ('first', 1), ('third', 3), ('fifth', 5), ('fourth', 4);

CREATE TABLE main_table
    (
	my_record_id bigserial PRIMARY KEY,
	"name with spaces" text,
	"this name's got an apostrophe" integer, -- '
	"ampersand & name" bool,
	lookup_1_id integer REFERENCES lookup_1(id)
	);
    ALTER TABLE main_table OWNER TO testuser;


CREATE VIEW main_table_dtv AS
    (
    SELECT
	('main_table-'||my_record_id) AS "DT_RowId",
	'record' AS "DT_RowClass",
	"name with spaces",
	"this name's got an apostrophe", --'
	"ampersand & name"
    FROM main_table
	)
    ;
    ALTER TABLE main_table_dtv OWNER TO testuser;

CREATE TABLE detail_1
    (
	id serial PRIMARY KEY,
	main_table_record_id bigint REFERENCES main_table(my_record_id),
	label_field TEXT,
	somevalue date
	)
    ;
    ALTER TABLE detail_1 OWNER TO testuser;

CREATE TABLE detail_2
    (
	main_table_record_id bigint REFERENCES main_table(my_record_id),
	post_time timestamp,
	notes text,
	CONSTRAINT detail_2_pkey PRIMARY KEY (main_table_record_id, post_time)
	)
    ;
    ALTER TABLE detail_2 OWNER TO testuser;
