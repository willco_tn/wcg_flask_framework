-----------------
-- Basic Setup --
-----------------

-- This file is written for PostgreSQL.  May require rewriting for other DB's.
-- Do a search & replace for "my_user" with your username
-- And a search and replace for "my_db" with your database

CREATE ROLE my_user LOGIN
  ENCRYPTED PASSWORD 'md5 encrypted password'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

CREATE DATABASE my_db
  WITH OWNER = my_user
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

USE my_db;

-------------
-- Reports --
-------------

CREATE TABLE reports
(
  id serial NOT NULL,
  label character varying(255),
  query text,
  template_name character varying(255),
  description text,
  param_options JSONB,
  CONSTRAINT reports_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE reports
  OWNER TO my_user;
GRANT ALL ON TABLE reports TO my_user;


---------------
-- AUDIT LOG --
---------------

DROP TABLE IF EXISTS audit_log CASCADE;
CREATE TABLE audit_log (
	id BIGSERIAL PRIMARY KEY
	,TIMESTAMP TIMESTAMP DEFAULT NOW()
       ,dataset VARCHAR(1024)
       ,record_key VARCHAR(1024)
       ,"operation" CHAR(1) CHECK("operation" IN ('C', 'D', 'U'))
       ,user_login VARCHAR(1024)
       );
ALTER TABLE audit_log OWNER TO my_user;



--------------------
-- File Data Type --
--------------------

CREATE TYPE file AS (
	name character varying (1024) --File Name
	,mime_type character varying(1024)-- MIME Type
	,size bigint -- size in bytes
	)
    ;
