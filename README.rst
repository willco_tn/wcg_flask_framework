=========================
 The WCG Flask Framework
=========================

---------------------
Written by Alan Moore
---------------------


Abstract
========

WCG Flask Framework is a framework for making database-driven web applications quickly and simply.  It was conceived as a way to migrate Paradox or Access applications quickly to the browser.

It's written for Python and Flask with JQuery/HTML5 on the front-end.

Some key highlights:

- Written for Postgresql, but can potentially support SQLServer 2012, MySQL, and SQLite.
  - Support for file upload fields is currently postgresql-only
  - Non-PostgreSQL databases are untested and probably bug-ridden
- LDAP auth against edirectory or Active Directory
- Easily extensible from downstream
- Nice AJAX + JQuery-UI front-end.
- Full reporting and search functionality
- Configurable access controls


Limitations
-----------

WCGFF is written with a certain kind of database in mind, and will work best with databases that have these features:

- Database is a PostgreSQL database
- Database is made up of fairly simple relational datasets, where a main "records" table is attached to "lookup" (many-to-one) and "detail" (one-to-many) tables.  Many-to-Many relations are possible, but are not very optimal without a lot of custom work.
- Tables have a single, numeric primary key column called "id". Support for compound Primary keys is experimental.
- Security needs are simple and write-access can be system-wide.

Things that WCGFF just simply doesn't support right now:

- Tables with multi-column primary keys (maybe; experimental support only)
- "Effective-date" based tables (i.e. where record updates don't overwrite old records, just insert a new one with an updated date/sequence number).
- JSON data fields.
- Granular permissions (per table/per field/etc.), with the exception of being able to hide datasets by group


Creating a Project with WCG Flask Framework
===========================================


Setting up the project directory
--------------------------------

Clone WCGFF
~~~~~~~~~~~

Using git, clone the wcg_flask_framework repository into a new folder named for your project::

    git clone /path/to/wcg_flask_framework /path/to/my_new_application


Fix upstream relationship
~~~~~~~~~~~~~~~~~~~~~~~~~

Now, remove the "origin" remote and repalce it with a non-tracking "upstream" remote::

    git remote remove origin
    git remote add upstream /path/to/wcg_flask_framework

This will allow you to pull in updates to wcg_flask_framework, but not accidentally push your application back up to the framework.


Create the environment
~~~~~~~~~~~~~~~~~~~~~~

Enter the project directory and create the python virtual environment by executing the script::

    cd my_new_application
    bash noncode/create_environment.sh

Test
~~~~

You should now be able to run your new application with this command::

   env/bin/python main.py

Check http://localhost:5000 to see if your empty application is running.


Keep Updated
~~~~~~~~~~~~

When fixes or features are added to WCG Flask Framework, and you want to incorporate them in your application, simply run this::

    git fetch upstream
    git merge upstream/master -m "merge upstream changes"

Assuming you have properly left the upstream files alone and only edited local override files, this should merge any changes you need without impacting your application negatively.


Configuring the application
---------------------------

Your application will be configured by the file "my_application/instance/config.py".  To get started, copy the "config.py" file from the project directory into the "instance" subdirectory, and open the copy in your editor.

General Options
~~~~~~~~~~~~~~~

SECRET_KEY
  This should be something random and unique for each application.  It's used for security purposes.  For more information, check the Flask documentation.

APPLICATION_NAME
  This is the name of the application that will appear at various places where an application name is required (e.g. HTML page titles).

ORGANIZATION_NAME
  This is the organization that owns the application.  It'll appear in the footer.

DEFAULT_CALLBACK
  This is the callback for the main page.  It should be a routed function in ``main.py`` or ``includes/local_routes.py``.

SEARCHES
  This tuple of strings defines which search forms are enabled and in what order they appear.  Values are ``keyword``, ``name``, and ``field``.

FIELD_SELECTORS
  A string value that defines how field input widgets are presented in the field search and custom reports forms.  ``dynamic`` will allow the user to select fields from a dropdown and only show selected fields, ``explicit`` will display all fields by default, whether they are used or not.

SEARCH_BY_SCHEMA
  Set this to "true" if the database uses multiple schemas, and you want to be able to filter searches by schema.

NAVIGATION
  This dict defines the navigation links to include in the header, and for each one defines the permissions required for access and the order that it should appear.  A Permission of "None" means anyone with a valid session can access the page; to lock it down, set the permission to a string that matches a session key which must be truthy to allow access.  E.g., if "permission" is set to "is_admin", a user must have a session variable called "is_admin" and that variable must be truthy, or else they won't be able to navigation to the page.

BASE_STYLESHEET
  The name of a stylesheet, found in static/css/, which will be used as the base for the application.  This is kind of a legacy setting, it should really just be left at "style.css", or not included at all.

LOCAL_STYLESHEETS
  This is a list or tuple of stylesheet filenames (located in static/css) which should be included in the headers.  "county_style.css" is included in the distribution, and you can also add local instance-only stylesheets to override values and add local styles.  They will be applied in the order listed.

LOCAL_JS_FILES
  This is a list or tuple of javascript files located in the "static/js" directory which should be included in the headers.  They will be included at the end, so they'll have access to all wcgff functions.

JQUERY_UI_CSS_PATH
  If you want to use a different jquery-ui theme, install your theme to a subdirectory of "static/css" and put the directory name here (relative to "static/css").

REPORTS_TABLE
  The name of the table which will hold the reports.  It's usually "reports", but if that clashes with something in your database, you can change it here.

REPORT_TYPES
  This list/tuple of strings defines which report modules are enabled.  ``prepared`` presents reports saved in the `REPORTS_TABLE` table, while ``custom_dataset`` presents the user-defined report form.

DEFAULT_REPORT_TEMPLATE
  The name, without file extension, of the default report template.  This will be used for all custom reports, and whenever a prepared report uses 'default' as a report template.

SITE_INDEX_TEMPLATE
  This is a template that will override the default "site_index", if you wish to customize it.

WCG_JS_VERSION
  This is a date string which will be prepended to the names of all javascript files in an attempt to avoid caching issues.  It is typically updated whenever the javascript is changed on the site.

UPLOAD_FOLDER
  This is the path to the directory under which file uploads are stored.

Database Configuration
~~~~~~~~~~~~~~~~~~~~~~

The DB dict defines connectivity to your application's database.  The options are:

type
  Which RDBMS you're using.  Possible values are postgresql, sqlite, mysql.  Currently only postgresql is fully supported.

name
  The database name

host
  The hostname or IP address where the database is located

user
  The username to connect with

password
  The password to connect with

The connection account should have read/write privileges on the database and any objects you need to access, or some things won't work right.


Authentication Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The AUTH dict defines how you want users to authenticate.

type
  Which auth backend to use.  Valid values are "dummy", "active directory", and "edirectory".

args
  The arguments to send to the backend constructor.  See below.

level
  The auth level defines what kind of overall authorization schema the site uses.  See below.

groups
  The groups dict is a mapping between auth source groups and session variables.  For example, if you set groups to `{"admins": "is_admin"}`, that means that a user logging in who is a member of the "admins" in the authenticating directory (e.g. Active Directory) will get a session variable called "`is_admin`".  By convention, I designate group membership session variables with "`is_`".

Auth levels
+++++++++++

There are four auth levels, designated with an integer from 0 to 3.

Level 0
  This is Public read/write.  Anyone visiting the site can read and write to the tables without restriction and without logging in.

Level 1
  This is Public read, Private write.  Anyone visiting the site can read the data, but users must log in to write to the data.

Level 2
  This is Private read/write.  Visitors must log in to even see the data, but once logged in everyone can write.

Level 3
  This is private read, restricted write.  Visitors must log in to see the data, and only members of a special group can write.

LDAP Args
+++++++++

The following arguments are for LDAP-based authentication (eDirectory, Active Directory).

server
  The host to contact for authentication.

require_group
  Require membership in this group for authentication.  Should just be a group name.

ssl
  Whether or not to use SSL to connect.

base_dn
  This is only for AD.  This is the base dn from which to search for users and groups.

bind_dn
  The DN of a user used to bind to the directory and do searches.  Typically you want a special user designated for just this purpose; don't use anyone's actual user account here.

bind_pw
  The password for the user given in bind_dn.

write_group
  Membership in this group adds the "write_access" session key to a user's session on login.  It's meaningless if your auth level is less than 3, otherwise it defines who can write to the database.

Dummy args
++++++++++

The Dummy auth backend is mainly for testing/demo purposes only.

users
  This is a list or tuple of dictionaries defining the users.  Each dict has a "username", "password", "user_fullname", and "groups".  "groups" is a list of groups the user is a member of.

write_group
  The group whose members will be granted write_access.


DataSets Configuration
~~~~~~~~~~~~~~~~~~~~~~

The DATASETS dict is the heart of the configuration.  It tells the application important things about the tables you want to expose, how they relate to one another, and how you want to view and search them.  A "dataset" basically means a set of tables that relate to one type of information, typically a main table and a set of lookup and detail tables.

Each entry in the dict is a dataset name followed by another dict describing aspects of that dataset.  The dataset name should not contain spaces or wierd symbols, as it will be used as a "key" for the dataset (used in URLS, e.g.).

The dict contains the following members:

label
  A display name for the dataset, to be used in titles, headers, menus, etc.

main_table
  The main data table for the dataset.

main_view
  This is a view which will be used to show records from the main table on the main dataset screen.  It needs to be specially formatted for the DataTables jquery plugin, meaning it should have fields Dt_RowId and Dt_RowClass.

search_view
  This is a view that needs to be formatted similar to the main_view, but will be used for searching and search results.  If it isn't defined, the main_view is used instead.

name_search_view
  If name searching is enabled, this view will be used for searching by name.  See the database documentation.

field_search_view
  If field searching is enabled, this view will be used for doing a custom search by fields.  See the database documentation.

id_columns
  This is a tuple of the primary key fields in the main_table.  Probably you only have one, so make sure to add the hanging comma so that you have a tuple.  In theory the application should support multiple PK fields, but this isn't entirely tested and some UI elements may fail.

limit_visibility
  This is a list of session keys, any of which must be `True` in a user's session for the dataset to be visible.  For example, if this value is `['write_access']`, only users with write access will be able to see the dataset.  For others, it will not seem to exist.

lookups
  This is a tuple of dicts defining the lookup tables.  Lookup tables are one-to-many relationships with the main table. See more below.

details
  This is a tuple of dicts defining the detail tables, or many-to-one relationships to the main table. See more below.


Lookup Entries
++++++++++++++

Each lookup table entry is a dict with the following fields:

label
  What to call the lookup table in human terms.

table_name
  The name of the table in the database

for_table
  Which table the lookup table is a lookup for.  It doesn't have to be the main_table, e.g. it could be one of the detail tables.

fk_columns
  This is a dict in which the key represents a column in the "for_table" table and the value represents a columsn in the lookup table.

display_column
  This is the field in the lookup table which will be displayed in a select list to represent the lookup value.

order_by
  This is a list of tuples defining how the lookup should be ordered.  Each tuple is in the form (fieldname, direction) where direction is either "ASC" or "DESC".

Detail Entries
++++++++++++++

Each detail entry is a dict with these fields:

label
  What to call the detail table, in human terms.

table_name
  The name of the table in the database

fk_columns
  This is a dict of fields that link the detail table to the main table.  The key is a column name in the main table, the value is a column name in the detail table.

order_by
  This is a list of tuples.  Each tuple is a field name and either "ASC" or "DESC".  The detail records will be ordered by the field names in the specified direction.


Database Configuration
----------------------

WCG Flask framework relies heavily on the database structure for formatting and displaying data.  It can work fine with an existing database, but there are some views and tables that will need to be created for it to work properly.

main_view configuration
~~~~~~~~~~~~~~~~~~~~~~~

The main_view for a table will be used to create the table seen on the datasets index.  Whatever fields you include, and however you name them, is what users will see.

The view also needs to additional fields, DT_RowId and DT_RowClass.  Case matters, so make sure to proprly quote these names when defining the view.

DT_RowId
  This should be in the format "datasetname-pkvalue".  The pkvalue is the value of the table's primary key field; this is one place where multiple PK fields are not supported yet.  The application will use this value to navigate to the correct record when clicked.

DT_RowClass
  This should be set to "record".  If you want additional classes defined, you can do that here as well.

search_view configuration
~~~~~~~~~~~~~~~~~~~~~~~~~

The search view needs the same DT_* fields that the main_view needs.  Any field against which a user should be able to search should be included in this view.  It will be used to display search results, so make sure to give friendly names to the fields.

name_search_view configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a special view for use with the name search.  It has 3 columns:

id
  This is an id for a record in the main_table

first_name
  A first name associated with the main_table record

last_name
  A last name associated with the main_table record

This is basically used when you want to allow people to search specifically by names that exist somewhere in the main_table.

field_search_view configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Field search views primarily exist to hide, rename, or reorder fields in the main table for field searching.  They can also be used to add detail table fields into the field search.

Field search views have the following limitations:

+ The main table must use "id" as a primary key
+ The main table's "id" column must appear verbatim
+ Any lookup fields should be left as-is if you want to keep the drop-down functionality
+ Detail fields will need to be aggregated to text fields


reports_table
~~~~~~~~~~~~~

The reports table hold reports for the reporting engine.  It can be called whatever you wish, but the default is "reports".  It has six columns:

id
  A simple incrementing integer primary key

label
  What the report is called

query
  The SQL query that will be used to generate the report output.  This should be a SELECT query; do not use any DML!!

template_name
  The name of a template to be used for displaying the query data.  "default" is used if you don't have one defined.

description
  A text description of the query and how it's used (parameters, etc.)

param_options
  An optional JSONB field containing a JSON object describing options for the parameters.


Writing report queries
++++++++++++++++++++++

Report queries are just SQL SELECT statements that generate a table output.  They can take parameters if you wish, and these are automatically prompted to the user.  Just include them in the form ":parameter_name" and the report engine will automatically generate an input field for them when the report is selected.

If you want to include special input types, there are some suffix codes you can put on your parameter to make the report engine create certain types of input controls:

_tf
  This will create a select with options "TRUE" and "FALSE".  Good for boolean fields.

_atf
  Like _tf, but it will also have an "any" option which will return the value "any".  Make sure your query ignores the field when the input is "any" (e.g., "WHERE (:some_field_atf = 'any' OR some_field::text = :some_field_atf)").

_num
  Creates an HTML5 number spinbox with a step of .01.  Use for decimal/float values.

_int
  Creates an HTML5 number spinbox with a step of 1.  Use for integer values.

_date
  Creates an HTML5 date field.

_lkp
  Creates a SELECT field.  Requires param_options and either a `choices` or `choice_query` option defined.

The parameters will be labelled by removing the special suffix, changing the underscores to spaces, and title-casing the string.

You have to write your queries with the idea that the form is giving you strings, and account for that.  Test your reports with various parameter values and make sure you add appropriate casts or tests to make sure it's producing the right output and not crashing.  The inputs are all parameterized so you shouldn't need to worry about SQL injection, but don't make any assumptions about input.

Parameter Options
~~~~~~~~~~~~~~~~~
When defining a report, you can add a JSON object to detail its parameter options (Note that this is a newer parameter, older implementations might not contain this column).  The object is in this format::
  {
    parameter_name: {
      label: "Override the default label",
      type: "Override the detected type",
      # Either a choices or choice_query is required for lookup fields
      choices: [{"value": 1, "label": "Option1"}, {"value": 2, "label": "Option2"}],
      choice_query: "SELECT id as value, name as label from mylookuptable where enabled",
      required: true/false, # if the parameter is required,
      order: Int or string used for sorting the parameters
    },
  }

It is not required to specify parameters or option keys if you don't wish to override the default.  Necessary keys will be auto-generated if you leave them out.


Extending the application
-------------------------

If you want to make it easy to merge in improvements from upstream (and you do), then you must be careful not to edit any of the source-controlled files.  There are ways to extend the application with additional functionality, however, which are detailed here.

Customizing Forms
~~~~~~~~~~~~~~~~~

When a dataset record is called up for display, the application will look for a jinja2 template whose name matches the dataset name under "templates/forms/".  If one is not found, it will generate a form using the default.jinja2 template.  To create a custom form for your dataset, simply create one that has the same name as your dataset; e.g. for an "employees" dataset, create "templates/forms/employees.jinja2".

Unless you want to create the form entirely from scratch, make your new form extend "forms/record_form.jinja2", then start adding content to the block "formfields".  This block exists inside the <form> element so you don't need to create all that, just add fields in whatever structure (table, list, etc) you wish.

Macros
++++++

Inside a custom form, there are a few jinja macros that can make life easier:

boolselect(name, attributes)
  This creates a boolean select box with options "", True, and False.  "attributes" is a dictionary in the form attribute:value.  Each item in the dict will be added as an attribute of the SELECT element.

lookupselect(column_name, data, input_name=None)
  This creates a drop-down based on a lookup table specified in the configuration.  "column_name" should be the name of the column in the "for_table".  "input_name" can optionally override the "name" attribute in the SELECT element (useful in the case of, e.g. details entries).  "data" is the data element in the template (just "data" should always be put here).

autodetail(table_name, data)
  This creates a table for detail records exactly like the default form does.  Just give it the table name (as specified in the configuration), and pass in the form's data object (just put "data" here).

Useful classes
++++++++++++++

Many special HTML classes can be applied to certain elements to produce automation.

autocomplete
  Applied to an INPUT[type=text], this will do an ajax autocomplete for the field based on values existing in the database.

date
  Applied to an input, it will cause a date or datetime to be "friendly-formatted" in read-only mode.

edit_only
  Applied to any element on a record form, it will only show the element in edit mode.

hide_when_empty
  Applied to a *container* of an input element or elements.  If all the input elements inside the container are empty, the container will be hidden when the form is in read-only mode.

never_show
  Will hide the element under any and all circumstances.



Customizing Base Templates
~~~~~~~~~~~~~~~~~~~~~~~~~~

The actual base templates should not be edited.  If you need to insert content into the base template (for all pages), you can include any of the following files in the templates.  These files should be located in ``templates/instance/``:

headers.jinja2
  Will be inserted just before `</head>` into the base template.

pre_nav.jinja2
  Will be inserted just before the navigation bar.

pre_content.jinja2
  Will be inserted just before the content, after the navigation.

post_content.jinja2
  Will be inserted just after the content, before the footer.

local_routes.py
~~~~~~~~~~~~~~~

The file ``includes/local_routes.py`` can be created in your application to override or create new callbacks, pages, or operations for your application.  If it doesn't exist, it'll be ignored.

``local_routes.py`` should contain a dict named `LOCAL`.  This dict defines three sub-dictionaries called "post_ops", "json_ops", and "pages".

post_ops
++++++++

A post_op is a callback that will be added to the callbacks handled by the `post_data()` function.  post_ops are used for writing data to the system, and can only be accessed by users with write access.  Each post_op is keyed with an opcode, and the value is a dict with these fields:

db_function
  A database object method which will be called when the post_op callback is used.

permission
  A session key which must be present for a user to use the post_op.

Essentially, post_ops connect a HTTP post request to a database object method.  The POST arguments will be passed along to the method as keyword args.

json_ops
++++++++

These are callbacks that will be added to what the `json_get()` function handles.  These generally only require read access, but you can customize this.  Each entry in this dict is keyed with the opcode name, and the value is a dict containing:

db_function
  A database method that will be called to fetch the JSON data.

permission
  A session key which the user must have in order to access this operation.


pages
+++++

Entries in "pages" will be added to the routing table so that they can be accessed from a URL.  These also need to be added to the `NAVIGATION` dict in the instance configuration (``instance/config.py``) to be accessible from the main navigation.  Pages is a list, and each entry is a dict, with these values.

url
  A URL for the page.  Can be used for items that don't have a callback that you want in the navigation, like external URLs.

callback
  The function which will be called when the page is accessed

permission
  A session key which a user must have in order to access the page

label
  What the page will be called in navigation


Login Hooks
+++++++++++

The ``login_hooks`` key is a list of functions that should be called after a user has successfully authenticated.  These functions should be defined or imported into localroutes.py.  Each function will be passed keyword arguments for username, attempted_url, attempted_hash, and the authenticator object.  Login hooks can access `g` and `session` if these are imported where they are defined.

Form Hooks
++++++++++

The ``form_hooks`` key is a dictionary for functions to run when a specific form is requested.  This can be used to populate the `data` dictionary passed to the template.  Each key/value pair in the dict is in the form `{form_id: [list, of, functions]}`; when the form is called, the form_id is matched and the functions are called in order.  Each function will be passed the `key` value and `data` dict as keyword arguments.

Custom Callback Functions
+++++++++++++++++++++++++

In additon to the `LOCAL` dict, the ``local_routes.py`` file can contain callback definitions for callbacks referred to in the `LOCAL["pages"]` list.  You need to define these *before* you define the `LOCAL` dict, of course.

Don't decorate these, this will be done later automatically using the data in the `LOCAL["pages"]` list.  You probably want to import `g` and `render_template` from `flask` so that you can access the database and render templates in your callback function.

local_database.py
~~~~~~~~~~~~~~~~~

``local_database.py`` is for adding special methods to your database object.  It should at a minimum import the `Database` object from `.db.database` and define a class called `LocalDatabase` which inherits `Database`::

    from .db.database import Database

    class LocalDatabase(Database):
	# method definitions here....

You don't need to do anything with the constructor, just create any database methods you'll need to handle your local functions.

Make sure that any function which will be exposed as a `json_op` or `post_op` has a trailing `**kwargs` in its arguments list to eat any arguments the http request may throw at it.  Also make sure it has appropriate positional arguments if it's going to be called with a parameter.

Methods exposed as json_ops don't need to explicitly return JSON, just a serializable list/tuple/dict.  WCGFF will take care of serializing it to JSON.  post_op callbacks will also serialize any response as JSON and return it to the calling javascript, so return whatever is appropriate.

Extending Javascript
~~~~~~~~~~~~~~~~~~~~

The config option `LOCAL_JS_FILES` is a list of files to be included after the upstream JS files.  The files named here should go into `static/js/`.

When writing js, you have access to several JS libararies:

- jQuery
- jQuery-ui
- moment.js
- datatables
- mustache.js
- tiny pubsub
- jquery inputmask

  The object `document.wcgff` is a reference to the main application object.  There are a number of hooks and signals that your code can latch into.

  Those... need to be documented

Customizing CSS
~~~~~~~~~~~~~~~

The configuration file lets you specify application-specific CSS files that you can include in 'static/css/', as well as a custom Jquery-ui theme.  See the configuration documentation for details.
